# Code Visualiser

### IMPORTANT
In order to clone project along with sub-modules, use **--recursive** flag during cloning.

```
git clone git@gitlab.com:code-visualiser/code-visualiser.git --recursive
```

If you clone without this flag, you need to manually initialise sub-modules by running
```
git submodule update --init
```

# Project structure
```
common/            common sharable code, no external frameworks!
adapters/          adapters for implementing outside communication
application/       core business logic
configuration/     spring boot module and all configurations, builds docker image
dev-scripts/       git submodule for common scripts and deployment helpers 
```

### Development environment setup
* JDK11 and JAVA_HOME set
* Docker (enable **Expose daemon on tcp...** from settings)
* IDEA plugins: Lombok
* Enable -> File | Settings | Build, Execution, Deployment | Compiler | Annotation Processors

### Third party services
* Neo4j - backend database. (**required**)

docker-compose files are provided to launch named services. 
* Can be either ran by right clicking on the manifest by 'Run ...'
* Using command line:
```
docker-compose -f dev-scripts/docker/neo4j.yml up -d
```
To kill containers
```
docker-compose -f dev-scripts/docker/neo4j.yml down
```

### Updating dev scripts and GraphifyEvolution
To update the sub-modules to their latest commit run:
```git submodule foreach git pull origin master```

Additionally to use GraphifyEvolution:
```
cd GraphifyEvolution
swift build
```

### Updating dev scripts
dev-scripts is git sub-modules. Sub-module is based on commit hash.
If you need to change anything inside dev-scripts do the following:

1. Checkout dev-scripts repository
2. Do required changes and commit-push to master
3. To pull latest changes inside project run:
```git submodule update --remote```
4. When committing changes, you should see dev-scripts new hash.
5. If you need to fix dev-scripts to any specific commit do the following:
* cd dev-scripts
* git checkout 7ee89cd5
