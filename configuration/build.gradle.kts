plugins {
    id("org.springframework.boot")
    id("com.google.cloud.tools.jib")
}

dependencies {
    implementation(project(":common"))
    implementation(project(":application"))
    implementation(project(":adapters:persistence"))
    implementation(project(":adapters:graphify"))
    implementation(project(":adapters:web"))

    implementation("org.springframework.boot:spring-boot-starter")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.cloud:spring-cloud-starter-config")

    developmentOnly("org.springframework.boot:spring-boot-devtools")

    testImplementation("org.springframework.cloud:spring-cloud-stream-test-support")
    testImplementation("org.testcontainers:junit-jupiter:1.14.2")
    testImplementation("org.testcontainers:neo4j:1.15.2")
}

jib {
    to {
        image = "code-visualiser"
    }
    container {
        creationTime = "USE_CURRENT_TIMESTAMP"
    }
}
