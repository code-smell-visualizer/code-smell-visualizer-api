package ee.ut.codevisualiser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@SpringBootApplication
public class CodeVisualiserApplication {

    public static void main(String[] args) {
        SpringApplication.run(CodeVisualiserApplication.class, args);
    }

    @PostConstruct
    public void init() {
        TimeZone.setDefault(TimeZone.getTimeZone("Europe/Tallinn"));
    }
}
