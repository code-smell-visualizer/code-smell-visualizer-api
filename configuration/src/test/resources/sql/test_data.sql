CREATE (a: App {author: 'TEST', branch: 'TEST', commit: 'TEST'});
CREATE (cl: Class {code: '', kind: 'classType', name: 'TEST1', path: '/src/test/TEST1', usr: 'TEST_USER1', version_number: 1});
CREATE (cl: Class {code: '', kind: 'classType', name: 'TEST2', path: '/src/test/TEST2', usr: 'TEST_USER2', version_number: 1});
MATCH (cl: Class), (a: App) CREATE (a)-[r:APP_OWNS_CLASS]->(cl);
