package ee.ut.codevisualiser.integration.web.codeevolution;

import ee.ut.codevisualiser.integration.web.CommonTestData;
import ee.ut.codevisualiser.integration.web.TestContainerBase;
import ee.ut.codevisualiser.integration.web.base.AppFixture;
import ee.ut.codevisualiser.web.codeevolution.GetCommitsInputModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public class CodeEvolutionTest extends TestContainerBase {

    @Autowired
    MockMvc mvc;

    @Autowired
    private AppFixture appFixture;

    ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach()
    void setup() {
        appFixture.createApp(CommonTestData.createAppNode());
    }

    @Test
    @Transactional
    public void GIVEN_app_created_WHEN_code_smells_impact_requested_with_one_code_smell_THEH_max_impact_returned() throws Exception {
        GetCommitsInputModel input = new GetCommitsInputModel(CommonTestData.TEST_APP_BRANCH);

        mvc.perform(post("/api/code-evolution/commits")
                .content(objectMapper.writeValueAsString(input))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].branch", is(CommonTestData.TEST_APP_BRANCH)))
                .andExpect(jsonPath("$[0].author", is(CommonTestData.TEST_APP_AUTHOR)))
                .andExpect(jsonPath("$[0].name", is(CommonTestData.TEST_APP_NAME)))
                .andExpect(jsonPath("$[0].version", is(1)));
    }

    @Test
    @Transactional
    public void WHEN_modified_parameters_request_made_THEN_correct_number_returned() throws Exception {
        mvc.perform(get("/api/code-evolution/branches")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]", is(CommonTestData.TEST_APP_BRANCH)));
    }
}
