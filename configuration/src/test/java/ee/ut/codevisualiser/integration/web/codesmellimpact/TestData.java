package ee.ut.codevisualiser.integration.web.codesmellimpact;

import ee.ut.codevisualiser.integration.web.CommonTestData;
import ee.ut.codevisualiser.web.codesmellimpact.CodeSmellImpactInputModel;

import java.util.List;

public class TestData extends CommonTestData {

    public static CodeSmellImpactInputModel createCodeSmellImpactInputModelWithOneCodeSmell() {
        return new CodeSmellImpactInputModel(TEST_APP_BRANCH, List.of(createLongMethod()), TEST_APP_NAME);
    }

    public static CodeSmellImpactInputModel createCodeSmellImpactInputModelWithMultipleCodeSmell() {
        return new CodeSmellImpactInputModel(TEST_APP_BRANCH, getAllCodeSmellInputModels(), TEST_APP_NAME);
    }
}
