package ee.ut.codevisualiser.integration.web;

import ee.ut.codevisualiser.CodeVisualiserApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.Neo4jContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

@Testcontainers
@SpringBootTest(classes = CodeVisualiserApplication.class)
@ContextConfiguration(initializers = TestContainerBase.Initializer.class)
public class TestContainerBase {

    private static final Neo4jContainer NEO4J_CONTAINER;

    static {
        NEO4J_CONTAINER = new Neo4jContainer<>(DockerImageName.parse("neo4j:4.2.0"))
                .withoutAuthentication();
        NEO4J_CONTAINER.start();
    }

    public static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        @Override
        public void initialize(ConfigurableApplicationContext context) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + NEO4J_CONTAINER.getBoltUrl()
            ).applyTo(context.getEnvironment());
        }
    }
}
