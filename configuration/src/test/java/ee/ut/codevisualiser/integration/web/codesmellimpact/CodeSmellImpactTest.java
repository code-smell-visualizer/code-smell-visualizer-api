package ee.ut.codevisualiser.integration.web.codesmellimpact;

import ee.ut.codevisualiser.integration.web.CommonTestData;
import ee.ut.codevisualiser.integration.web.TestContainerBase;
import ee.ut.codevisualiser.integration.web.base.AppFixture;
import ee.ut.codevisualiser.web.codesmellimpact.CodeSmellImpactInputModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public class CodeSmellImpactTest extends TestContainerBase {

    @Autowired
    MockMvc mvc;

    @Autowired
    private AppFixture appFixture;

    ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach()
    void setup() {
        appFixture.createApp(CommonTestData.createAppNode());
    }

    @Test
    @Transactional
    public void GIVEN_app_is_created_WHEN_code_smells_impact_requested_with_one_code_smell_THEH_max_impact_returned() throws Exception {
        CodeSmellImpactInputModel input = TestData.createCodeSmellImpactInputModelWithOneCodeSmell();

        mvc.perform(post("/api/code-smell-impact")
                .content(objectMapper.writeValueAsString(input))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is("Long method")))
                .andExpect(jsonPath("$[0].impact", is(100.0)));
    }

    @Test
    @Transactional
    public void GIVEN_app_is_created_WHEN_code_smells_impact_requested_with_many_code_smells_THEH_correct_impact_returned() throws Exception {
        CodeSmellImpactInputModel input = TestData.createCodeSmellImpactInputModelWithMultipleCodeSmell();

        mvc.perform(post("/api/code-smell-impact")
                .content(objectMapper.writeValueAsString(input))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(22)));
    }

}
