package ee.ut.codevisualiser.integration.web;

import ee.ut.codevisualiser.application.domain.codesmell.enums.CodeSmellType;
import ee.ut.codevisualiser.persistance.codeanalysis.general.ClassNode;
import ee.ut.codevisualiser.persistance.codeanalysis.general.MethodNode;
import ee.ut.codevisualiser.persistance.codeevolution.AppNode;
import ee.ut.codevisualiser.web.codeanalysis.CodeAnalysisInputModel;
import ee.ut.codevisualiser.web.codeanalysis.CodeSmellInputModel;
import ee.ut.codevisualiser.web.codeanalysis.CodeSmellParametersInputModel;
import ee.ut.codevisualiser.web.codesmell.UpdateCodeSmellsInputModel;

import java.time.LocalDateTime;
import java.util.List;

public class CommonTestData {

    public static final String TEST_APP_NAME = "TEST_APP_NAME";
    public static final String TEST_APP_AUTHOR = "TEST_APP_AUTHOR";
    public static final String TEST_APP_BRANCH = "TEST_APP_BRANCH";
    public static final String TEST_COMMIT_HASH = "TEST_COMMIT_HASH";
    public static final String TEST_COMMIT_MESSAGE = "TEST_COMMIT_MESSAGE";

    public static final String TEST_CLASS_CODE = "TEST_CLASS_CODE";
    public static final String TEST_CLASS_NAME = "TEST_CLASS_NAME";
    public static final String TEST_CLASS_PATH = "TEST_CLASS_PATH";
    public static final String TEST_CLASS_USR = "TEST_CLASS_USR";

    public static final String TEST_CLASS_CODE2 = "TEST_CLASS_CODE2";
    public static final String TEST_CLASS_NAME2 = "TEST_CLASS_NAME2";
    public static final String TEST_CLASS_PATH2 = "TEST_CLASS_PATH2";
    public static final String TEST_CLASS_USR2 = "TEST_CLASS_USR2";

    public static final String TEST_METHOD_CODE = "TEST_METHOD_CODE";
    public static final String TEST_METHOD_NAME = "TEST_METHOD_NAME";
    public static final String TEST_METHOD_USR = "TEST_METHOD_USR";
    public static final Integer NUMBER_OF_INSTRUCTIONS = 10;

    public static final Long VERSION_NUMBER = 1L;

    public static CodeAnalysisInputModel createCodeAnalysisInputModel(Long appId) {
        CodeSmellInputModel codeSmellInputModel = createLongMethod();
        return new CodeAnalysisInputModel(TEST_APP_BRANCH, List.of(codeSmellInputModel), appId, TEST_APP_NAME);
    }

    public static CodeAnalysisInputModel createCodeAnalysisInputModelWithMultipleCodeSmells(Long appId) {
        CodeSmellInputModel smell1 = createLongMethod();
        CodeSmellInputModel smell2 = createDataClass();
        return new CodeAnalysisInputModel(TEST_APP_BRANCH, List.of(smell1, smell2), appId, TEST_APP_NAME);
    }

    public static CodeAnalysisInputModel createCodeAnalysisInputModelWithAllCodeSmells(Long appId) {
        return new CodeAnalysisInputModel(
                TEST_APP_BRANCH,
                getAllCodeSmellInputModels(),
                appId,
                TEST_APP_NAME);
    }

    public static UpdateCodeSmellsInputModel createUpdateCodeSmellsInputModel() {
        return new UpdateCodeSmellsInputModel(getAllCodeSmellInputModels());
    }

    protected static List<CodeSmellInputModel> getAllCodeSmellInputModels() {
        return List.of(
                createLongMethod(),
                createLargeClass(),
                createDataClass(),
                createShotGunSurgery(),
                createSwitchStatements(),
                createLazyClass(),
                createMessageChains(),
                createComments(),
                createCD(),
                createIntensiveCoupling(),
                createDistortedHierarchy(),
                createTraditionBreaker(),
                createDivergentChange(),
                createLongParameterList(),
                createFeatureEnvy(),
                //createDataClump(),
                createSpeculativeGeneralityC(),
                createSpeculativeGeneralityM(),
                createMiddleMan(),
                createBrainMethod(),
                createGodClass(),
                createSwissArmyKnife(),
                createComplexClass()
                );
    }

    protected static CodeSmellInputModel createLongMethod() {
        return CodeSmellInputModel.builder()
                .name("Long method")
                .selected(true)
                .type(CodeSmellType.METHOD_BASED.name())
                .parameters(List.of(CodeSmellParametersInputModel.builder()
                        .name("nr of instructions")
                        .value(5.0)
                        .build()))
                .build();
    }

    protected static CodeSmellInputModel createDataClass() {
        return CodeSmellInputModel.builder()
                .name("Data class")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED.name())
                .build();
    }

    protected static CodeSmellInputModel createLargeClass() {
        return CodeSmellInputModel.builder()
                .name("Large class / Blob class")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED.name())
                .parameters(List.of(
                        new CodeSmellParametersInputModel(10L, "cohesion in methods", 1.0),
                        new CodeSmellParametersInputModel(20L, "nr of methods", 1.0),
                        new CodeSmellParametersInputModel(30L, "nr of attributes", 1.0)))
                .build();
    }

    protected static CodeSmellInputModel createShotGunSurgery() {
        return CodeSmellInputModel.builder()
                .name("Shotgun surgery")
                .selected(true)
                .type(CodeSmellType.METHOD_BASED.name())
                .parameters(List.of(
                        new CodeSmellParametersInputModel(40L, "nr of callers", 1.0)))
                .build();
    }

    protected static CodeSmellInputModel createSwitchStatements() {
        return CodeSmellInputModel.builder()
                .name("Switch statements")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED.name())
                .parameters(List.of(
                        new CodeSmellParametersInputModel(50L, "nr of statements", 1.0)))
                .build();
    }

    protected static CodeSmellInputModel createLazyClass() {
        return CodeSmellInputModel.builder()
                .name("Lazy class")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED.name())
                .parameters(List.of(
                        new CodeSmellParametersInputModel(60L, "median nr of instructions", 1.0),
                        new CodeSmellParametersInputModel(70L, "complexity-method ratio", 1.0),
                        new CodeSmellParametersInputModel(80L, "coupling between classes", 1.0),
                        new CodeSmellParametersInputModel(90L, "nr of depth of inheritance", 1.0)))
                .build();
    }

    protected static CodeSmellInputModel createMessageChains() {
        return CodeSmellInputModel.builder()
                .name("Message chains")
                .selected(true)
                .type(CodeSmellType.METHOD_BASED.name())
                .parameters(List.of(
                        new CodeSmellParametersInputModel(100L, "nr of chained messages", 1.0)))
                .build();
    }

    protected static CodeSmellInputModel createComments() {
        return CodeSmellInputModel.builder()
                .name("Comments")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED.name())
                .parameters(List.of(
                        new CodeSmellParametersInputModel(110L, "nr of comments", 1.0)))
                .build();
    }

    protected static CodeSmellInputModel createCD() {
        return CodeSmellInputModel.builder()
                .name("Cyclic dependencies (dependencies between classes)")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED.name())
                .build();
    }

    protected static CodeSmellInputModel createIntensiveCoupling() {
        return CodeSmellInputModel.builder()
                .name("Intensive coupling")
                .selected(true)
                .type(CodeSmellType.METHOD_BASED.name())
                .parameters(List.of(
                        new CodeSmellParametersInputModel(120L, "short-term memory cap", 1.0),
                        new CodeSmellParametersInputModel(130L, "half coupling dispersion", 1.0),
                        new CodeSmellParametersInputModel(140L, "few coupling intensity", 1.0),
                        new CodeSmellParametersInputModel(150L, "quarter coupling dispersion", 1.0),
                        new CodeSmellParametersInputModel(160L, "maximum nesting depth", 1.0)))
                .build();
    }

    protected static CodeSmellInputModel createDistortedHierarchy() {
        return CodeSmellInputModel.builder()
                .name("Distorted hierarchy")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED.name())
                .parameters(List.of(
                        new CodeSmellParametersInputModel(170L, "short term memory cap", 1.0)))
                .build();
    }

    protected static CodeSmellInputModel createTraditionBreaker() {
        return CodeSmellInputModel.builder()
                .name("Tradition Breaker")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED.name())
                .parameters(List.of(
                        new CodeSmellParametersInputModel(180L, "low nr of methods/attributes", 1.0),
                        new CodeSmellParametersInputModel(190L, "high nr of methods/attributes", 1.0)))
                .build();
    }

    protected static CodeSmellInputModel createDivergentChange() {
        return CodeSmellInputModel.builder()
                .name("Divergent Change/Schizophrenic Class")
                .selected(true)
                .type(CodeSmellType.METHOD_BASED.name())
                .parameters(List.of(
                        new CodeSmellParametersInputModel(200L, "nr of called methods", 1.0)))
                .build();
    }

    protected static CodeSmellInputModel createLongParameterList() {
        return CodeSmellInputModel.builder()
                .name("Long parameter list")
                .selected(true)
                .type(CodeSmellType.METHOD_BASED.name())
                .parameters(List.of(
                        new CodeSmellParametersInputModel(210L, "nr of parameters", 1.0)))
                .build();
    }

    protected static CodeSmellInputModel createFeatureEnvy() {
        return CodeSmellInputModel.builder()
                .name("Feature envy")
                .selected(true)
                .type(CodeSmellType.METHOD_BASED.name())
                .parameters(List.of(
                        new CodeSmellParametersInputModel(220L, "access to foreign variables", 1.0),
                        new CodeSmellParametersInputModel(230L, "locality fraction", 1.0),
                        new CodeSmellParametersInputModel(240L, "access to foreign classes", 1.0)))
                .build();
    }

    /*
    protected static CodeSmellInputModel createDataClump() {
        return CodeSmellInputModel.builder()
                .name("Data clumps (class variables)")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED.name())
                .parameters(List.of(
                        new CodeSmellParametersInputModel(250L, "nr of repeating variables", 1.0)))
                .build();
    }
     */

    protected static CodeSmellInputModel createSpeculativeGeneralityC() {
        return CodeSmellInputModel.builder()
                .name("Speculative generality (interfaces)")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED.name())
                .build();
    }

    protected static CodeSmellInputModel createSpeculativeGeneralityM() {
        return CodeSmellInputModel.builder()
                .name("Speculative generality (methods)")
                .selected(true)
                .type(CodeSmellType.METHOD_BASED.name())
                .build();
    }

    protected static CodeSmellInputModel createMiddleMan() {
        return CodeSmellInputModel.builder()
                .name("Middle man")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED.name())
                .parameters(List.of(
                        new CodeSmellParametersInputModel(260L, "nr of instructions method", 1.0),
                        new CodeSmellParametersInputModel(270L, "methods delegation ratio", 1.0)))
                .build();
    }

    protected static CodeSmellInputModel createBrainMethod() {
        return CodeSmellInputModel.builder()
                .name("Brain method")
                .selected(true)
                .type(CodeSmellType.METHOD_BASED.name())
                .parameters(List.of(
                        new CodeSmellParametersInputModel(280L, "instructions for class", 1.0),
                        new CodeSmellParametersInputModel(290L, "cyclomatic complexity", 1.0),
                        new CodeSmellParametersInputModel(300L, "maximal nesting depth", 1.0),
                        new CodeSmellParametersInputModel(310L, "accessed variables", 1.0)))
                .build();
    }


    protected static CodeSmellInputModel createGodClass() {
        return CodeSmellInputModel.builder()
                .name("God class")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED.name())
                .parameters(List.of(
                        new CodeSmellParametersInputModel(320L, "class cohesion fraction", 1.0),
                        new CodeSmellParametersInputModel(330L, "weighted method count", 1.0),
                        new CodeSmellParametersInputModel(340L, "access to foreign data", 1.0)))
                .build();
    }

    protected static CodeSmellInputModel createSwissArmyKnife() {
        return CodeSmellInputModel.builder()
                .name("Swiss army knife")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED.name())
                .parameters(List.of(
                        new CodeSmellParametersInputModel(350L, "nr of methods", 1.0)))
                .build();
    }

    protected static CodeSmellInputModel createComplexClass() {
        return CodeSmellInputModel.builder()
                .name("Complex class")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED.name())
                .parameters(List.of(
                        new CodeSmellParametersInputModel(360L, "class complexity", 1.0)))
                .build();
    }


    public static AppNode createAppNode() {
        return AppNode.builder()
                .name(TEST_APP_NAME)
                .author(TEST_APP_AUTHOR)
                .branch(TEST_APP_BRANCH)
                .commitHash(TEST_COMMIT_HASH)
                .commitMessage(TEST_COMMIT_MESSAGE)
                .time(LocalDateTime.now().toString())
                .classNodes(List.of(createClassNode()))
                .versionNumber(VERSION_NUMBER)
                .build();
    }

    public static AppNode createAppNodeWithMultipleClasses() {
        return AppNode.builder()
                .name(TEST_APP_NAME)
                .author(TEST_APP_AUTHOR)
                .branch(TEST_APP_BRANCH)
                .commitHash(TEST_COMMIT_HASH)
                .commitMessage(TEST_COMMIT_MESSAGE)
                .time(LocalDateTime.now().toString())
                .classNodes(List.of(createClassNode(), createClassNode2()))
                .versionNumber(VERSION_NUMBER)
                .build();
    }

    public static ClassNode createClassNode() {
        return ClassNode.builder()
                .code(TEST_CLASS_CODE)
                .name(TEST_CLASS_NAME)
                .path(TEST_CLASS_PATH)
                .usr(TEST_CLASS_USR)
                .methods(List.of(createMethodNode()))
                .versionNr(VERSION_NUMBER)
                .build();
    }

    public static ClassNode createClassNode2() {
        return ClassNode.builder()
                .code(TEST_CLASS_CODE2)
                .name(TEST_CLASS_NAME2)
                .path(TEST_CLASS_PATH2)
                .usr(TEST_CLASS_USR2)
                .methods(List.of(createMethodNode()))
                .versionNr(VERSION_NUMBER)
                .build();
    }

    public static MethodNode createMethodNode() {
        return MethodNode.builder()
                .code(TEST_METHOD_CODE)
                .name(TEST_METHOD_NAME)
                .numberOfInstructions(NUMBER_OF_INSTRUCTIONS)
                .usr(TEST_METHOD_USR)
                .versionNr(VERSION_NUMBER)
                .build();
    }
}
