package ee.ut.codevisualiser.integration.web.codeanalysis;

import ee.ut.codevisualiser.integration.web.CommonTestData;
import ee.ut.codevisualiser.integration.web.TestContainerBase;
import ee.ut.codevisualiser.integration.web.base.AppFixture;
import ee.ut.codevisualiser.web.codeanalysis.CodeAnalysisInputModel;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public class CodeAnalysisTest extends TestContainerBase {

    @Autowired
    MockMvc mvc;

    @Autowired
    private AppFixture appFixture;

    ObjectMapper objectMapper = new ObjectMapper();


    @Test
    @Transactional
    public void GIVEN_app_exists_WHEN_single_input_passed_THEN_correct_data_returned() throws Exception {
        Long appId = appFixture.createApp(CommonTestData.createAppNode()).id();

        CodeAnalysisInputModel input = CommonTestData.createCodeAnalysisInputModel(appId);

        mvc.perform(post("/api/code-analysis")
                .content(objectMapper.writeValueAsString(input))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.classes", hasSize(1)))
                .andExpect(jsonPath("$.classes[0].name", is(CommonTestData.TEST_CLASS_NAME)))
                .andExpect(jsonPath("$.classes[0].size", is(10)))
                .andExpect(jsonPath("$.classes[0].force.x", is(800)))
                .andExpect(jsonPath("$.classes[0].force.y", is(450)))
                .andExpect(jsonPath("$.classes[0].score", is(1.0)));
    }

    @Test
    @Transactional
    public void GIVEN_app_exists_WHEN_multiple_input_passed_THEN_correct_data_returned() throws Exception {
        Long appId = appFixture.createApp(CommonTestData.createAppNodeWithMultipleClasses()).id();

        CodeAnalysisInputModel input = CommonTestData.createCodeAnalysisInputModelWithMultipleCodeSmells(appId);

        mvc.perform(post("/api/code-analysis")
                .content(objectMapper.writeValueAsString(input))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.classes", hasSize(2)))
                .andExpect(jsonPath("$.classes[0].name", is(CommonTestData.TEST_CLASS_NAME)))
                .andExpect(jsonPath("$.classes[0].size", is(10)))
                .andExpect(jsonPath("$.classes[0].force.x", is(400)))
                .andExpect(jsonPath("$.classes[0].force.y", is(225)))
                .andExpect(jsonPath("$.classes[0].score", is(1.0)))
                .andExpect(jsonPath("$.classes[1].name", is(CommonTestData.TEST_CLASS_NAME2)))
                .andExpect(jsonPath("$.classes[1].size", is(10)))
                .andExpect(jsonPath("$.classes[1].force.x", is(1200)))
                .andExpect(jsonPath("$.classes[1].force.y", is(225)))
                .andExpect(jsonPath("$.classes[1].score", is(1.0)));
    }

    @Test
    @Transactional
    public void GIVEN_app_exists_WHEN_all_smell_inputs_passed_THEN_correct_data_returned() throws Exception {
        appFixture.createApp(CommonTestData.createAppNodeWithMultipleClasses()).id();

        CodeAnalysisInputModel input = CommonTestData.createCodeAnalysisInputModelWithAllCodeSmells(null);

        mvc.perform(post("/api/code-analysis")
                .content(objectMapper.writeValueAsString(input))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.classes", hasSize(2)))
                .andExpect(jsonPath("$.classes[0].name", is(CommonTestData.TEST_CLASS_NAME)))
                .andExpect(jsonPath("$.classes[0].size", is(10)))
                .andExpect(jsonPath("$.classes[0].force.x", is(400)))
                .andExpect(jsonPath("$.classes[0].force.y", is(225)))
                .andExpect(jsonPath("$.classes[0].score", is(1.0)))
                .andExpect(jsonPath("$.classes[1].name", is(CommonTestData.TEST_CLASS_NAME2)))
                .andExpect(jsonPath("$.classes[1].size", is(10)))
                .andExpect(jsonPath("$.classes[1].force.x", is(1200)))
                .andExpect(jsonPath("$.classes[1].force.y", is(225)))
                .andExpect(jsonPath("$.classes[1].score", is(1.0)));
    }
}
