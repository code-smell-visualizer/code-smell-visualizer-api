package ee.ut.codevisualiser.integration.web.codesmell;

import ee.ut.codevisualiser.integration.web.CommonTestData;
import ee.ut.codevisualiser.integration.web.TestContainerBase;
import ee.ut.codevisualiser.web.codesmell.UpdateCodeSmellsInputModel;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public class CodeSmellTest extends TestContainerBase {

    @Autowired
    MockMvc mvc;

    ObjectMapper objectMapper = new ObjectMapper();

    @Test
    @Transactional
    public void WHEN_code_smells_get_request_made_THEN_code_smells_returned() throws Exception {
        mvc.perform(get("/api/code-smell")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(22)))
                .andExpect(jsonPath("$[0].name", is("Brain method")));
    }

    @Test
    @Transactional
    public void WHEN_update_code_smells_THEN_no_error_thrown() throws Exception {
        UpdateCodeSmellsInputModel input = CommonTestData.createUpdateCodeSmellsInputModel();

        mvc.perform(post("/api/code-smell")
                .content(objectMapper.writeValueAsString(input))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void WHEN_modified_parameters_request_made_THEN_correct_number_returned() throws Exception {
        mvc.perform(get("/api/code-smell/nr-of-params")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is(0)));
    }

    @Test
    @Transactional
    public void GIVEN_initial_state_unchanged_WHEN_parameters_modified_THEN_code_smells_updated() throws Exception {
        UpdateCodeSmellsInputModel input = CommonTestData.createUpdateCodeSmellsInputModel();

        mvc.perform(post("/api/code-smell")
                .content(objectMapper.writeValueAsString(input))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mvc.perform(get("/api/code-smell")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(22)))
                .andExpect(jsonPath("$[0].name", is("Brain method")))
                .andExpect(jsonPath("$[0].parameters", hasSize(4)))
                .andExpect(jsonPath("$[0].parameters[0].name", is("instructions for class")))
                .andExpect(jsonPath("$[0].parameters[0].value", is(1.0)));

        mvc.perform(get("/api/code-smell/nr-of-params")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is(32)));
    }


}
