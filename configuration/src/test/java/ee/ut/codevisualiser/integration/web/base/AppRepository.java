package ee.ut.codevisualiser.integration.web.base;

import ee.ut.codevisualiser.persistance.codeevolution.AppNode;
import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface AppRepository extends Neo4jRepository<AppNode, Long> {
}
