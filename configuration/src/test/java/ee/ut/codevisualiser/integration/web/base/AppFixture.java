package ee.ut.codevisualiser.integration.web.base;

import ee.ut.codevisualiser.common.TestPersistenceAdapter;
import ee.ut.codevisualiser.persistance.codeevolution.AppNode;


@TestPersistenceAdapter
public class AppFixture {

    private final AppRepository appRepository;

    public AppFixture(AppRepository appRepository) {
        this.appRepository = appRepository;
    }

    public AppNode createApp(AppNode appNode) {
        return appRepository.save(appNode);
    }
}
