package ee.ut.codevisualiser.web.codesmell;

import com.fasterxml.jackson.annotation.JsonProperty;
import ee.ut.codevisualiser.application.domain.codesmell.CodeSmell;
import ee.ut.codevisualiser.application.domain.codesmell.CodeSmellParameter;
import ee.ut.codevisualiser.application.domain.codesmell.enums.CodeSmellType;
import ee.ut.codevisualiser.application.usecase.codesmell.UpdateCodeSmells.Request;
import ee.ut.codevisualiser.web.codeanalysis.CodeSmellInputModel;
import ee.ut.codevisualiser.web.codeanalysis.CodeSmellParametersInputModel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
public class UpdateCodeSmellsInputModel {

    @JsonProperty("codeSmells")
    List<CodeSmellInputModel> codeSmellsInputModel;

    public Request toUpdateCodeSmellRequest() {
        List<CodeSmell> codeSmells = codeSmellsInputModel.stream()
                .map(this::toCodeSmell)
                .collect(Collectors.toList());
        return Request.of(codeSmells);
    }

    private CodeSmell toCodeSmell(CodeSmellInputModel smell) {
        return CodeSmell.builder()
                .id(smell.id())
                .name(smell.name())
                .description(smell.description())
                .selected(smell.selected())
                .type(CodeSmellType.valueOf(smell.type()))
                .parameters(toParametersList(smell.parameters()))
                .build();
    }

    private List<CodeSmellParameter> toParametersList(List<CodeSmellParametersInputModel> parameters) {
        if (parameters == null) {
            return null;
        }
        return parameters.stream().map(this::toCodeSmellParameter).collect(Collectors.toList());
    }

    private CodeSmellParameter toCodeSmellParameter(CodeSmellParametersInputModel parameter) {
        return CodeSmellParameter.builder()
                .id(parameter.id())
                .name(parameter.name())
                .value(parameter.value())
                .build();
    }
}
