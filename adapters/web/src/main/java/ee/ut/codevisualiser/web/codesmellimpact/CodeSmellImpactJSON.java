package ee.ut.codevisualiser.web.codesmellimpact;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CodeSmellImpactJSON {
    private String name;
    private Double impact;
}
