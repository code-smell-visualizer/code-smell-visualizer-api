package ee.ut.codevisualiser.web.codesmellimpact;

import ee.ut.codevisualiser.application.domain.codesmellimpact.CodeSmellImpact;
import ee.ut.codevisualiser.application.usecase.codesmellimpact.GetCodeSmellImpacts.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.util.List;
import java.util.stream.Collectors;

public class CodeSmellImpactPresenter {

    List<CodeSmellImpactJSON> result;

    public void present(Response response) {
        result = response.codeSmellsImpact.stream()
                .map(this::toCodeSmellJson)
                .collect(Collectors.toList());
    }

    private CodeSmellImpactJSON toCodeSmellJson(CodeSmellImpact smell) {
        return new CodeSmellImpactJSON(smell.codeSmellName(), smell.impact());
    }

    public ResponseEntity<List<CodeSmellImpactJSON>> getViewModel() {
        Assert.notNull(result, () -> "Result must be presented");
        return ResponseEntity.ok(result);
    }

}
