package ee.ut.codevisualiser.web.codeevolution;

import ee.ut.codevisualiser.application.usecase.codeevolution.GetBranches;
import ee.ut.codevisualiser.application.usecase.codeevolution.GetCommits;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/code-evolution")
@RequiredArgsConstructor
public class CodeEvolutionController {

    private final GetBranches getBranches;
    private final GetCommits getCommits;

    @PostMapping("/commits")
    public ResponseEntity<List<CommitJSON>> getCommits(@RequestBody GetCommitsInputModel input) {
        var response = getCommits.execute(input.toRequest());
        var presenter = new CodeEvolutionPresenter();

        presenter.present(response);
        return presenter.getViewModel();
    }

    @GetMapping("/branches")
    public ResponseEntity<List<String>> getBranches() {
        var response = getBranches.execute();
        var presenter = new BranchesPresenter();

        presenter.present(response);
        return presenter.getViewModel();
    }
}
