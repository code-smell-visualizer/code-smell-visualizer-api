package ee.ut.codevisualiser.web.codeanalysis;

import ee.ut.codevisualiser.application.domain.codeanalysis.ClassAnalysis;
import ee.ut.codevisualiser.application.domain.codeanalysis.Force;
import ee.ut.codevisualiser.application.usecase.codeanalysis.GetCodeAnalysis.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.util.Comparator;
import java.util.stream.Collectors;

public class CodeAnalysisPresenter {

    private CodeAnalysisJSON result;

    public void present(Response response) {
        result = new CodeAnalysisJSON(
                response.getClassAnalyses().stream()
                        .map(this::toClassAnalysisJSON)
                        .sorted(Comparator.comparing(ClassAnalysisJSON::getName))
                        .collect(Collectors.toList()));
    }

    private ClassAnalysisJSON toClassAnalysisJSON(ClassAnalysis classAnalysis) {
        return new ClassAnalysisJSON(classAnalysis.name(),
                toCoordinatesJSON(classAnalysis.force()),
                classAnalysis.size(),
                round(classAnalysis.score()));
    }

    private ForceJSON toCoordinatesJSON(Force force) {
        return new ForceJSON(force.x(), force.y());
    }

    public ResponseEntity<CodeAnalysisJSON> getViewModel() {
        Assert.notNull(result, () -> "Result must be presented");
        return ResponseEntity.ok(result);
    }

    private double round(double value) {
        return Math.round(value * 10000.0) / 10000.0;
    }
}
