package ee.ut.codevisualiser.web.upload;

import ee.ut.codevisualiser.application.usecase.upload.GetExistingApplications.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.util.List;

public class GetExistingApplicationsPresenter {

    List<String> result;

    public void  present(Response response) {
        result = response.getExistingApplications();
    }

    public ResponseEntity<List<String>> getViewModel() {
        Assert.notNull(result, () -> "Result must be presented");
        return ResponseEntity.ok(result);
    }
}
