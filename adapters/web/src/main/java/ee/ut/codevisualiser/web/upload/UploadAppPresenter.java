package ee.ut.codevisualiser.web.upload;

import ee.ut.codevisualiser.application.usecase.upload.UploadApp;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

public class UploadAppPresenter {

    String result;

    public void present(UploadApp.Response response) {
        result = response.getAppName();
    }

    public ResponseEntity<String> getViewModel() {
        Assert.notNull(result, () -> "Result must be presented");
        return ResponseEntity.ok(result);
    }
}
