package ee.ut.codevisualiser.web.codeanalysis;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ClassAnalysisJSON {
    private final String name;
    private final ForceJSON force;
    private final int size;
    private final double score;
}
