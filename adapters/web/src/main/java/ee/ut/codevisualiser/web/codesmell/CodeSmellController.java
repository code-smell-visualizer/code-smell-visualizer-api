package ee.ut.codevisualiser.web.codesmell;

import ee.ut.codevisualiser.application.usecase.codesmell.GetCodeSmells;
import ee.ut.codevisualiser.application.usecase.codesmell.GetNrOfModifiedParameters;
import ee.ut.codevisualiser.application.usecase.codesmell.UpdateCodeSmells;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/code-smell")
@RequiredArgsConstructor
public class CodeSmellController {

    private final GetCodeSmells getCodeSmells;
    private final GetNrOfModifiedParameters getNrOfModifiedParameters;
    private final UpdateCodeSmells updateCodeSmells;

    @GetMapping
    ResponseEntity<List<CodeSmellJSON>> getCodeSmells() {
        var response = getCodeSmells.execute();
        var presenter = new CodeSmellPresenter();

        presenter.present(response);
        return presenter.getViewModel();
    }

    @PostMapping
    ResponseEntity<Void> updateCodeSmells(@RequestBody UpdateCodeSmellsInputModel updateCodeSmellsInputModel) {
        updateCodeSmells.execute(updateCodeSmellsInputModel.toUpdateCodeSmellRequest());
        return ResponseEntity.ok().build();
    }

    @GetMapping("/nr-of-params")
    ResponseEntity<Integer> getNrOfModifiedParams() {
        var response = getNrOfModifiedParameters.execute();
        var presenter = new NrOfModifiedParametersPresenter();

        presenter.present(response);
        return presenter.getViewModel();
    }
}
