package ee.ut.codevisualiser.web.codeanalysis;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(fluent = true)
public class CodeSmellInputModel {

    @JsonProperty("id")
    Long id;

    @JsonProperty("name")
    String name;

    @JsonProperty("selected")
    boolean selected;

    @JsonProperty("type")
    String type;

    @JsonProperty("description")
    String description;

    @JsonProperty("parameters")
    List<CodeSmellParametersInputModel> parameters;
}
