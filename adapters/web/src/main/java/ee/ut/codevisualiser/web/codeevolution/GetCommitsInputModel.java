package ee.ut.codevisualiser.web.codeevolution;

import com.fasterxml.jackson.annotation.JsonProperty;
import ee.ut.codevisualiser.application.usecase.codeevolution.GetCommits.Request;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(fluent = true)
public class GetCommitsInputModel {

    @JsonProperty("branch")
    String branch;

    public Request toRequest() {
        return Request.of(branch);
    }
}
