package ee.ut.codevisualiser.web.codeanalysis;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class CodeAnalysisJSON {
    private final List<ClassAnalysisJSON> classes;
}
