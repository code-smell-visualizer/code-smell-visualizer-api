package ee.ut.codevisualiser.web.codeevolution;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class CommitJSON {

    private final Long id;
    private final String author;
    private final String branch;
    private final String hash;
    private final String message;
    private final String name;
    private final Date time;
    private final long version;
}
