package ee.ut.codevisualiser.web.codeevolution;

import ee.ut.codevisualiser.application.domain.codeevolution.App;
import ee.ut.codevisualiser.application.usecase.codeevolution.GetCommits.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CodeEvolutionPresenter {

    private List<CommitJSON> result;

    public void present(Response response) {
        result = toCommitJSONList(response.commits);
    }

    private List<CommitJSON> toCommitJSONList(List<App> commits) {
        return commits.stream()
                .sorted(Comparator.comparing(App::time, Comparator.reverseOrder()))
                .map(this::toCommitJSON).collect(Collectors.toList());
    }

    private CommitJSON toCommitJSON(App commit) {
        return new CommitJSON(
                commit.id(),
                commit.author(),
                commit.branch(),
                commit.commitHash(),
                commit.commitMessage(),
                commit.name(),
                commit.time(),
                commit.versionNumber());
    }

    public ResponseEntity<List<CommitJSON>> getViewModel() {
        Assert.notNull(result, () -> "Result must be presented");
        return ResponseEntity.ok(result);
    }
}
