package ee.ut.codevisualiser.web.codeevolution;

import ee.ut.codevisualiser.application.usecase.codeevolution.GetBranches.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.util.List;

public class BranchesPresenter {

    private List<String> result;

    public void present(Response response) {
        result = response.branches;
    }

    public ResponseEntity<List<String>> getViewModel() {
        Assert.notNull(result, () -> "Result must be presented");
        return ResponseEntity.ok(result);
    }
}
