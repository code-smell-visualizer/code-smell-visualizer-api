package ee.ut.codevisualiser.web.codesmell;

import ee.ut.codevisualiser.application.usecase.codesmell.GetNrOfModifiedParameters.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

public class NrOfModifiedParametersPresenter {

    private Integer result;

    public void present(Response response) {
        result = response.nrOfParamsModified;
    }

    public ResponseEntity<Integer> getViewModel() {
        Assert.notNull(result, () -> "Result must be presented");
        return ResponseEntity.ok(result);
    }
}
