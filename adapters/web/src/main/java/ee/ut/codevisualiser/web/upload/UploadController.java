package ee.ut.codevisualiser.web.upload;

import ee.ut.codevisualiser.application.usecase.upload.GetExistingApplications;
import ee.ut.codevisualiser.application.usecase.upload.UploadApp;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/upload")
@RequiredArgsConstructor
public class UploadController {

    private final GetExistingApplications getExistingApplications;
    private final UploadApp uploadApp;

    @GetMapping("/existing-applications")
    ResponseEntity<List<String>> getExistingApplications() {
        var response = getExistingApplications.execute();
        var presenter = new GetExistingApplicationsPresenter();

        presenter.present(response);
        return presenter.getViewModel();
    }

    @PostMapping
    ResponseEntity<String> uploadApp(@RequestBody UploadAppInputModel input) {
        var response =  uploadApp.execute(input.toRequest());
        var presenter = new UploadAppPresenter();

        presenter.present(response);
        return presenter.getViewModel();
    }
}
