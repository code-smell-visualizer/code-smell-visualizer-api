package ee.ut.codevisualiser.web.codesmell;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class CodeSmellJSON {

    private Long id;
    private String name;
    private boolean selected;
    private String type;
    private String description;
    private List<CodeSmellParameterJSON> parameters;

}
