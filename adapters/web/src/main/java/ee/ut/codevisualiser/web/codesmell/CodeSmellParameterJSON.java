package ee.ut.codevisualiser.web.codesmell;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CodeSmellParameterJSON {
    private Long id;
    private String name;
    private double value;
}
