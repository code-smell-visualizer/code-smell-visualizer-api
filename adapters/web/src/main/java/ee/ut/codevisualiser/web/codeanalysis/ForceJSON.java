package ee.ut.codevisualiser.web.codeanalysis;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ForceJSON {
    private int x;
    private int y;
}
