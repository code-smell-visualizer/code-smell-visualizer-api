package ee.ut.codevisualiser.web.codesmellimpact;

import ee.ut.codevisualiser.application.usecase.codesmellimpact.GetCodeSmellImpacts;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/code-smell-impact")
@RequiredArgsConstructor
public class CodeSmellImpactController {

    private final GetCodeSmellImpacts getCodeSmellImpacts;

    @PostMapping
    ResponseEntity<List<CodeSmellImpactJSON>> getSmellStatistics(@RequestBody CodeSmellImpactInputModel input) {
        var response = getCodeSmellImpacts.execute(input.toRequest());
        var presenter = new CodeSmellImpactPresenter();

        presenter.present(response);
        return presenter.getViewModel();
    }
}
