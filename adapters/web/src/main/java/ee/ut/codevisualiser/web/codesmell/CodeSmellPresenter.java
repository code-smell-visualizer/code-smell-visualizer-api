package ee.ut.codevisualiser.web.codesmell;

import ee.ut.codevisualiser.application.domain.codesmell.CodeSmell;
import ee.ut.codevisualiser.application.domain.codesmell.CodeSmellParameter;
import ee.ut.codevisualiser.application.usecase.codesmell.GetCodeSmells.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CodeSmellPresenter {

    private List<CodeSmellJSON> result;

    public void present(Response response) {
        result = response.codeSmells.stream().map(this::toCodeSmellJSON)
                .sorted(Comparator.comparing(CodeSmellJSON::getName))
                .collect(Collectors.toList());
    }

    private CodeSmellJSON toCodeSmellJSON(CodeSmell codeSmell) {
        return new CodeSmellJSON(
                codeSmell.id(),
                codeSmell.name(),
                codeSmell.selected(),
                codeSmell.type().name(),
                codeSmell.description() == null ? "" : codeSmell.description(),
                toParameterList(codeSmell.parameters()));
    }

    private List<CodeSmellParameterJSON> toParameterList(List<CodeSmellParameter> parameters) {
        if (parameters == null) {
            return null;
        }
        return parameters.stream().map(this::toParameterJSON).collect(Collectors.toList());

    }

    private CodeSmellParameterJSON toParameterJSON(CodeSmellParameter parameter) {
        return new CodeSmellParameterJSON(parameter.id(), parameter.name(), parameter.value());
    }

    public ResponseEntity<List<CodeSmellJSON>> getViewModel() {
        Assert.notNull(result, () -> "Result must be presented");
        return ResponseEntity.ok(result);
    }
}
