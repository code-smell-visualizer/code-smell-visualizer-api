package ee.ut.codevisualiser.web.upload;

import com.fasterxml.jackson.annotation.JsonProperty;
import ee.ut.codevisualiser.application.usecase.upload.UploadApp.Request;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;

@AllArgsConstructor
@RequiredArgsConstructor
public class UploadAppInputModel {

    @JsonProperty("appPath")
    String appPath;

    public Request toRequest() {
        return Request.of(appPath);
    }
}
