package ee.ut.codevisualiser.web.codeanalysis;

import ee.ut.codevisualiser.application.usecase.codeanalysis.GetCodeAnalysis;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/code-analysis")
@RequiredArgsConstructor
public class CodeAnalysisController {

    private final GetCodeAnalysis getCodeAnalysis;

    @PostMapping
    ResponseEntity<CodeAnalysisJSON> getCodeAnalysis(@RequestBody CodeAnalysisInputModel input) {
        var response = getCodeAnalysis.execute(input.toRequest());
        var presenter = new CodeAnalysisPresenter();

        presenter.present(response);
        return presenter.getViewModel();
    }
}
