package ee.ut.codevisualiser.web.codeanalysis;

import ee.ut.codevisualiser.application.domain.codeanalysis.ClassAnalysis;
import ee.ut.codevisualiser.application.domain.codeanalysis.Force;
import ee.ut.codevisualiser.application.domain.codesmell.enums.CodeSmellType;
import ee.ut.codevisualiser.application.usecase.codeanalysis.GetCodeAnalysis.Response;

import java.util.List;

public class TestData {

    public static final Long ID = 1L;
    public static final String NAME = "TEST";
    public static final Double SCORE = 1.0;
    public static final Integer SIZE = 100;
    public static final Integer FORCE_X = 100;
    public static final Integer FORCE_Y = 100;

    public static Response mockResponse() {
        List<ClassAnalysis> classAnalyses = List.of(ClassAnalysis.builder()
                .id(ID)
                .name(NAME)
                .score(SCORE)
                .size(SIZE)
                .force(Force.builder()
                        .x(FORCE_X)
                        .y(FORCE_Y)
                        .build())
                .build());
        return Response.of(classAnalyses);
    }

    public static CodeAnalysisInputModel createInputModel() {
        return new CodeAnalysisInputModel("TEST", List.of(createLongMethod()), 1L, "TEST");
    }

    protected static CodeSmellInputModel createLongMethod() {
        return CodeSmellInputModel.builder()
                .name("Long method")
                .selected(true)
                .type(CodeSmellType.METHOD_BASED.name())
                .parameters(List.of(CodeSmellParametersInputModel.builder()
                        .name("nr of instructions")
                        .value(5.0)
                        .build()))
                .build();
    }
}
