package ee.ut.codevisualiser.web.codesmell;

import ee.ut.codevisualiser.application.domain.codesmell.CodeSmell;
import ee.ut.codevisualiser.application.domain.codesmell.enums.CodeSmellType;
import ee.ut.codevisualiser.application.usecase.codesmell.GetCodeSmells;
import ee.ut.codevisualiser.application.usecase.codesmell.GetNrOfModifiedParameters;
import ee.ut.codevisualiser.web.codeanalysis.CodeSmellInputModel;
import ee.ut.codevisualiser.web.codeanalysis.CodeSmellParametersInputModel;

import java.util.List;

public class TestData {

    public static Long ID = 1L;
    public static Boolean SELECTED = true;
    public static String NAME = "TEST";
    public static String DESCRIPTION = "TEST";
    public static CodeSmellType TYPE = CodeSmellType.METHOD_BASED;

    public static GetCodeSmells.Response mockGetCodeSmellsResponse() {
        return GetCodeSmells.Response.of(List.of(CodeSmell.builder()
                .id(ID)
                .type(TYPE)
                .selected(SELECTED)
                .name(NAME)
                .description(DESCRIPTION)
                .build()));
    }

    public static UpdateCodeSmellsInputModel createUpdateCodeSmellsInputModel() {
        return new UpdateCodeSmellsInputModel(getAllCodeSmellInputModels());
    }

    protected static List<CodeSmellInputModel> getAllCodeSmellInputModels() {
        return List.of(
                createLongMethod(),
                createDataClass()
        );
    }

    protected static CodeSmellInputModel createLongMethod() {
        return CodeSmellInputModel.builder()
                .name("Long method")
                .selected(true)
                .type(CodeSmellType.METHOD_BASED.name())
                .parameters(List.of(CodeSmellParametersInputModel.builder()
                        .name("nr of instructions")
                        .value(5.0)
                        .build()))
                .build();
    }

    protected static CodeSmellInputModel createDataClass() {
        return CodeSmellInputModel.builder()
                .name("Data class")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED.name())
                .build();
    }

    public static GetNrOfModifiedParameters.Response mockNrOfModifiedParametersResponse() {
        return GetNrOfModifiedParameters.Response.of(10);
    }
}
