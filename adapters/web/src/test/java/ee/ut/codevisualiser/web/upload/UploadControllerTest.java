package ee.ut.codevisualiser.web.upload;

import ee.ut.codevisualiser.application.usecase.upload.GetExistingApplications;
import ee.ut.codevisualiser.application.usecase.upload.UploadApp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UploadControllerTest {

    private UploadController controller;
    private UploadApp uploadApp;
    private GetExistingApplications getExistingApplications;
    private Verify verify;

    @BeforeEach
    void setup() {
        getExistingApplications = mock(GetExistingApplications.class);
        uploadApp = mock(UploadApp.class);
        controller = new UploadController(getExistingApplications, uploadApp);
        verify = new Verify();
    }

    @Test
    void WHEN_existing_applications_required_THEN_returned_data_mapped_correctly() {
        when(getExistingApplications.execute()).thenReturn(TestData.mockGetExistingApplicationsResponse());

        var response = controller.getExistingApplications();

        verify.getExistingApplicationsResponseMappedCorrectly(response);
    }

    @Test
    void WHEN_app_uploaded_THEN_app_name_returned() {
        when(uploadApp.execute(any())).thenReturn(TestData.mockUploadAppResponse());

        var response = controller.uploadApp(TestData.mockUploadAppInputModel());

        verify.getUploadAppResponseMappedCorrectly(response);
    }

    private static class Verify {

        void getExistingApplicationsResponseMappedCorrectly(ResponseEntity<List<String>> response) {
            assertThat(Objects.requireNonNull(response.getBody())).hasOnlyOneElementSatisfying(app -> {
                assertThat(app).isEqualTo(TestData.EXISTING_APP);
            });
        }

        void getUploadAppResponseMappedCorrectly(ResponseEntity<String> response) {
            assertThat(Objects.requireNonNull(response.getBody())).isEqualTo(TestData.EXISTING_APP);
        }
    }
}
