package ee.ut.codevisualiser.web.codeanalysis;

import ee.ut.codevisualiser.application.usecase.codeanalysis.GetCodeAnalysis;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CodeAnalysisControllerTest {

    private CodeAnalysisController controller;
    private GetCodeAnalysis getCodeAnalysis;
    private Verify verify;


    @BeforeEach
    void setup() {
        getCodeAnalysis = mock(GetCodeAnalysis.class);
        controller = new CodeAnalysisController(getCodeAnalysis);
        verify = new Verify();
    }

    @Test
    void WHEN_code_smells_analysis_requested_THEN_returned_data_mapped_correctly() {
        when(getCodeAnalysis.execute(any())).thenReturn(TestData.mockResponse());

        var response = controller.getCodeAnalysis(TestData.createInputModel());

        verify.responseMappedCorrectly(response);
    }

    @Test
    void WHEN_code_smells_analysis_requested_and_error_occurs_THEN_correct_error_message_thrown() {
        when(getCodeAnalysis.execute(any())).thenReturn(TestData.mockResponse());

        var response = controller.getCodeAnalysis(TestData.createInputModel());

        verify.responseMappedCorrectly(response);
    }

    private static class Verify {
        void responseMappedCorrectly(ResponseEntity<CodeAnalysisJSON> response) {
            assertThat(Objects.requireNonNull(response.getBody()).getClasses()).hasOnlyOneElementSatisfying(classAnalysis -> {
                assertThat(classAnalysis.getName()).isEqualTo(TestData.NAME);
                assertThat(classAnalysis.getScore()).isEqualTo(TestData.SCORE);
                assertThat(classAnalysis.getSize()).isEqualTo(TestData.SIZE);
                assertThat(classAnalysis.getForce().getX()).isEqualTo(TestData.FORCE_X);
                assertThat(classAnalysis.getForce().getY()).isEqualTo(TestData.FORCE_Y);
            });
        }

    }
}
