package ee.ut.codevisualiser.web.codesmell;

import ee.ut.codevisualiser.application.usecase.codesmell.GetCodeSmells;
import ee.ut.codevisualiser.application.usecase.codesmell.GetNrOfModifiedParameters;
import ee.ut.codevisualiser.application.usecase.codesmell.UpdateCodeSmells;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class CodeSmellControllerTest {

    private CodeSmellController controller;
    private GetCodeSmells getCodeSmells;
    private GetNrOfModifiedParameters getNrOfModifiedParameters;
    private UpdateCodeSmells updateCodeSmells;
    private Verify verify;


    @BeforeEach
    void setup() {
        getCodeSmells = mock(GetCodeSmells.class);
        getNrOfModifiedParameters = mock(GetNrOfModifiedParameters.class);
        updateCodeSmells = mock(UpdateCodeSmells.class);
        controller = new CodeSmellController(getCodeSmells, getNrOfModifiedParameters, updateCodeSmells);
        verify = new Verify();
    }

    @Test
    void WHEN_code_smells_requested_THEN_returned_data_mapped_correctly() {
       when(getCodeSmells.execute()).thenReturn(TestData.mockGetCodeSmellsResponse());

       var response = controller.getCodeSmells();

       verify.getCodeSmellsResponseMappedCorrectly(response);
    }

    @Test
    void WHEN_code_smells_updated_THEN_no_error_thrown() {
        doNothing().when(updateCodeSmells).execute(any());
        controller.updateCodeSmells(TestData.createUpdateCodeSmellsInputModel());
    }

    @Test
    void WHEN_nr_of_modified_code_smells_params_requested_THEN_correct_number_returned() {
        when(getNrOfModifiedParameters.execute()).thenReturn(TestData.mockNrOfModifiedParametersResponse());

        var response = controller.getNrOfModifiedParams();

        verify.getNrOfModifiedParamsResponseMappedCorrectly(response);
    }

    private static class Verify {
        void getCodeSmellsResponseMappedCorrectly(ResponseEntity<List<CodeSmellJSON>> response) {
            assertThat(Objects.requireNonNull(response.getBody())).hasOnlyOneElementSatisfying(codeSmell -> {
                assertThat(codeSmell.getName()).isEqualTo(TestData.NAME);
                assertThat(codeSmell.getDescription()).isEqualTo(TestData.DESCRIPTION);
                assertThat(codeSmell.getId()).isEqualTo(TestData.ID);
                assertThat(codeSmell.isSelected()).isEqualTo(TestData.SELECTED);
            });
        }

        void getNrOfModifiedParamsResponseMappedCorrectly(ResponseEntity<Integer> response) {
            assertThat(Objects.requireNonNull(response.getBody()).equals(10));
        }


    }
}
