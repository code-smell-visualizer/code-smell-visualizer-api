package ee.ut.codevisualiser.web.codeevolution;

import ee.ut.codevisualiser.application.usecase.codeevolution.GetBranches;
import ee.ut.codevisualiser.application.usecase.codeevolution.GetCommits;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CodeSmellEvolutionControllerTest {

    private CodeEvolutionController controller;
    private GetCommits getCommits;
    private GetBranches getBranches;
    private Verify verify;

    @BeforeEach
    void setup() {
        getBranches = mock(GetBranches.class);
        getCommits = mock(GetCommits.class);
        controller = new CodeEvolutionController(getBranches, getCommits);
        verify = new Verify();
    }

    @Test
    void WHEN_commits_requested_THEN_response_mapped_correctly() {
        when(getCommits.execute(any())).thenReturn(TestData.mockGetCommitsResponse());

        var response = controller.getCommits(TestData.createCommitsInputModel());

        verify.getCommitsResponseMappedCorrectly(response);
    }

    @Test
    void WHEN_branches_requested_THEN_response_mapped_correctly() {
        when(getBranches.execute()).thenReturn(TestData.mockGetBranchesResponse());

        var response = controller.getBranches();

        verify.getBranchesResponseMappedCorrectly(response);
    }


    private static class Verify {
        void getCommitsResponseMappedCorrectly(ResponseEntity<List<CommitJSON>> response) {
            assertThat(Objects.requireNonNull(response.getBody())).hasOnlyOneElementSatisfying(commit -> {
                assertThat(commit.getName()).isEqualTo(TestData.NAME);
                assertThat(commit.getVersion()).isEqualTo(TestData.VERSION);
                assertThat(commit.getMessage()).isEqualTo(TestData.MESSAGE);
                assertThat(commit.getAuthor()).isEqualTo(TestData.NAME);
            });
        }

        void getBranchesResponseMappedCorrectly(ResponseEntity<List<String>> response) {
            assertThat(Objects.requireNonNull(response.getBody()))
                    .hasOnlyOneElementSatisfying(branch -> assertThat(branch).isEqualTo(TestData.TEST_APP_BRANCH));
        }
    }
}
