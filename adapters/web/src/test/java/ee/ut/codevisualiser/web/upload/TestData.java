package ee.ut.codevisualiser.web.upload;

import ee.ut.codevisualiser.application.usecase.upload.GetExistingApplications;
import ee.ut.codevisualiser.application.usecase.upload.UploadApp;

import java.util.List;

public class TestData {

    public static final String EXISTING_APP = "app";
    public static final String APP_PATH = "path";

    public static GetExistingApplications.Response mockGetExistingApplicationsResponse() {
        return GetExistingApplications.Response.of(List.of(EXISTING_APP));
    }

    public static UploadAppInputModel mockUploadAppInputModel() {
        return new UploadAppInputModel(APP_PATH);
    }

    public static UploadApp.Response mockUploadAppResponse() {
        return UploadApp.Response.of(EXISTING_APP);
    }
}
