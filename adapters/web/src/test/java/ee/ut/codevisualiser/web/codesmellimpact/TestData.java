package ee.ut.codevisualiser.web.codesmellimpact;

import ee.ut.codevisualiser.application.domain.codesmell.enums.CodeSmellType;
import ee.ut.codevisualiser.application.domain.codesmellimpact.CodeSmellImpact;
import ee.ut.codevisualiser.application.usecase.codesmellimpact.GetCodeSmellImpacts;
import ee.ut.codevisualiser.web.codeanalysis.CodeSmellInputModel;
import ee.ut.codevisualiser.web.codeanalysis.CodeSmellParametersInputModel;

import java.util.List;

public class TestData {

    public static final String NAME = "TEST";
    public static final String BRANCH = "TEST";
    public static final double IMPACT = 10.0;

    public static GetCodeSmellImpacts.Response mockResponse() {
        return GetCodeSmellImpacts.Response.of(List.of(CodeSmellImpact.builder()
                .codeSmellName(NAME)
                .impact(IMPACT)
                .build()));
    }

    public static CodeSmellImpactInputModel createCodeSmellImpactInputModel() {
        return new CodeSmellImpactInputModel(BRANCH, List.of(createLongMethod()), NAME);
    }

    protected static CodeSmellInputModel createLongMethod() {
        return CodeSmellInputModel.builder()
                .name("Long method")
                .selected(true)
                .type(CodeSmellType.METHOD_BASED.name())
                .parameters(List.of(CodeSmellParametersInputModel.builder()
                        .name("nr of instructions")
                        .value(5.0)
                        .build()))
                .build();
    }
}
