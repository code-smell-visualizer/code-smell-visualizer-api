package ee.ut.codevisualiser.web.codeevolution;

import ee.ut.codevisualiser.application.domain.codeevolution.App;
import ee.ut.codevisualiser.application.usecase.codeevolution.GetBranches;
import ee.ut.codevisualiser.application.usecase.codeevolution.GetCommits;

import java.util.List;

public class TestData {

    public static String NAME = "TEST";
    public static String MESSAGE = "TEST";
    public static Long VERSION = 1L;
    public static String TEST_APP_BRANCH = "TEST_APP_BRANCH";

    public static GetCommits.Response mockGetCommitsResponse() {
        return GetCommits.Response.of(List.of(App.builder()
                .name(NAME)
                .versionNumber(VERSION)
                .commitMessage(MESSAGE)
                .author(NAME)
                .build()
        ));
    }

    public static GetCommitsInputModel createCommitsInputModel() {
        return new GetCommitsInputModel(TEST_APP_BRANCH);
    }

    public static GetBranches.Response mockGetBranchesResponse() {
        return GetBranches.Response.of(List.of(TEST_APP_BRANCH));
    }
}
