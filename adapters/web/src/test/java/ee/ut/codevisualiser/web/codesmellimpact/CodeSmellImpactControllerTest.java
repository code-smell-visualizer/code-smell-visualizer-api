package ee.ut.codevisualiser.web.codesmellimpact;

import ee.ut.codevisualiser.application.usecase.codesmellimpact.GetCodeSmellImpacts;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CodeSmellImpactControllerTest {

    private CodeSmellImpactController controller;
    private GetCodeSmellImpacts getCodeSmellImpacts;
    private Verify verify;

    @BeforeEach
    void setup() {
        getCodeSmellImpacts = mock(GetCodeSmellImpacts.class);
        controller = new CodeSmellImpactController(getCodeSmellImpacts);
        verify = new Verify();
    }


    @Test
    void WHEN_code_smell_impacts_requested_THEN_returned_data_mapped_correctly() {
        when(getCodeSmellImpacts.execute(any())).thenReturn(TestData.mockResponse());

        var response = controller.getSmellStatistics(TestData.createCodeSmellImpactInputModel());

        verify.responseMappedCorrectly(response);
    }

    private static class Verify {
        void responseMappedCorrectly(ResponseEntity<List<CodeSmellImpactJSON>> response) {
            assertThat(Objects.requireNonNull(response.getBody())).hasOnlyOneElementSatisfying(impact -> {
                assertThat(impact.getName()).isEqualTo(TestData.NAME);
                assertThat(impact.getImpact()).isEqualTo(TestData.IMPACT);
            });
        }

    }
}
