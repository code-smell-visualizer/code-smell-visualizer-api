dependencies {
    implementation(project(":common"))
    implementation(project(":application"))

    implementation("org.springframework.boot:spring-boot-starter-undertow")
    api("org.springframework.boot:spring-boot-starter-web") {
        exclude(module = "spring-boot-starter-tomcat")
    }
    api("org.zalando:problem-spring-web:0.25.2")

    implementation("org.springframework.boot:spring-boot-starter-security")

    implementation("io.springfox:springfox-boot-starter:3.0.0")
    implementation("io.springfox:springfox-swagger-ui:3.0.0")
}
