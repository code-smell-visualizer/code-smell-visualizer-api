package ee.ut.codevisualiser.persistance.upload;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;

import java.util.List;

public interface Uploads extends Neo4jRepository<String, Long> {

    @Query("MATCH (a:App) return distinct a.name")
    List<String> findAllExistingApps();
}
