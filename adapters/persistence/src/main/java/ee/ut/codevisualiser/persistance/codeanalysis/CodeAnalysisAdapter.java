package ee.ut.codevisualiser.persistance.codeanalysis;

import ee.ut.codevisualiser.application.domain.codeanalysis.CodeAnalysis;
import ee.ut.codevisualiser.application.domain.codeanalysis.CodeAnalysisResult;
import ee.ut.codevisualiser.application.domain.codesmell.CodeSmell;
import ee.ut.codevisualiser.application.domain.codesmell.CodeSmellParameter;
import ee.ut.codevisualiser.application.usecase.codeanalysis.CodeAnalysisGateway;
import ee.ut.codevisualiser.common.PersistenceAdapter;
import ee.ut.codevisualiser.persistance.codeanalysis.classcodesmell.ClassCodeSmellAnalyser;
import ee.ut.codevisualiser.persistance.codeanalysis.classcodesmell.ClassCodeSmellQueryResult;
import ee.ut.codevisualiser.persistance.codeanalysis.classusage.ClassUsageAnalyser;
import ee.ut.codevisualiser.persistance.codeanalysis.classusage.ClassUsageQueryResult;
import ee.ut.codevisualiser.persistance.codeanalysis.general.GeneralClassAnalyser;
import ee.ut.codevisualiser.persistance.codeanalysis.general.GeneralClassAnalyserQueryResult;
import ee.ut.codevisualiser.persistance.codeanalysis.methodcodesmell.MethodCodeSmellAnalyser;
import ee.ut.codevisualiser.persistance.codeanalysis.methodcodesmell.MethodCodeSmellQueryResult;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;

@PersistenceAdapter
@Transactional
public class CodeAnalysisAdapter implements CodeAnalysisGateway {

    private final ClassUsageAnalyser classUsageAnalyser;
    private final GeneralClassAnalyser generalClassAnalyser;
    private final ClassCodeSmellAnalyser classCodeSmellAnalyser;
    private final MethodCodeSmellAnalyser methodCodeSmellAnalyser;

    private CodeAnalysisResult codeAnalysisResult;
    private Long appId;

    public CodeAnalysisAdapter(GeneralClassAnalyser generalClassAnalyser, MethodCodeSmellAnalyser methodCodeSmellAnalyser, ClassUsageAnalyser classUsageAnalyser, ClassCodeSmellAnalyser classCodeSmellAnalyser) {
        this.generalClassAnalyser = generalClassAnalyser;
        this.methodCodeSmellAnalyser = methodCodeSmellAnalyser;
        this.classUsageAnalyser = classUsageAnalyser;
        this.classCodeSmellAnalyser = classCodeSmellAnalyser;
    }

    @Override
    public CodeAnalysisResult getCodeAnalysis(String branch, Long appId, String appName, List<CodeSmell> codeSmells) {
        initializeCodeSmellAnalysisResult(branch, appId, appName);
        updateCodeSmellAnalysisResultWithClassUsages();
        codeSmells.forEach(this::updateAnalysisResultWithCodeSmellAnalysis);
        return codeAnalysisResult;
    }

    private void initializeCodeSmellAnalysisResult(String branch, Long applicationId, String appName) {
        HashMap<Long, CodeAnalysis> result = new HashMap<>();
        try {
            appId = applicationId != null ? applicationId : generalClassAnalyser.getLatestAppVersionId(branch, appName);
        } catch (Exception e) {
            appId = generalClassAnalyser.getLatestAppVersionId(appName);
        }
        List<GeneralClassAnalyserQueryResult> generalClassAnalyses = generalClassAnalyser.getGeneralAnalysis(appId);
        generalClassAnalyses.forEach(analysis -> result.put(analysis.id(), CodeAnalysis.builder()
                .name(analysis.name())
                .path(analysis.path())
                .nrOfMethods(analysis.methods())
                .codeSmellScore(0)
                .nrOfUsages(0)
                .build()));
        codeAnalysisResult = CodeAnalysisResult.builder()
                .classCodeSmellAnalysis(result)
                .build();
    }

    private void updateCodeSmellAnalysisResultWithClassUsages() {
        List<ClassUsageQueryResult> classUsageQueryResult = classUsageAnalyser.getClassUsageCount(appId);
        classUsageQueryResult.forEach(classUsage -> {
            if (codeAnalysisResult.classExists(classUsage.classId())) {
                codeAnalysisResult.setClassUsage(classUsage.classId(), classUsage.count());
            }
        });
    }

    private void updateAnalysisResultWithCodeSmellAnalysis(CodeSmell codeSmell) {
        switch (codeSmell.name()) {
            case "Long method": getLongMethodCodeAnalysis(codeSmell.parameters());
                break;
            case "Large class / Blob class": getLargeClassCodeAnalysis(codeSmell.parameters());
                break;
            case "Shotgun surgery": getShotgunSurgeryCodeAnalysis(codeSmell.parameters());
                break;
            case "Switch statements": getSwitchStatementsCodeAnalysis(codeSmell.parameters());
                break;
            case "Lazy class": getLazyClassCodeAnalysis(codeSmell.parameters());
                break;
            case "Message chains": getMessageChainsCodeAnalysis(codeSmell.parameters());
                break;
            case "Data class": getDataClassCodeAnalysis();
                break;
            case "Comments": getCommentsCodeAnalysis(codeSmell.parameters());
                break;
            case "Cyclic dependencies (dependencies between classes)": getCyclicDependenciesCodeAnalysis();
                break;
            case "Intensive coupling": getIntensiveCouplingCodeAnalysis(codeSmell.parameters());
                break;
            case "Distorted hierarchy": getDistortedHierarchyCodeAnalysis(codeSmell.parameters());
                break;
            case "Tradition Breaker": getTraditionBreakerCodeAnalysis(codeSmell.parameters());
                break;
            case "Divergent Change/Schizophrenic Class": getSchizophrenicClassCodeAnalysis(codeSmell.parameters());
                break;
            case "Long parameter list": getLongParameterListCodeAnalysis(codeSmell.parameters());
                break;
            case "Feature envy": getFeatureEnvyCodeAnalysis(codeSmell.parameters());
                break;
            case "Data clumps (class variables)": getClassVariableDataClumpsCodeAnalysis(codeSmell.parameters());
                break;
            case "Data clumps (function arguments)": getMethodDataClumpsCodeAnalysis(codeSmell.parameters());
                break;
            case "Speculative generality (interfaces)": getSpeculativeGeneralityClassCodeAnalysis();
                break;
            case "Speculative generality (methods)": getSpeculativeGeneralityMethodCodeAnalysis();
                break;
            case "Middle man": getMiddleManCodeAnalysis(codeSmell.parameters());
                break;
            case "Parallel inheritance hiearchies":
                break;
            case "Inappropriate intimacy":
                break;
            case "Brain method": getBrainMethodCodeAnalysis(codeSmell.parameters());
                break;
            case "God class": getGodClassCodeAnalysis(codeSmell.parameters());
                break;
            case "Swiss army knife": getSwissArmyKnifeCodeAnalysis(codeSmell.parameters());
                break;
            case "Complex class": getComplexClassCodeAnalysis(codeSmell.parameters());
                break;
        }
    }


    private void getLongMethodCodeAnalysis(List<CodeSmellParameter> parameters) {
        double numberOfInstructions = getParameterValueByName("nr of instructions", parameters);
        addMethodCodeSmellScores(methodCodeSmellAnalyser.countLongMethodsByClass(appId, numberOfInstructions));
    }

    private void getLargeClassCodeAnalysis(List<CodeSmellParameter> parameters) {
        double lackOfCohesionInMethods = getParameterValueByName("cohesion in methods", parameters);
        double numberOfMethods = getParameterValueByName("nr of methods", parameters);
        double numberOfAttributes = getParameterValueByName("nr of attributes", parameters);
        addClassCodeSmellScores(classCodeSmellAnalyser.getLargeClasses(
                appId,
                lackOfCohesionInMethods,
                numberOfMethods,
                numberOfAttributes));
    }

    private void getShotgunSurgeryCodeAnalysis(List<CodeSmellParameter> parameters) {
        double numberOfCallers = getParameterValueByName("nr of callers", parameters);
        addMethodCodeSmellScores(methodCodeSmellAnalyser.countShotgunSurgeryByClass(appId, numberOfCallers));
    }

    private void getSwitchStatementsCodeAnalysis(List<CodeSmellParameter> parameters) {
        double numberOfSwitchStatements = getParameterValueByName("nr of statements", parameters);
        addClassCodeSmellScores(classCodeSmellAnalyser.getClassesWithTooManySwitchStatements(
                appId,
                numberOfSwitchStatements));
    }

    private void getLazyClassCodeAnalysis(List<CodeSmellParameter> parameters) {
        double medianNumberOfInstructions = getParameterValueByName("median nr of instructions", parameters);
        double lowComplexityMethodRatio = getParameterValueByName("complexity-method ratio", parameters);
        double medianCouplingBetweenObjectClasses = getParameterValueByName("coupling between classes", parameters);
        double numberOfSomeDepthOfInheritance = getParameterValueByName("nr of depth of inheritance", parameters);
        addClassCodeSmellScores(classCodeSmellAnalyser.getLazyClasses(
                appId,
                medianNumberOfInstructions,
                lowComplexityMethodRatio,
                medianCouplingBetweenObjectClasses,
                numberOfSomeDepthOfInheritance));
    }

    private void getMessageChainsCodeAnalysis(List<CodeSmellParameter> parameters) {
        double numberOfChainedMessages = getParameterValueByName("nr of chained messages", parameters);
        addMethodCodeSmellScores(methodCodeSmellAnalyser.countMessageChainsByClass(appId, numberOfChainedMessages));
    }

    private void getDataClassCodeAnalysis() {
        addClassCodeSmellScores(classCodeSmellAnalyser.getDataClasses(appId));
    }

    private void getCommentsCodeAnalysis(List<CodeSmellParameter> parameters) {
        double numberOfComments = getParameterValueByName("nr of comments", parameters);
        addClassCodeSmellScores(classCodeSmellAnalyser.getComments(appId, numberOfComments));
    }

    private void getCyclicDependenciesCodeAnalysis() {
        addClassCodeSmellScores(classCodeSmellAnalyser.getCyclicDependencies(appId));
    }

    private void getIntensiveCouplingCodeAnalysis(List<CodeSmellParameter> parameters) {
        double maxNumberOfShortMemoryCap = getParameterValueByName("short-term memory cap", parameters);
        double halfCouplingDispersion = getParameterValueByName("half coupling dispersion", parameters);
        double fewCouplingIntensity = getParameterValueByName("few coupling intensity", parameters);
        double quarterCouplingDispersion = getParameterValueByName("quarter coupling dispersion", parameters);
        double shallowMaximumNestingDepth = getParameterValueByName("maximum nesting depth", parameters);
        addMethodCodeSmellScores(methodCodeSmellAnalyser.countIntensiveCouplingByClass(
                appId,
                maxNumberOfShortMemoryCap,
                halfCouplingDispersion,
                fewCouplingIntensity,
                quarterCouplingDispersion,
                shallowMaximumNestingDepth));
    }

    private void getDistortedHierarchyCodeAnalysis(List<CodeSmellParameter> parameters) {
        double shortTermMemoryCap = getParameterValueByName("short term memory cap", parameters);
        addClassCodeSmellScores(classCodeSmellAnalyser.getDistortedHierarchyClasses(appId, shortTermMemoryCap));
    }

    private void getTraditionBreakerCodeAnalysis(List<CodeSmellParameter> parameters) {
        double lowNumberOfMethodsAndAttributes = getParameterValueByName("low nr of methods/attributes", parameters);
        double highNumberOfMethodsAndAttributes = getParameterValueByName("high nr of methods/attributes", parameters);
        addClassCodeSmellScores(classCodeSmellAnalyser.getTraditionBreakerClasses(
                appId,
                lowNumberOfMethodsAndAttributes,
                highNumberOfMethodsAndAttributes));
    }

    private void getSchizophrenicClassCodeAnalysis(List<CodeSmellParameter> parameters) {
        double numberOfCalledMethods = getParameterValueByName("nr of called methods", parameters);
        addMethodCodeSmellScores(methodCodeSmellAnalyser.countSchizophrenicMethodsByClass(appId, numberOfCalledMethods));
    }

    private void getLongParameterListCodeAnalysis(List<CodeSmellParameter> parameters) {
        double numberOfParameters = getParameterValueByName("nr of parameters", parameters);
        addMethodCodeSmellScores(methodCodeSmellAnalyser.countLongParameterListByClass(appId, numberOfParameters));
    }

    private void getFeatureEnvyCodeAnalysis(List<CodeSmellParameter> parameters) {
        double fewAccessToForeignVariables = getParameterValueByName("access to foreign variables", parameters);
        double localityFraction = getParameterValueByName("locality fraction", parameters);
        double fewAccessToForeignClasses = getParameterValueByName("access to foreign classes", parameters);
        addMethodCodeSmellScores(methodCodeSmellAnalyser.countFeatureEnvyByClass(
                appId,
                fewAccessToForeignVariables,
                localityFraction,
                fewAccessToForeignClasses));
    }

    private void getClassVariableDataClumpsCodeAnalysis(List<CodeSmellParameter> parameters) {
        double numberOfRepeatingVariables = getParameterValueByName("nr of repeating variables", parameters);
        addClassCodeSmellScores(classCodeSmellAnalyser.getDataClumpsClasses(appId, numberOfRepeatingVariables));
    }

    private void getMethodDataClumpsCodeAnalysis(List<CodeSmellParameter> parameters) {
        /* TODO: not working atm
        int numberOfRepeatingArguments = parameters.get(0);
        addMethodCodeSmellScores(methodCodeSmellAnalyser.countDataClumpsByClass(numberOfRepeatingArguments));
         */
    }

    private void getSpeculativeGeneralityClassCodeAnalysis() {
        addClassCodeSmellScores(classCodeSmellAnalyser.getSpeculativeGeneralityInterfaces(appId));
    }

    private void getSpeculativeGeneralityMethodCodeAnalysis() {
        addMethodCodeSmellScores(methodCodeSmellAnalyser.countSpeculativeGeneralityByClass(appId));
    }

    private void getMiddleManCodeAnalysis(List<CodeSmellParameter> parameters) {
        double numberOfInstructionsMethod = getParameterValueByName("nr of instructions method", parameters);
        double delegationToAllMethodsRatioHalf = getParameterValueByName("methods delegation ratio", parameters);
        addClassCodeSmellScores(classCodeSmellAnalyser.getMiddleManClasses(
                appId,
                numberOfInstructionsMethod,
                delegationToAllMethodsRatioHalf));
    }

    private void getBrainMethodCodeAnalysis(List<CodeSmellParameter> parameters) {
        double highNumberOfInstructionsForClass = getParameterValueByName("instructions for class", parameters);
        double highCyclomaticComplexity = getParameterValueByName("cyclomatic complexity", parameters);
        double severalMaximalNestingDepth = getParameterValueByName("maximal nesting depth", parameters);
        double manyAccessedVariables = getParameterValueByName("accessed variables", parameters);
        addMethodCodeSmellScores(methodCodeSmellAnalyser.countBrainMethodsByClass(
                appId,
                highNumberOfInstructionsForClass,
                highCyclomaticComplexity,
                severalMaximalNestingDepth,
                manyAccessedVariables));
    }

    private void getGodClassCodeAnalysis(List<CodeSmellParameter> parameters) {
        double tightClassCohesionFraction = getParameterValueByName("class cohesion fraction", parameters);
        double veryHighWeightedMethodCount = getParameterValueByName("weighted method count", parameters);
        double fewAccessToForeignData = getParameterValueByName("access to foreign data", parameters);
        addClassCodeSmellScores(classCodeSmellAnalyser.getGodClasses(
                appId,
                tightClassCohesionFraction,
                veryHighWeightedMethodCount,
                fewAccessToForeignData));
    }

    private void getSwissArmyKnifeCodeAnalysis(List<CodeSmellParameter> parameters) {
        double veryHighNumberOfMethods = getParameterValueByName("nr of methods", parameters);
        addClassCodeSmellScores(classCodeSmellAnalyser.getSwissArmyKnifeClasses(
                appId,
                veryHighNumberOfMethods));
    }

    private void getComplexClassCodeAnalysis(List<CodeSmellParameter> parameters) {
        double veryHighClassComplexity = getParameterValueByName("class complexity", parameters);
        addClassCodeSmellScores(classCodeSmellAnalyser.getComplexClasses(
                appId,
                veryHighClassComplexity));
    }

    private double getParameterValueByName(String name, List<CodeSmellParameter> parameters) {
        return parameters.stream()
                .filter(param -> param.name().equals(name))
                .findFirst().orElseThrow(RuntimeException::new)
                .value();
    }

    private void addClassCodeSmellScores(List<ClassCodeSmellQueryResult> classCodeSmellQueryResults) {
        classCodeSmellQueryResults.forEach(result -> codeAnalysisResult.addClassCodeSmellScore(result.classId()));
    }

    private void addMethodCodeSmellScores(List<MethodCodeSmellQueryResult> classCodeSmellQueryResults) {
        classCodeSmellQueryResults.forEach(result -> codeAnalysisResult.addMethodCodeSmellScore(result.classId(), result.count()));
    }

}
