package ee.ut.codevisualiser.persistance.codeanalysis.general;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;

@NodeEntity(label = "Method")
@Getter
@Builder
@Accessors(fluent = true)
@NoArgsConstructor
@AllArgsConstructor
public class MethodNode {

    @Id
    @GeneratedValue
    private Long id;

    @Property("code")
    private String code;

    @Property("kind")
    private String kind;

    @Property("name")
    private String name;

    @Property("number_of_instructions")
    private int numberOfInstructions;

    @Property("usr")
    private String usr;

    @Property("version_number")
    private Long versionNr;
}
