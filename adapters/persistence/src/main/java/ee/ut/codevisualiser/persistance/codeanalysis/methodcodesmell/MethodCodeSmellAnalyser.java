package ee.ut.codevisualiser.persistance.codeanalysis.methodcodesmell;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MethodCodeSmellAnalyser extends Neo4jRepository<List<MethodCodeSmellQueryResult>, Long> {

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(c:Class)-[r:CLASS_OWNS_METHOD]->(m:Method) " +
            "WHERE " +
            "id(a) = $appId AND " +
            "m.number_of_instructions > $numberOfInstructions " +
            "RETURN " +
            "distinct id(c) as classId, " +
            "c.name as name, " +
            "count(m) as count")
    List<MethodCodeSmellQueryResult> countLongMethodsByClass(@Param("appId") Long appId,
                                                             @Param("numberOfInstructions") double numberOfInstructions);

    @Query("MATCH (other_m:Method)-[r:CALLS]->(m:Method)<-[:CLASS_OWNS_METHOD]-(c:Class)<-[:APP_OWNS_CLASS]-(a:App) " +
            "WHERE " +
            "id(a) = $appId " +
            "WITH " +
            "c, " +
            "m, " +
            "COUNT(r) as number_of_callers " +
            "WHERE number_of_callers > $numberOfCallers " +
            "RETURN " +
            "distinct id(c) as classId, " +
            "c.name as name, " +
            "count(m) as count")
    List<MethodCodeSmellQueryResult> countShotgunSurgeryByClass(@Param("appId") Long appId,
                                                                @Param("numberOfCallers") double numberOfCallers);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(c:Class)-[CLASS_OWNS_METHOD]-(m:Method) " +
            "WHERE " +
            "id(a) = $appId AND " +
            "m.max_number_of_chaned_message_calls > $numberOfChainedMessages " +
            "RETURN " +
            "distinct id(c) as classId, " +
            "c.name as name, " +
            "count(m) as count")
    List<MethodCodeSmellQueryResult> countMessageChainsByClass(@Param("appId") Long appId,
                                                               @Param("numberOfChainedMessages") double numberOfChainedMessages);

    @Query("MATCH " +
            "(a:App)-[:APP_OWNS_CLASS]->(c:Class)-[r:CLASS_OWNS_METHOD]->(m1:Method)-[s:CALLS]->(m2:Method), " +
            "(c2:Class)-[r2:CLASS_OWNS_METHOD]->(m2) " +
            "WHERE " +
            "id(a) = $appId AND " +
            "id(c) <> id(c2) " +
            "WITH " +
            "   c, " +
            "   m1, " +
            "   count(distinct m2) as method_count, " +
            "   collect(distinct m2.name) as names, " +
            "   collect(distinct c2.name) as class_names, " +
            "   count(distinct c2) as class_count  " +
            "WHERE " +
            "   ((method_count >= $maxNumberOfShortMemoryCap and " +
            "       class_count/method_count <= $halfCouplingDispersion) or " +
            "   (method_count >= $fewCouplingIntensity " +
            "       and class_count/method_count <= $quarterCouplingDispersion)) and " +
            "       m1.max_nesting_depth >= $shallowMaximumNestingDepth " +
            "RETURN " +
            "distinct id(c) as classId, " +
            "c.name as name, " +
            "count(m1) as count")
    List<MethodCodeSmellQueryResult> countIntensiveCouplingByClass(@Param("appId") Long appId,
                                                                   @Param("maxNumberOfShortMemoryCap") double maxNumberOfShortMemoryCap,
                                                                   @Param("halfCouplingDispersion") double halfCouplingDispersion,
                                                                   @Param("fewCouplingIntensity") double fewCouplingIntensity,
                                                                   @Param("quarterCouplingDispersion") double quarterCouplingDispersion,
                                                                   @Param("shallowMaximumNestingDepth") double shallowMaximumNestingDepth
    );

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(c:Class)-[:CLASS_OWNS_METHOD]->(m:Method)-[r:CALLS]->(other_method:Method) " +
            "WHERE " +
            "id(a) = $appId " +
            "WITH " +
            "c, " +
            "m, " +
            "COUNT(r) as number_of_called_methods " +
            "WHERE number_of_called_methods > $numberOfCalledMethods " +
            "RETURN " +
            "distinct id(c) as classId, " +
            "c.name as name, " +
            "count(m) as count")
    List<MethodCodeSmellQueryResult> countSchizophrenicMethodsByClass(@Param("appId") Long appId,
                                                                      @Param("numberOfCalledMethods") double numberOfCalledMethods);

    @Query("MATCH (app:App)-[:APP_OWNS_CLASS]->(c:Class)-[:CLASS_OWNS_METHOD]->(m:Method)-[r:METHOD_OWNS_ARGUMENT]->(a:Argument) " +
            "WITH " +
            "c, " +
            "m, " +
            "app, " +
            "count(a) as argument_count " +
            "WHERE " +
            "id(app) = $appId AND " +
            "argument_count > $numberOfParameters  " +
            "RETURN " +
            "distinct id(c) as classId, " +
            "c.name as name, " +
            "count(m) as count")
    List<MethodCodeSmellQueryResult> countLongParameterListByClass(@Param("appId") Long appId,
                                                                   @Param("numberOfParameters") double numberOfParameters);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(class:Class)-[:CLASS_OWNS_METHOD]->(m:Method)-[:USES]->(v:Variable) <-[:CLASS_OWNS_VARIABLE]-(other_class:Class) " +
            "WHERE " +
            "id(a) = $appId AND " +
            "class <> other_class " +
            "WITH " +
            "class, " +
            "m, " +
            "count(distinct v) as variable_count, " +
            "collect(distinct v.name) as names, " +
            "collect(distinct other_class.name) as class_names, " +
            "count(distinct other_class) as class_count " +
            "MATCH (class)-[:CLASS_OWNS_METHOD]->(m)-[:USES]->(v:Variable)<-[:CLASS_OWNS_VARIABLE]-(class) " +
            "WITH " +
            "class, " +
            "m, " +
            "variable_count, " +
            "class_names, " +
            "names, " +
            "count(distinct v) as local_variable_count, " +
            "collect(distinct v.name) as local_names, " +
            "class_count " +
            "WHERE " +
            "local_variable_count + variable_count > 0 " +
            "WITH " +
            "class, " +
            "m, " +
            "variable_count, " +
            "class_names, " +
            "names, " +
            "local_variable_count, " +
            "local_names, " +
            "class_count, " +
            "local_variable_count*1.0/(local_variable_count+variable_count) as locality " +
            "WHERE " +
            "variable_count > $fewAccessToForeignVariables and " +
            "locality < $localityFraction and " +
            "class_count <= $fewAccessToForeignClasses " +
            "RETURN " +
            "distinct id(class) as classId, " +
            "class.name as name, " +
            "count(m) as count")
    List<MethodCodeSmellQueryResult> countFeatureEnvyByClass(@Param("appId") Long appId,
                                                             @Param("fewAccessToForeignVariables") double fewAccessToForeignVariables,
                                                             @Param("localityFraction") double localityFraction,
                                                             @Param("fewAccessToForeignClasses") double fewAccessToForeignClasses);

    @Query("MATCH " +
            "(app:App)-[:APP_OWNS_MODULE]->(module:Module)-[:MODULE_OWNS_CLASS]->(class:Class) " +
            "-[:CLASS_OWNS_METHOD]->(method:Method)-[:METHOD_OWNS_ARGUMENT]->(argument:Argument) " +
            "MATCH " +
            "(app)-[:APP_OWNS_MODULE]->(module:Module)-[:MODULE_OWNS_CLASS]->(other_class:Class) " +
            "-[:CLASS_OWNS_METHOD]->(other_method:Method)-[:METHOD_OWNS_ARGUMENT]->(other_argument:Argument) " +
            "WHERE " +
            "method <> other_method and " +
            "argument.name = other_argument.name and " +
            "argument.type = other_argument.type " +
            "WITH " +
            "app, " +
            "class, " +
            "other_class, " +
            "method, " +
            "other_method, " +
            "argument " +
            "order by other_method.name " +
            "WITH " +
            "app, " +
            "class, " +
            "other_class, " +
            "method, " +
            "other_method, " +
            "argument " +
            "order by argument.name " +
            "WITH " +
            "collect(argument.name) as argument_names, " +
            "count(argument.name) as argument_count, " +
            "method, " +
            "other_method, " +
            "app, " +
            "class " +
            "WHERE " +
            "argument_count >= $numberOfRepeatingArguments " +
            "WITH " +
            "collect(other_method.name) + method.name as method_names, " +
            "collect(id(other_method)) + id(method) as method_ids, " +
            "count(distinct other_method) as method_count, " +
            "method, " +
            "app, " +
            "argument_names, " +
            "argument_count, " +
            "class " +
            "WITH " +
            "collect(class.name) as class_names, " +
            "method_names, " +
            "app, " +
            "argument_names, " +
            "argument_count, " +
            "method_ids, " +
            "method_count " +
            "MATCH " +
            " (app)-[:APP_OWNS_MODULE]->(:Module)-[:MODULE_OWNS_CLASS]->(class:Class) " +
            " -[:CLASS_OWNS_METHOD]->(method:Method)-[:METHOD_OWNS_ARGUMENT]->(argument:Argument) " +
            "WHERE " +
            "id(method) in method_ids and " +
            "argument.name in argument_names " +
            "WITH " +
            "argument, " +
            "app, " +
            "method, " +
            "argument_names, " +
            "argument_count, " +
            "class order by argument.name " +
            "WITH " +
            "collect(distinct argument.name) as new_argument_names, " +
            "app, " +
            "method, " +
            "argument_names, " +
            "argument_count, " +
            "class " +
            "WITH " +
            "collect(method.name) as new_method_names, " +
            "collect(class.name) as class_names, " +
            "new_argument_names, " +
            "app, " +
            "argument_names, " +
            "argument_count " +
            "RETURN " +
            "distinct id(class) as classId, " +
            "class.name as name, " +
            "count(method) as count")
    List<MethodCodeSmellQueryResult> countDataClumpsByClass(@Param("numberOfRepeatingArguments") double numberOfRepeatingArguments);

    @Query("MATCH  " +
            "(a:App)-[:APP_OWNS_CLASS]->(class)-[:CLASS_OWNS_METHOD]->(m:Method) " +
            "-[:METHOD_OWNS_ARGUMENT]->(p:Argument) " +
            "-[:IS_OF_TYPE]->(other_class:Class) " +
            "WHERE " +
            "id(a) = $appId AND " +
            "NOT (m)-[:CALLS|USES]->() " +
            "<-[:CLASS_OWNS_VARIABLE|CLASS_OWNS_METHOD]-(other_class) AND " +
            "NOT m.data_string contains (\"=\" + p.name) AND " +
            "NOT m.data_string contains (\"= \" + p.name) AND " +
            "NOT m.data_string contains (\":\" + p.name) AND " +
            "NOT m.data_string contains (\": \" + p.name) AND " +
            "NOT m.data_string contains (\"(\" + p.name + \")\") and " +
            "NOT m.data_string contains (\"(\" + p.name + \",\") and " +
            "NOT m.data_string contains (\"(\" + p.name + \" ,\") and " +
            "NOT m.data_string contains (\", \" + p.name + \")\") and " +
            "NOT m.data_string contains (\", \" + p.name + \",\") and " +
            "class.is_interface = false " +
            "RETURN " +
            "distinct id(class) as classId, " +
            "class.name as name, " +
            "count(m) as count")
    List<MethodCodeSmellQueryResult> countSpeculativeGeneralityByClass(@Param("appId") Long appId);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(class:Class)-[:CLASS_OWNS_METHOD]->(method:Method) " +
            "WHERE " +
            "id(a) = $appId AND " +
            "class.number_of_instructions > $highNumberOfInstructionsForClass and " +
            "method.cyclomatic_complexity >= $highCyclomaticComplexity and " +
            "method.max_nesting_depth >= $severalMaximalNestingDepth " +
            "MATCH (method)-[:USES]->(variable:Variable) " +
            "WITH " +
            "class, " +
            "method, " +
            "count(distinct variable) as number_of_variables, " +
            "collect(distinct variable.name) as variable_names " +
            "WHERE " +
            "number_of_variables > $manyAccessedVariables " +
            "RETURN " +
            "distinct id(class) as classId, " +
            "class.name as name, " +
            "count(method) as count")
    List<MethodCodeSmellQueryResult> countBrainMethodsByClass(@Param("appId") Long appId,
                                                              @Param("highNumberOfInstructionsForClass") double highNumberOfInstructionsForClass,
                                                              @Param("highCyclomaticComplexity") double highCyclomaticComplexity,
                                                              @Param("severalMaximalNestingDepth") double severalMaximalNestingDepth,
                                                              @Param("manyAccessedVariables") double manyAccessedVariables);
}
