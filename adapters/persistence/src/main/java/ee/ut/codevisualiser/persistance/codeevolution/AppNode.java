package ee.ut.codevisualiser.persistance.codeevolution;

import ee.ut.codevisualiser.persistance.codeanalysis.general.ClassNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.neo4j.ogm.annotation.*;

import java.util.List;

@NodeEntity(label = "App")
@Getter
@Builder
@Accessors(fluent = true)
@NoArgsConstructor
@AllArgsConstructor
public class AppNode {

    @Id
    @GeneratedValue
    private Long id;

    @Property("author")
    private String author;

    @Property("branch")
    private String branch;

    @Property("commit")
    private String commitHash;

    @Property("message")
    private String commitMessage;

    @Property("name")
    private String name;

    @Property("time")
    private String time;

    @Property("parent_commit")
    private String parentCommit;

    @Property("version_number")
    private long versionNumber;

    @Relationship(type = "APP_OWNS_CLASS")
    private List<ClassNode> classNodes;

}
