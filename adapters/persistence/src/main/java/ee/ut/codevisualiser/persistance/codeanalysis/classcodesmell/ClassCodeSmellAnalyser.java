package ee.ut.codevisualiser.persistance.codeanalysis.classcodesmell;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ClassCodeSmellAnalyser extends Neo4jRepository<List<ClassCodeSmellQueryResult>, Long> {

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(cl:Class) " +
            "WHERE" +
            "   id(a) = $appId AND " +
            "   cl.lack_of_cohesion_in_methods > $lack_of_cohesion_in_methods AND " +
            "   cl.number_of_methods >  $number_of_methods AND " +
            "   cl.number_of_attributes > $number_of_attributes " +
            "RETURN " +
            "  id(cl) as classId, " +
            "  cl.name as name")
    List<ClassCodeSmellQueryResult> getLargeClasses(@Param("appId") Long appId,
                                                    @Param("lack_of_cohesion_in_methods") double lackOfCohesionInMethods,
                                                    @Param("number_of_methods") double numberOfMethods,
                                                    @Param("number_of_attributes") double numberOfAttributes);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(cl:Class)-[r:CLASS_OWNS_METHOD]->(m:Method) " +
            "WHERE " +
            "id(a) = $appId AND " +
            "m.number_of_switch_statements > $numberOfSwitchStatements " +
            "RETURN " +
            "  id(cl) as classId, " +
            "  cl.name as name")
    List<ClassCodeSmellQueryResult> getClassesWithTooManySwitchStatements(@Param("appId") Long appId,
                                                                          @Param("numberOfSwitchStatements") double numberOfSwitchStatements);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(cl:Class) " +
            "WHERE " +
            "id(a) = $appId AND " +
            "(cl.number_of_methods = 0 OR " +
            "(cl.number_of_instructions < $mediumNumberOfInstructions AND  " +
            "cl.class_complexity/cl.number_of_methods <= $lowComplexityMethodRatio) OR " +
            "(cl.coupling_between_object_classes < $mediumCouplingBetweenObjectClasses AND " +
            "cl.depth_of_inheritance > $numberOfSomeDepthOfInheritance)) " +
            "RETURN " +
            "  id(cl) as classId, " +
            "  cl.name as name")
    List<ClassCodeSmellQueryResult> getLazyClasses(@Param("appId") Long appId,
                                                   @Param("mediumNumberOfInstructions") double mediumNumberOfInstructions,
                                                   @Param("lowComplexityMethodRatio") double lowComplexityMethodRatio,
                                                   @Param("mediumCouplingBetweenObjectClasses") double mediumCouplingBetweenObjectClasses,
                                                   @Param("numberOfSomeDepthOfInheritance") double numberOfSomeDepthOfInheritance);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(cl:Class) " +
            "WHERE " +
            "id(a) = $appId AND " +
            "cl.number_of_methods = 0 " +
            "RETURN " +
            "  id(cl) as classId, " +
            "  cl.name as name")
    List<ClassCodeSmellQueryResult> getDataClasses(@Param("appId") Long appId);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(cl:Class) " +
            "WHERE " +
            "id(a) = $appId AND " +
            "cl.number_of_comments = $numberOfComments " +
            "RETURN " +
            "  id(cl) as classId, " +
            "  cl.name as name")
    List<ClassCodeSmellQueryResult> getComments(@Param("appId") Long appId,
                                                @Param("numberOfComments") double numberOfComments);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(cl:Class)-[:CLASS_OWNS_VARIABLE]->(v:Variable)-[:IS_OF_TYPE]->(cl2:Class)  " +
            "WHERE " +
            "id(a) = $appId AND " +
            "cl <> cl2  " +
            "MATCH cyclePath=shortestPath((cl2)-[:CLASS_OWNS_VARIABLE|IS_OF_TYPE*]->(cl)) " +
            "WITH " +
            "cl, " +
            "v, " +
            "[n in nodes(cyclePath) | n.name ] as names " +
            "RETURN " +
            "  id(cl) as classId, " +
            "  cl.name as name")
    List<ClassCodeSmellQueryResult> getCyclicDependencies(@Param("appId") Long appId);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(cl:Class) " +
            "WHERE " +
            "id(a) = $appId AND " +
            "cl.depth_of_inheritance > $shortTermMemoryCap " +
            "RETURN " +
            "  id(cl) as classId, " +
            "  cl.name as name")
    List<ClassCodeSmellQueryResult> getDistortedHierarchyClasses(@Param("appId") Long appId,
                                                                 @Param("shortTermMemoryCap") double shortTermMemoryCap);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(cl:Class)-[r:EXTENDS]->(parent:Class) " +
            "WHERE " +
            "id(a) = $appId AND " +
            "NOT ()-[:EXTENDS]->(cl) AND " +
            "cl.number_of_methods + cl.number_of_attributes < $lowNumberOfMethodsAndAttributes AND " +
            "parent.number_of_methods + parent.number_of_attributes >= $highNumberOfMethodsAndAttributes " +
            "RETURN " +
            "  id(cl) as classId, " +
            "  cl.name as name")
    List<ClassCodeSmellQueryResult> getTraditionBreakerClasses(@Param("appId") Long appId,
                                                               @Param("lowNumberOfMethodsAndAttributes") double lowNumberOfMethodsAndAttributes,
                                                               @Param("highNumberOfMethodsAndAttributes") double highNumberOfMethodsAndAttributes);

    @Query("MATCH (firstClass:Class)-[:EXTENDS*]-> (parent:Class) <-[:EXTENDS*]-(secondClass:Class), " +
            "(firstClass:Class)-[:DUPLICATES]->(secondClass:Class) " +
            "WHERE firstClass.data_string contains d.fragment or " +
            "secondClass.data_string contains d.fragment " +
            "RETURN " +
            "  id(firstClass) as classId, " +
            "  firstClass.name as name")
    List<ClassCodeSmellQueryResult> getSiblingDuplicationClasses();

    @Query("MATCH (firstClass:Class)-[r:DUPLICATES]->(secondClass:Class), " +
            "(module:Module)-[:MODULE_OWNS_CLASS]->(firstClass), " +
            "(module:Module)-[:MODULE_OWNS_CLASS]->(secondClass) " +
            "WHERE firstClass.data_string contains r.fragment or " +
            "secondClass.data_string contains r.fragment " +
            "RETURN " +
            "  id(firstClass) as classId, " +
            "  firstClass.name as name")
    List<ClassCodeSmellQueryResult> getInternalDuplicationClasses();

    @Query("MATCH (firstClass:Class)-[:DUPLICATES]->(secondClass:Class), " +
            "(module:Module)-[:MODULE_OWNS_CLASS]->(firstClass), " +
            "(secondModule:Module)-[:MODULE_OWNS_CLASS]->(secondClass) " +
            "WHERE id(module) <> id(secondModule) and " +
            "firstClass.data_string contains d.fragment or " +
            "secondClass.data_string contains d.fragment " +
            "RETURN " +
            "  id(firstClass) as classId, " +
            "  firstClass.name as name")
    List<ClassCodeSmellQueryResult> getExternalDuplicationClasses();

    @Query("MATCH (app:App)-[:APP_OWNS_MODULE]->(module:Module)-[:MODULE_OWNS_CLASS] " +
            "->(class:Class)-[:CLASS_OWNS_VARIABLE]->(variable:Variable) " +
            "MATCH (app)-[:APP_OWNS_MODULE]->(module:Module)-[:MODULE_OWNS_CLASS] " +
            "->(other_class:Class)-[:CLASS_OWNS_VARIABLE]->(other_variable:Variable) " +
            "WHERE " +
            "id(app) = $appId AND " +
            "class <> other_class and variable.type = other_variable.type and variable.name = other_variable.name " +
            "WITH app, " +
            "class, " +
            "other_class, " +
            "variable order by variable.name " +
            "WITH app, " +
            "class, " +
            "other_class, " +
            "collect(distinct variable.name) as variable_names, " +
            "count(DISTINCT variable) as variable_count WITH app, " +
            "class, " +
            "other_class, " +
            "variable_names, " +
            "variable_count order by id(class) WITH app, " +
            "collect(distinct class.name) as class_names, " +
            "class, " +
            "variable_names, " +
            "variable_count " +
            "WHERE variable_count >= $numberOfRepeatingVariables " +
            "RETURN " +
            "  id(class) as classId, " +
            "  class.name as name")
    List<ClassCodeSmellQueryResult> getDataClumpsClasses(@Param("appId") Long appId,
                                                         @Param("numberOfRepeatingVariables") double numberOfRepeatingVariables);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(cl:Class) " +
            "WHERE " +
            "id(a) = $appId AND " +
            "()-[:IMPLEMENTS|EXTENDS]->(cl) and " +
            "cl.is_interface = true " +
            "RETURN " +
            "  id(cl) as classId, " +
            "  cl.name as name")
    List<ClassCodeSmellQueryResult> getSpeculativeGeneralityInterfaces(@Param("appId") Long appId);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(class:Class)-[:CLASS_OWNS_METHOD]->(method:Method)-[:USES|CALLS]->(ref)<-[:CLASS_OWNS_VARIABLE|CLASS_OWNS_METHOD]-(other_class:Class) " +
            "WHERE " +
            "id(a) = $appId AND " +
            "class <> other_class and " +
            "method.number_of_instructions < $numberOfInstructionsMethod " +
            "WITH " +
            "class, " +
            "method, " +
            "collect(ref.name) as referenced_names, " +
            "collect(other_class.name) as class_names " +
            "WITH " +
            "collect(method.name) as method_names, " +
            "collect(referenced_names) as references, " +
            "collect(class_names) as classes, " +
            "collect(method.number_of_instructions) as " +
            "numbers_of_instructions, " +
            "class, " +
            "count(method) as method_count, " +
            "count(method)*1.0/class.number_of_methods as method_ratio " +
            "WHERE " +
            "method_ratio > $delegationToAllMethodsRatioHalf " +
            "RETURN " +
            "  id(class) as classId, " +
            "  class.name as name")
    List<ClassCodeSmellQueryResult> getMiddleManClasses(@Param("appId") Long appId,
                                                        @Param("numberOfInstructionsMethod") double numberOfInstructionsMethod,
                                                        @Param("delegationToAllMethodsRatioHalf") double delegationToAllMethodsRatioHalf);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(class:Class)-[:CLASS_OWNS_METHOD]->(method:Method) " +
            "MATCH (class)-[:CLASS_OWNS_METHOD]->(other_method:Method) " +
            "WHERE " +
            "id(a) = $appId AND " +
            "method <> other_method " +
            "WITH " +
            "count(DISTINCT [method, other_method]) as pair_count, " +
            "class " +
            "MATCH " +
            "(class)-[:CLASS_OWNS_METHOD]->(method:Method) " +
            "MATCH " +
            "(class)-[:CLASS_OWNS_METHOD]->(other_method:Method) " +
            "MATCH " +
            "(class)-[:CLASS_OWNS_VARIABLE]->(variable:Variable) " +
            "WHERE " +
            "method <> other_method and " +
            "(method)-[:USES]->(variable)<-[:USES]-(other_method) " +
            "WITH " +
            "class, " +
            "pair_count, " +
            "method, " +
            "other_method, " +
            "collect(distinct variable.name) as variable_names, " +
            "count(distinct variable) as variable_count " +
            "WHERE " +
            "variable_count >= 1 " +
            "WITH " +
            "class, " +
            "pair_count, " +
            "count(distinct [method, other_method]) as connected_method_count " +
            "WITH " +
            "class, " +
            "connected_method_count*0.1/pair_count as class_cohesion, " +
            "connected_method_count, " +
            "pair_count " +
            "WHERE " +
            "class_cohesion < $tightClassCohesionFraction and " +
            "class.class_complexity >= $veryHighWeightedMethodCount " +
            "OPTIONAL MATCH " +
            "(class)-[:CLASS_OWNS_METHOD]->(m:Method)-[:USES] " +
            "->(variable:Variable)<-[:CLASS_OWNS_VARIABLE]-(other_class:Class) " +
            "WHERE " +
            "class <> other_class " +
            "WITH " +
            "class, " +
            "    class_cohesion, " +
            "    connected_method_count, " +
            "    pair_count, " +
            "    count(distinct variable) as foreign_variable_count " +
            "WHERE " +
            "foreign_variable_count >= $fewAccessToForeignData " +
            "RETURN " +
            "  id(class) as classId, " +
            "  class.name as name")
    List<ClassCodeSmellQueryResult> getGodClasses(@Param("appId") Long appId,
                                                  @Param("tightClassCohesionFraction") double tightClassCohesionFraction,
                                                  @Param("veryHighWeightedMethodCount") double veryHighWeightedMethodCount,
                                                  @Param("fewAccessToForeignData") double fewAccessToForeignData);


    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(cl:Class) " +
            "WHERE " +
            "id(a) = $appId AND " +
            "cl.is_interface AND " +
            "cl.number_of_methods > $veryHighNumberOfMethods " +
            "RETURN " +
            "  id(cl) as classId, " +
            "  cl.name as name")
    List<ClassCodeSmellQueryResult> getSwissArmyKnifeClasses(@Param("appId") Long appId,
                                                             @Param("veryHighNumberOfMethods") double veryHighNumberOfMethods);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(cl:Class) " +
            "WHERE " +
            "id(a) = $appId AND " +
            "cl.class_complexity > $veryHighClassComplexity " +
            "RETURN " +
            "  id(cl) as classId, " +
            "  cl.name as name")
    List<ClassCodeSmellQueryResult> getComplexClasses(@Param("appId") Long appId,
                                                      @Param("veryHighClassComplexity") double veryHighClassComplexity);
}
