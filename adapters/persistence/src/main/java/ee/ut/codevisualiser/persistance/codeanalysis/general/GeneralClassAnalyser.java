package ee.ut.codevisualiser.persistance.codeanalysis.general;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface GeneralClassAnalyser extends Neo4jRepository<List<GeneralClassAnalyserQueryResult>, Long> {

    @Query("MATCH (a:App)-[r:APP_OWNS_CLASS]->(c:Class)-[:CLASS_OWNS_METHOD]->(m:Method) " +
            "WHERE id(a) = $appId " +
            "RETURN DISTINCT id(c) AS id, c.name AS name, c.path AS path, count(distinct m) as methods " +
            "ORDER BY c.name")
    List<GeneralClassAnalyserQueryResult> getGeneralAnalysis(@Param("appId") Long appId);

    @Query("MATCH (a:App) WHERE a.branch = $branch and a.name = $appName return id(a) order by a.time desc limit 1")
    long getLatestAppVersionId(@Param("branch") String branch, @Param("appName") String appName);

    @Query("MATCH (a:App) WHERE a.name = $appName return id(a) order by a.time desc limit 1")
    long getLatestAppVersionId(@Param("appName") String appName);
}
