package ee.ut.codevisualiser.persistance.upload;

import ee.ut.codevisualiser.application.usecase.upload.AppGateway;
import ee.ut.codevisualiser.common.PersistenceAdapter;

import javax.transaction.Transactional;
import java.util.List;

@PersistenceAdapter
@Transactional
public class AppAdapter implements AppGateway {

    private final Uploads uploads;

    public AppAdapter(Uploads uploads) {
        this.uploads = uploads;
    }

    @Override
    public List<String> getExistingApplications() {
        return uploads.findAllExistingApps();
    }
}
