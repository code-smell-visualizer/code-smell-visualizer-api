package ee.ut.codevisualiser.persistance.codesmellimpact;

import ee.ut.codevisualiser.application.domain.codesmell.CodeSmell;
import ee.ut.codevisualiser.application.domain.codesmell.CodeSmellParameter;
import ee.ut.codevisualiser.application.domain.codesmellimpact.CodeSmellImpact;
import ee.ut.codevisualiser.application.usecase.codesmellimpact.CodeSmellImpactGateway;
import ee.ut.codevisualiser.common.PersistenceAdapter;
import ee.ut.codevisualiser.persistance.codeanalysis.general.GeneralClassAnalyser;
import ee.ut.codevisualiser.persistance.codeanalysis.general.GeneralClassAnalyserQueryResult;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@PersistenceAdapter
@Transactional
public class CodeSmellImpactAdapter implements CodeSmellImpactGateway {

    private final CodeSmellImpacts codeSmellImpacts;
    private final GeneralClassAnalyser generalClassAnalyser;
    private List<CodeSmellImpact> result;
    private Long appId;
    private Integer nrOfClassesTotal;
    private Integer nrOfMethodsTotal;

    public CodeSmellImpactAdapter(CodeSmellImpacts codeSmellImpacts, GeneralClassAnalyser generalClassAnalyser) {
        this.codeSmellImpacts = codeSmellImpacts;
        this.generalClassAnalyser = generalClassAnalyser;
    }

    @Override
    public List<CodeSmellImpact> getCodeSmellImpacts(String branch, String appName, List<CodeSmell> codeSmells) {
        result = new ArrayList<>();
        initializeAppId(branch, appName);
        initializeNrOfClasses();
        codeSmells.forEach(this::updateResultWithCodeSmellImpact);
        return result;
    }

    private void initializeAppId(String branch, String appName) {
        try {
            appId = generalClassAnalyser.getLatestAppVersionId(branch, appName);
        } catch (Exception e) {
            appId = generalClassAnalyser.getLatestAppVersionId(appName);
        }
    }

    private void initializeNrOfClasses() {
        List<GeneralClassAnalyserQueryResult> result = generalClassAnalyser.getGeneralAnalysis(appId);
        nrOfClassesTotal = result.size();
        nrOfMethodsTotal = result.stream().map(GeneralClassAnalyserQueryResult::methods).reduce(0, Integer::sum);

    }

    private void updateResultWithCodeSmellImpact(CodeSmell codeSmell) {
        switch (codeSmell.name()) {
            case "Long method": getLongMethodCodeAnalysis(codeSmell.name(), codeSmell.parameters());
                break;
            case "Large class / Blob class": getLargeClassCodeAnalysis(codeSmell.name(), codeSmell.parameters());
                break;
            case "Shotgun surgery": getShotgunSurgeryCodeAnalysis(codeSmell.name(), codeSmell.parameters());
                break;
            case "Switch statements": getSwitchStatementsCodeAnalysis(codeSmell.name(), codeSmell.parameters());
                break;
            case "Lazy class": getLazyClassCodeAnalysis(codeSmell.name(), codeSmell.parameters());
                break;
            case "Message chains": getMessageChainsCodeAnalysis(codeSmell.name(), codeSmell.parameters());
                break;
            case "Data class": getDataClassCodeAnalysis(codeSmell.name());
                break;
            case "Comments": getCommentsCodeAnalysis(codeSmell.name(), codeSmell.parameters());
                break;
            case "Cyclic dependencies (dependencies between classes)": getCyclicDependenciesCodeAnalysis(codeSmell.name());
                break;
            case "Intensive coupling": getIntensiveCouplingCodeAnalysis(codeSmell.name(), codeSmell.parameters());
                break;
            case "Distorted hierarchy": getDistortedHierarchyCodeAnalysis(codeSmell.name(), codeSmell.parameters());
                break;
            case "Tradition Breaker": getTraditionBreakerCodeAnalysis(codeSmell.name(), codeSmell.parameters());
                break;
            case "Divergent Change/Schizophrenic Class": getSchizophrenicClassCodeAnalysis(codeSmell.name(), codeSmell.parameters());
                break;
            case "Long parameter list": getLongParameterListCodeAnalysis(codeSmell.name(), codeSmell.parameters());
                break;
            case "Feature envy": getFeatureEnvyCodeAnalysis(codeSmell.name(), codeSmell.parameters());
                break;
            case "Data Data clumps (class variables)": getClassVariableDataClumpsCodeAnalysis(codeSmell.name(), codeSmell.parameters());
                break;
            case "Data clumps (function arguments)": getMethodDataClumpsCodeAnalysis(codeSmell.name(), codeSmell.parameters());
                break;
            case "Speculative generality (interfaces)": getSpeculativeGeneralityClassCodeAnalysis(codeSmell.name());
                break;
            case "Speculative generality (methods)": getSpeculativeGeneralityMethodCodeAnalysis(codeSmell.name());
                break;
            case "Middle man": getMiddleManCodeAnalysis(codeSmell.name(), codeSmell.parameters());
                break;
            case "Brain method": getBrainMethodCodeAnalysis(codeSmell.name(), codeSmell.parameters());
                break;
            case "God class": getGodClassCodeAnalysis(codeSmell.name(), codeSmell.parameters());
                break;
            case "Swiss army knife": getSwissArmyKnifeCodeAnalysis(codeSmell.name(), codeSmell.parameters());
                break;
            case "Complex class": getComplexClassCodeAnalysis(codeSmell.name(), codeSmell.parameters());
                break;
        }
    }


    private void getLongMethodCodeAnalysis(String name, List<CodeSmellParameter> parameters) {
        double numberOfInstructions = getParameterValueByName("nr of instructions", parameters);
        result.add(CodeSmellImpact.builder()
                .codeSmellName(name)
                .impact(getMethodsImpact(codeSmellImpacts.countLongMethod(appId, numberOfInstructions)))
                .build());
    }

    private void getLargeClassCodeAnalysis(String name, List<CodeSmellParameter> parameters) {
        double lackOfCohesionInMethods = getParameterValueByName("cohesion in methods", parameters);
        double numberOfMethods = getParameterValueByName("nr of methods", parameters);
        double numberOfAttributes = getParameterValueByName("nr of attributes", parameters);
        result.add(CodeSmellImpact.builder()
                .codeSmellName(name)
                .impact(getClassesImpact(codeSmellImpacts.countLargeClasses(
                        appId,
                        lackOfCohesionInMethods,
                        numberOfMethods,
                        numberOfAttributes)))
                .build());
    }

    private void getShotgunSurgeryCodeAnalysis(String name, List<CodeSmellParameter> parameters) {
        double numberOfCallers = getParameterValueByName("nr of callers", parameters);
        result.add(CodeSmellImpact.builder()
                .codeSmellName(name)
                .impact(getMethodsImpact(codeSmellImpacts.countShotgunSurgery(appId, numberOfCallers)))
                .build());
    }

    private void getSwitchStatementsCodeAnalysis(String name, List<CodeSmellParameter> parameters) {
        double numberOfSwitchStatements = getParameterValueByName("nr of statements", parameters);
        result.add(CodeSmellImpact.builder()
                .codeSmellName(name)
                .impact(getClassesImpact(codeSmellImpacts.countClassesWithTooManySwitchStatements(
                        appId,
                        numberOfSwitchStatements)))
                .build());
    }

    private void getLazyClassCodeAnalysis(String name, List<CodeSmellParameter> parameters) {
        double medianNumberOfInstructions = getParameterValueByName("median nr of instructions", parameters);
        double lowComplexityMethodRatio = getParameterValueByName("complexity-method ratio", parameters);
        double medianCouplingBetweenObjectClasses = getParameterValueByName("coupling between classes", parameters);
        double numberOfSomeDepthOfInheritance = getParameterValueByName("nr of depth of inheritance", parameters);
        result.add(CodeSmellImpact.builder()
                .codeSmellName(name)
                .impact(getClassesImpact(codeSmellImpacts.countLazyClasses(
                        appId,
                        medianNumberOfInstructions,
                        lowComplexityMethodRatio,
                        medianCouplingBetweenObjectClasses,
                        numberOfSomeDepthOfInheritance)))
                .build());
    }

    private void getMessageChainsCodeAnalysis(String name, List<CodeSmellParameter> parameters) {
        double numberOfChainedMessages = getParameterValueByName("nr of chained messages", parameters);
        result.add(CodeSmellImpact.builder()
                .codeSmellName(name)
                .impact(getMethodsImpact(codeSmellImpacts.countMessageChains(appId, numberOfChainedMessages)))
                .build());
    }

    private void getDataClassCodeAnalysis(String name) {
        result.add(CodeSmellImpact.builder()
                .codeSmellName(name)
                .impact(getClassesImpact(codeSmellImpacts.countDataClasses(appId)))
                .build());
    }

    private void getCommentsCodeAnalysis(String name, List<CodeSmellParameter> parameters) {
        double numberOfComments = getParameterValueByName("nr of comments", parameters);
        result.add(CodeSmellImpact.builder()
                .codeSmellName(name)
                .impact(getClassesImpact(codeSmellImpacts.countComments(appId, numberOfComments)))
                .build());
    }

    private void getCyclicDependenciesCodeAnalysis(String name) {
        result.add(CodeSmellImpact.builder()
                .codeSmellName(name)
                .impact(getClassesImpact(codeSmellImpacts.countCyclicDependencies(appId)))
                .build());
    }

    private void getIntensiveCouplingCodeAnalysis(String name, List<CodeSmellParameter> parameters) {
        double maxNumberOfShortMemoryCap = getParameterValueByName("short-term memory cap", parameters);
        double halfCouplingDispersion = getParameterValueByName("half coupling dispersion", parameters);
        double fewCouplingIntensity = getParameterValueByName("few coupling intensity", parameters);
        double quarterCouplingDispersion = getParameterValueByName("quarter coupling dispersion", parameters);
        double shallowMaximumNestingDepth = getParameterValueByName("maximum nesting depth", parameters);
        result.add(CodeSmellImpact.builder()
                .codeSmellName(name)
                .impact(getMethodsImpact(codeSmellImpacts.countIntensiveCoupling(
                        appId,
                        maxNumberOfShortMemoryCap,
                        halfCouplingDispersion,
                        fewCouplingIntensity,
                        quarterCouplingDispersion,
                        shallowMaximumNestingDepth)))
                .build());
    }

    private void getDistortedHierarchyCodeAnalysis(String name, List<CodeSmellParameter> parameters) {
        double shortTermMemoryCap = getParameterValueByName("short term memory cap", parameters);
        result.add(CodeSmellImpact.builder()
                .codeSmellName(name)
                .impact(getClassesImpact(codeSmellImpacts.countDistortedHierarchy(appId, shortTermMemoryCap)))
                .build());
    }

    private void getTraditionBreakerCodeAnalysis(String name, List<CodeSmellParameter> parameters) {
        double lowNumberOfMethodsAndAttributes = getParameterValueByName("low nr of methods/attributes", parameters);
        double highNumberOfMethodsAndAttributes = getParameterValueByName("high nr of methods/attributes", parameters);
        result.add(CodeSmellImpact.builder()
                .codeSmellName(name)
                .impact(getClassesImpact(codeSmellImpacts.countTraditionBreaker(
                        appId,
                        lowNumberOfMethodsAndAttributes,
                        highNumberOfMethodsAndAttributes)))
                .build());
    }

    private void getSiblingDuplicationCodeAnalysis() {
        /*
        addClassCodeSmellScores(classCodeSmellAnalyser.getSiblingDuplicationClasses());
         */ // TODO 22/02/2021: d is undefined
    }

    private void getInternalDuplicationCodeAnalysis() {
        /*
        addClassCodeSmellScores(classCodeSmellAnalyser.getInternalDuplicationClasses());
         */ // TODO 22/02/2021: r is undefined
    }

    private void getExternalDuplicationCodeAnalysis() {
        /*
        addClassCodeSmellScores(classCodeSmellAnalyser.getExternalDuplicationClasses());
         */ // TODO 22/02/2021: r is undefined
    }

    private void getSchizophrenicClassCodeAnalysis(String name, List<CodeSmellParameter> parameters) {
        double numberOfCalledMethods = getParameterValueByName("nr of called methods", parameters);
        result.add(CodeSmellImpact.builder()
                .codeSmellName(name)
                .impact(getMethodsImpact(codeSmellImpacts.countSchizophrenicMethods(appId, numberOfCalledMethods)))
                .build());
    }

    private void getLongParameterListCodeAnalysis(String name, List<CodeSmellParameter> parameters) {
        double numberOfParameters = getParameterValueByName("nr of parameters", parameters);
        result.add(CodeSmellImpact.builder()
                .codeSmellName(name)
                .impact(getMethodsImpact(codeSmellImpacts.countLongParameterList(appId, numberOfParameters)))
                .build());
    }

    private void getFeatureEnvyCodeAnalysis(String name, List<CodeSmellParameter> parameters) {
        double fewAccessToForeignVariables = getParameterValueByName("access to foreign variables", parameters);
        double localityFraction = getParameterValueByName("locality fraction", parameters);
        double fewAccessToForeignClasses = getParameterValueByName("access to foreign classes", parameters);
        result.add(CodeSmellImpact.builder()
                .codeSmellName(name)
                .impact(getMethodsImpact(codeSmellImpacts.countFeatureEnvyByClass(
                        appId,
                        fewAccessToForeignVariables,
                        localityFraction,
                        fewAccessToForeignClasses)))
                .build());
    }

    private void getClassVariableDataClumpsCodeAnalysis(String name, List<CodeSmellParameter> parameters) {
        double numberOfRepeatingVariables = getParameterValueByName("nr of repeating variables", parameters);
        result.add(CodeSmellImpact.builder()
                .codeSmellName(name)
                .impact(getClassesImpact(codeSmellImpacts.countDataClumpsClasses(appId, numberOfRepeatingVariables)))
                .build());
    }

    private void getMethodDataClumpsCodeAnalysis(String name, List<CodeSmellParameter> parameters) {
        /* TODO: not working atm
        int numberOfRepeatingArguments = parameters.get(0);
        addMethodCodeSmellScores(methodCodeSmellAnalyser.countDataClumpsByClass(numberOfRepeatingArguments));
         */
    }

    private void getSpeculativeGeneralityClassCodeAnalysis(String name) {
        result.add(CodeSmellImpact.builder()
                .codeSmellName(name)
                .impact(getClassesImpact(codeSmellImpacts.countSpeculativeGeneralityInterfaces(appId)))
                .build());
    }

    private void getSpeculativeGeneralityMethodCodeAnalysis(String name) {
        result.add(CodeSmellImpact.builder()
                .codeSmellName(name)
                .impact(getMethodsImpact(codeSmellImpacts.countSpeculativeGenerality(appId)))
                .build());
    }

    private void getMiddleManCodeAnalysis(String name, List<CodeSmellParameter> parameters) {
        double numberOfInstructionsMethod = getParameterValueByName("nr of instructions method", parameters);
        double delegationToAllMethodsRatioHalf = getParameterValueByName("methods delegation ratio", parameters);
        result.add(CodeSmellImpact.builder()
                .codeSmellName(name)
                .impact(getClassesImpact(codeSmellImpacts.countMiddleMan(
                        appId,
                        numberOfInstructionsMethod,
                        delegationToAllMethodsRatioHalf)))
                .build());
    }

    private void getBrainMethodCodeAnalysis(String name, List<CodeSmellParameter> parameters) {
        double highNumberOfInstructionsForClass = getParameterValueByName("instructions for class", parameters);
        double highCyclomaticComplexity = getParameterValueByName("cyclomatic complexity", parameters);
        double severalMaximalNestingDepth = getParameterValueByName("maximal nesting depth", parameters);
        double manyAccessedVariables = getParameterValueByName("accessed variables", parameters);
        result.add(CodeSmellImpact.builder()
                .codeSmellName(name)
                .impact(getMethodsImpact(codeSmellImpacts.countBrainMethodsByClass(
                        appId,
                        highNumberOfInstructionsForClass,
                        highCyclomaticComplexity,
                        severalMaximalNestingDepth,
                        manyAccessedVariables)))
                .build());
    }

    private void getGodClassCodeAnalysis(String name, List<CodeSmellParameter> parameters) {
        double tightClassCohesionFraction = getParameterValueByName("class cohesion fraction", parameters);
        double veryHighWeightedMethodCount = getParameterValueByName("weighted method count", parameters);
        double fewAccessToForeignData = getParameterValueByName("access to foreign data", parameters);
        result.add(CodeSmellImpact.builder()
                .codeSmellName(name)
                .impact(getClassesImpact(codeSmellImpacts.countGodClasses(
                        appId,
                        tightClassCohesionFraction,
                        veryHighWeightedMethodCount,
                        fewAccessToForeignData)))
                .build());
    }

    private void getSwissArmyKnifeCodeAnalysis(String name, List<CodeSmellParameter> parameters) {
        double veryHighNumberOfMethods = getParameterValueByName("nr of methods", parameters);
        result.add(CodeSmellImpact.builder()
                .codeSmellName(name)
                .impact(getClassesImpact(codeSmellImpacts.countSwissArmyKnifeClasses(appId, veryHighNumberOfMethods)))
                .build());
    }

    private void getComplexClassCodeAnalysis(String name, List<CodeSmellParameter> parameters) {
        double veryHighClassComplexity = getParameterValueByName("class complexity", parameters);
        result.add(CodeSmellImpact.builder()
                .codeSmellName(name)
                .impact(getClassesImpact(codeSmellImpacts.countComplexClasses(appId, veryHighClassComplexity)))
                .build());
    }

    private double getMethodsImpact(Double input) {
        return (double) input / nrOfMethodsTotal * 100;
    }

    private double getClassesImpact(Double input) {
        return (double) input / nrOfClassesTotal * 100;
    }


    private double getParameterValueByName(String name, List<CodeSmellParameter> parameters) {
        return parameters.stream()
                .filter(param -> param.name().equals(name))
                .findFirst().orElseThrow(RuntimeException::new)
                .value();
    }
}
