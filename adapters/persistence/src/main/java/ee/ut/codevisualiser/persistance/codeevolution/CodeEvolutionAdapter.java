package ee.ut.codevisualiser.persistance.codeevolution;

import ee.ut.codevisualiser.application.domain.codeevolution.App;
import ee.ut.codevisualiser.application.usecase.codeevolution.CodeEvolutionGateway;
import ee.ut.codevisualiser.common.PersistenceAdapter;

import javax.transaction.Transactional;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@PersistenceAdapter
@Transactional
public class CodeEvolutionAdapter implements CodeEvolutionGateway {

    private final CodeEvolution codeEvolution;

    public CodeEvolutionAdapter(CodeEvolution codeEvolution) {
        this.codeEvolution = codeEvolution;
    }

    @Override
    public List<App> getCommits(String branch) {
        return toAppList(codeEvolution.findAllByBranchOrderByTimeDesc(branch));
    }

    @Override
    public List<String> getAllBranches() {
        return codeEvolution.getAllBranches();
    }

    private List<App> toAppList(List<AppNode> appNodes) {
        return appNodes.stream().map(this::toApp).collect(Collectors.toList());
    }

    private App toApp(AppNode appNode) {
        return App.builder()
                .id(appNode.id())
                .author(appNode.author())
                .branch(appNode.branch())
                .commitHash(appNode.commitHash())
                .commitMessage(appNode.commitMessage())
                .name(appNode.name())
                .time(getDate(appNode.time())) // TODO 14/03/2021: FIX
                .versionNumber(appNode.versionNumber())
                .build();
    }

    private Date getDate(String time) {
        DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy", Locale.ENGLISH);
        try {
            return dateFormat.parse(time.substring(0, time.length() - 6));
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
