package ee.ut.codevisualiser.persistance.codeanalysis.classusage;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ClassUsageAnalyser extends Neo4jRepository<List<ClassUsageQueryResult>, Long> {

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(c1:Class)-[:CLASS_OWNS_METHOD]->(m:Method)-[:USES|CALLS]->(ref)<-" +
           "[:CLASS_OWNS_VARIABLE|CLASS_OWNS_METHOD]-(c:Class)<-[:APP_OWNS_CLASS]-(a1:App) " +
            "WHERE id(a) = $appId AND id(a1) = $appId AND c <> c1 " +
            "RETURN id(c) as classId, c.name as name, COUNT(m) as count")
    List<ClassUsageQueryResult> getClassUsageCount(@Param("appId") Long appId);
}
