package ee.ut.codevisualiser.persistance.codesmell;

import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CodeSmells extends Neo4jRepository<CodeSmellNode, Long> {

    List<CodeSmellNode> findAllByAppId(@Param("app_id") Long appId);
}
