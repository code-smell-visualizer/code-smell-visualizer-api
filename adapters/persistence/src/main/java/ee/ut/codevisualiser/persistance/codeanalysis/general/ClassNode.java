package ee.ut.codevisualiser.persistance.codeanalysis.general;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.neo4j.ogm.annotation.*;

import java.util.List;

@NodeEntity(label = "Class")
@Getter
@Builder
@Accessors(fluent = true)
@NoArgsConstructor
@AllArgsConstructor
public class ClassNode {

    @Id
    @GeneratedValue
    private Long id;

    @Property("code")
    private String code;

    @Property("kind")
    private String kind;

    @Property("name")
    private String name;

    @Property("path")
    private String path;

    @Property("usr")
    private String usr;

    @Property("version_number")
    private Long versionNr;

    @Relationship(type = "CLASS_OWNS_METHOD")
    private List<MethodNode> methods;
}
