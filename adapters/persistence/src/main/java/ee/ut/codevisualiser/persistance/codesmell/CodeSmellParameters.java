package ee.ut.codevisualiser.persistance.codesmell;

import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface CodeSmellParameters extends Neo4jRepository<ParameterNode, Long> {
}
