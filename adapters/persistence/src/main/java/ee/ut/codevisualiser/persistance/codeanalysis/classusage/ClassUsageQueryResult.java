package ee.ut.codevisualiser.persistance.codeanalysis.classusage;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.neo4j.annotation.QueryResult;

@Getter
@Builder
@Accessors(fluent = true)
@NoArgsConstructor
@AllArgsConstructor
@QueryResult
public class ClassUsageQueryResult {
    Long classId;
    String name;
    int count;
}
