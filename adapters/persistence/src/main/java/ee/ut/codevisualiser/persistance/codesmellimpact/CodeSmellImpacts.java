package ee.ut.codevisualiser.persistance.codesmellimpact;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

public interface CodeSmellImpacts extends Neo4jRepository<Double, Long> {

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(c:Class)-[r:CLASS_OWNS_METHOD]->(m:Method) " +
            "WHERE " +
            "   id(a) = $appId AND " +
            "   m.number_of_instructions > $numberOfInstructions " +
            "RETURN " +
            "   count(distinct m) as nr_of_smells")
    Double countLongMethod(@Param("appId") Long appId,
                           @Param("numberOfInstructions") double numberOfInstructions);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(cl:Class) " +
            "WHERE" +
            "   id(a) = $appId AND " +
            "   cl.lack_of_cohesion_in_methods > $lack_of_cohesion_in_methods AND " +
            "   cl.number_of_methods >  $number_of_methods AND " +
            "   cl.number_of_attributes > $number_of_attributes " +
            "RETURN " +
            "   count(distinct cl) as nr_of_smells")
    Double countLargeClasses(@Param("appId") Long appId,
                                                    @Param("lack_of_cohesion_in_methods") double lackOfCohesionInMethods,
                                                    @Param("number_of_methods") double numberOfMethods,
                                                    @Param("number_of_attributes") double numberOfAttributes);

    @Query("MATCH (other_m:Method)-[r:CALLS]->(m:Method)<-[:CLASS_OWNS_METHOD]-(c:Class)<-[:APP_OWNS_CLASS]-(a:App) " +
            "WHERE " +
            "   id(a) = $appId " +
            "WITH " +
            "   c, " +
            "   m, " +
            "   COUNT(r) as number_of_callers " +
            "WHERE " +
            "   number_of_callers > $numberOfCallers " +
            "RETURN " +
            "   count(distinct m) as count")
    Double countShotgunSurgery(@Param("appId") Long appId, @Param("numberOfCallers") double numberOfCallers);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(cl:Class)-[r:CLASS_OWNS_METHOD]->(m:Method) " +
            "WHERE " +
            "   id(a) = $appId AND " +
            "   m.number_of_switch_statements > $numberOfSwitchStatements " +
            "RETURN " +
            "   count(distinct cl) as nr_of_smells")
    Double countClassesWithTooManySwitchStatements(
            @Param("appId") Long appId,
            @Param("numberOfSwitchStatements") double numberOfSwitchStatements);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(cl:Class) " +
            "WHERE " +
            "   id(a) = $appId AND " +
            "   (cl.number_of_methods = 0 OR " +
            "   (cl.number_of_instructions < $mediumNumberOfInstructions AND  " +
            "   cl.class_complexity/cl.number_of_methods <= $lowComplexityMethodRatio) OR " +
            "   (cl.coupling_between_object_classes < $mediumCouplingBetweenObjectClasses AND " +
            "   cl.depth_of_inheritance > $numberOfSomeDepthOfInheritance)) " +
            "RETURN " +
            "   count(distinct cl) as nr_of_smells")
    Double countLazyClasses(
            @Param("appId") Long appId,
            @Param("mediumNumberOfInstructions") double mediumNumberOfInstructions,
            @Param("lowComplexityMethodRatio") double lowComplexityMethodRatio,
            @Param("mediumCouplingBetweenObjectClasses") double mediumCouplingBetweenObjectClasses,
            @Param("numberOfSomeDepthOfInheritance") double numberOfSomeDepthOfInheritance);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(c:Class)-[CLASS_OWNS_METHOD]-(m:Method) " +
            "WHERE " +
            "   id(a) = $appId AND " +
            "   m.max_number_of_chaned_message_calls > $numberOfChainedMessages " +
            "RETURN " +
            "   count(m) as nr_of_smells")
    Double countMessageChains(@Param("appId") Long appId,
                              @Param("numberOfChainedMessages") double numberOfChainedMessages);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(cl:Class) " +
            "WHERE " +
            "   id(a) = $appId AND " +
            "   cl.number_of_methods = 0 " +
            "RETURN " +
            "   count(distinct cl) as nr_of_smells")
    Double countDataClasses(@Param("appId") Long appId);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(cl:Class) " +
            "WHERE " +
            "   id(a) = $appId AND " +
            "   cl.number_of_comments = $numberOfComments " +
            "RETURN " +
            "   count(distinct cl) as nr_of_smells")
    Double countComments(@Param("appId") Long appId, @Param("numberOfComments") double numberOfComments);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(cl:Class)-[:CLASS_OWNS_VARIABLE]->(v:Variable)-[:IS_OF_TYPE]->(cl2:Class)  " +
            "WHERE " +
            "   id(a) = $appId AND " +
            "   cl <> cl2  " +
            "MATCH cyclePath=shortestPath((cl2)-[:CLASS_OWNS_VARIABLE|IS_OF_TYPE*]->(cl)) " +
            "WITH " +
            "   cl, " +
            "   v, " +
            "   [n in nodes(cyclePath) | n.name ] as names " +
            "RETURN " +
            "   count(distinct cl) as nr_of_smells")
    Double countCyclicDependencies(@Param("appId") Long appId);

    @Query("MATCH " +
            "(a:App)-[:APP_OWNS_CLASS]->(c:Class)-[r:CLASS_OWNS_METHOD]->(m1:Method)-[s:CALLS]->(m2:Method), " +
            "(c2:Class)-[r2:CLASS_OWNS_METHOD]->(m2) " +
            "WHERE " +
            "   id(a) = $appId AND " +
            "   id(c) <> id(c2) " +
            "WITH " +
            "   c, " +
            "   m1, " +
            "   count(distinct m2) as method_count, " +
            "   collect(distinct m2.name) as names, " +
            "   collect(distinct c2.name) as class_names, " +
            "   count(distinct c2) as class_count  " +
            "WHERE " +
            "   ((method_count >= $maxNumberOfShortMemoryCap and " +
            "       class_count/method_count <= $halfCouplingDispersion) or " +
            "   (method_count >= $fewCouplingIntensity " +
            "       and class_count/method_count <= $quarterCouplingDispersion)) and " +
            "       m1.max_nesting_depth >= $shallowMaximumNestingDepth " +
            "RETURN " +
            "   count(m1) as nr_of_smells")
    Double countIntensiveCoupling(
            @Param("appId") Long appId,
            @Param("maxNumberOfShortMemoryCap") double maxNumberOfShortMemoryCap,
            @Param("halfCouplingDispersion") double halfCouplingDispersion,
            @Param("fewCouplingIntensity") double fewCouplingIntensity,
            @Param("quarterCouplingDispersion") double quarterCouplingDispersion,
            @Param("shallowMaximumNestingDepth") double shallowMaximumNestingDepth
    );

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(cl:Class) " +
            "WHERE " +
            "   id(a) = $appId AND " +
            "   cl.depth_of_inheritance > $shortTermMemoryCap " +
            "RETURN " +
            "   count(distinct cl) as nr_of_smells")
    Double countDistortedHierarchy(@Param("appId") Long appId,
                                   @Param("shortTermMemoryCap") double shortTermMemoryCap);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(cl:Class)-[r:EXTENDS]->(parent:Class) " +
            "WHERE " +
            "   id(a) = $appId AND " +
            "   NOT ()-[:EXTENDS]->(cl) AND " +
            "   cl.number_of_methods + cl.number_of_attributes < $lowNumberOfMethodsAndAttributes AND " +
            "   parent.number_of_methods + parent.number_of_attributes >= $highNumberOfMethodsAndAttributes " +
            "RETURN " +
            "   count(distinct cl) as nr_of_smells")
    Double countTraditionBreaker(
            @Param("appId") Long appId,
            @Param("lowNumberOfMethodsAndAttributes") double lowNumberOfMethodsAndAttributes,
            @Param("highNumberOfMethodsAndAttributes") double highNumberOfMethodsAndAttributes);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(c:Class)-[:CLASS_OWNS_METHOD]->(m:Method)-[r:CALLS]->(other_method:Method) " +
            "WHERE " +
            "   id(a) = $appId " +
            "WITH " +
            "   c, " +
            "   m, " +
            "   COUNT(r) as number_of_called_methods " +
            "WHERE number_of_called_methods > $numberOfCalledMethods " +
            "RETURN " +
            "   count(m) as nr_of_smells")
    Double countSchizophrenicMethods(
            @Param("appId") Long appId,
            @Param("numberOfCalledMethods") double numberOfCalledMethods);

    @Query("MATCH (app:App)-[:APP_OWNS_CLASS]->(c:Class)-[:CLASS_OWNS_METHOD]->(m:Method)-[r:METHOD_OWNS_ARGUMENT]->(a:Argument) " +
            "WITH " +
            "   c, " +
            "   m, " +
            "   app, " +
            "   count(a) as argument_count " +
            "WHERE " +
            "   id(app) = $appId AND " +
            "   argument_count > $numberOfParameters  " +
            "RETURN " +
            "   count(m) as nr_of_smells")
    Double countLongParameterList(@Param("appId") Long appId,
                                  @Param("numberOfParameters") double numberOfParameters);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(class:Class)-[:CLASS_OWNS_METHOD]->(m:Method)-[:USES]->(v:Variable) <-[:CLASS_OWNS_VARIABLE]-(other_class:Class) " +
            "WHERE " +
            "   id(a) = $appId AND " +
            "   class <> other_class " +
            "WITH " +
            "   class, " +
            "   m, " +
            "   count(distinct v) as variable_count, " +
            "   collect(distinct v.name) as names, " +
            "   collect(distinct other_class.name) as class_names, " +
            "   count(distinct other_class) as class_count " +
            "MATCH (class)-[:CLASS_OWNS_METHOD]->(m)-[:USES]->(v:Variable)<-[:CLASS_OWNS_VARIABLE]-(class) " +
            "WITH " +
            "   class, " +
            "   m, " +
            "   variable_count, " +
            "   class_names, " +
            "   names, " +
            "   count(distinct v) as local_variable_count, " +
            "   collect(distinct v.name) as local_names, " +
            "   class_count " +
            "WHERE " +
            "   local_variable_count + variable_count > 0 " +
            "WITH " +
            "   class, " +
            "   m, " +
            "   variable_count, " +
            "   class_names, " +
            "   names, " +
            "   local_variable_count, " +
            "   local_names, " +
            "   class_count, " +
            "   local_variable_count*1.0/(local_variable_count+variable_count) as locality " +
            "WHERE " +
            "   variable_count > $fewAccessToForeignVariables and " +
            "   locality < $localityFraction and " +
            "   class_count <= $fewAccessToForeignClasses " +
            "RETURN " +
            "   count(m) as nr_of_smells")
    Double countFeatureEnvyByClass(
            @Param("appId") Long appId,
            @Param("fewAccessToForeignVariables") double fewAccessToForeignVariables,
            @Param("localityFraction") double localityFraction,
            @Param("fewAccessToForeignClasses") double fewAccessToForeignClasses);

    @Query("MATCH (app:App)-[:APP_OWNS_MODULE]->(module:Module)-[:MODULE_OWNS_CLASS] " +
            "->(class:Class)-[:CLASS_OWNS_VARIABLE]->(variable:Variable) " +
            "MATCH (app)-[:APP_OWNS_MODULE]->(module:Module)-[:MODULE_OWNS_CLASS] " +
            "->(other_class:Class)-[:CLASS_OWNS_VARIABLE]->(other_variable:Variable) " +
            "WHERE " +
            "   id(app) = $appId AND " +
            "   class <> other_class and variable.type = other_variable.type and variable.name = other_variable.name " +
            "WITH app, " +
            "   class, " +
            "   other_class, " +
            "   variable order by variable.name " +
            "WITH app, " +
            "   class, " +
            "   other_class, " +
            "   collect(distinct variable.name) as variable_names, " +
            "   count(DISTINCT variable) as variable_count WITH app, " +
            "   class, " +
            "   other_class, " +
            "   variable_names, " +
            "   variable_count order by id(class) WITH app, " +
            "   collect(distinct class.name) as class_names, " +
            "   variable_names, " +
            "   variable_count " +
            "WHERE variable_count >= $numberOfRepeatingVariables " +
            "RETURN " +
            "   count(distinct class) as nr_of_smells")
    Double countDataClumpsClasses(@Param("appId") Long appId,
                                  @Param("numberOfRepeatingVariables") double numberOfRepeatingVariables);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(cl:Class) " +
            "WHERE " +
            "   id(a) = $appId AND " +
            "   ()-[:IMPLEMENTS|EXTENDS]->(cl) and " +
            "   cl.is_interface = true " +
            "RETURN " +
            "   count(distinct cl) as nr_of_smells")
    Double countSpeculativeGeneralityInterfaces(@Param("appId") Long appId);

    @Query("MATCH  " +
            "(a:App)-[:APP_OWNS_CLASS]->(class)-[:CLASS_OWNS_METHOD]->(m:Method) " +
            "-[:METHOD_OWNS_ARGUMENT]->(p:Argument) " +
            "-[:IS_OF_TYPE]->(other_class:Class) " +
            "WHERE " +
            "   id(a) = $appId AND " +
            "   NOT (m)-[:CALLS|USES]->() " +
            "   <-[:CLASS_OWNS_VARIABLE|CLASS_OWNS_METHOD]-(other_class) AND " +
            "   NOT m.data_string contains (\"=\" + p.name) AND " +
            "   NOT m.data_string contains (\"= \" + p.name) AND " +
            "   NOT m.data_string contains (\":\" + p.name) AND " +
            "   NOT m.data_string contains (\": \" + p.name) AND " +
            "   NOT m.data_string contains (\"(\" + p.name + \")\") and " +
            "   NOT m.data_string contains (\"(\" + p.name + \",\") and " +
            "   NOT m.data_string contains (\"(\" + p.name + \" ,\") and " +
            "   NOT m.data_string contains (\", \" + p.name + \")\") and " +
            "   NOT m.data_string contains (\", \" + p.name + \",\") and " +
            "   class.is_interface = false " +
            "RETURN " +
            "   count(m) as nr_of_smells")
    Double countSpeculativeGenerality(@Param("appId") Long appId);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(class:Class)-[:CLASS_OWNS_METHOD]->(method:Method)-[:USES|CALLS]->(ref)<-[:CLASS_OWNS_VARIABLE|CLASS_OWNS_METHOD]-(other_class:Class) " +
            "WHERE " +
            "   id(a) = $appId AND " +
            "   class <> other_class and " +
            "   method.number_of_instructions < $numberOfInstructionsMethod " +
            "WITH " +
            "   class, " +
            "   method, " +
            "   collect(ref.name) as referenced_names, " +
            "   collect(other_class.name) as class_names " +
            "WITH " +
            "   collect(method.name) as method_names, " +
            "   collect(referenced_names) as references, " +
            "   collect(class_names) as classes, " +
            "   collect(method.number_of_instructions) as " +
            "   numbers_of_instructions, " +
            "   class, " +
            "   count(method) as method_count, " +
            "   count(method)*1.0/class.number_of_methods as method_ratio " +
            "WHERE " +
            "   method_ratio > $delegationToAllMethodsRatioHalf " +
            "RETURN " +
            "   count(distinct class) as nr_of_smells")
    Double countMiddleMan(@Param("appId") Long appId,
                          @Param("numberOfInstructionsMethod") double numberOfInstructionsMethod,
                          @Param("delegationToAllMethodsRatioHalf") double delegationToAllMethodsRatioHalf);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(class:Class)-[:CLASS_OWNS_METHOD]->(method:Method) " +
            "WHERE " +
            "   id(a) = $appId AND " +
            "   class.number_of_instructions > $highNumberOfInstructionsForClass and " +
            "   method.cyclomatic_complexity >= $highCyclomaticComplexity and " +
            "   method.max_nesting_depth >= $severalMaximalNestingDepth " +
            "MATCH (method)-[:USES]->(variable:Variable) " +
            "WITH " +
            "   class, " +
            "   method, " +
            "   count(distinct variable) as number_of_variables, " +
            "   collect(distinct variable.name) as variable_names " +
            "WHERE " +
            "   number_of_variables > $manyAccessedVariables " +
            "RETURN " +
            "   count(method) as nr_of_smells")
    Double countBrainMethodsByClass(
            @Param("appId") Long appId,
            @Param("highNumberOfInstructionsForClass") double highNumberOfInstructionsForClass,
            @Param("highCyclomaticComplexity") double highCyclomaticComplexity,
            @Param("severalMaximalNestingDepth") double severalMaximalNestingDepth,
            @Param("manyAccessedVariables") double manyAccessedVariables);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(class:Class)-[:CLASS_OWNS_METHOD]->(method:Method) " +
            "MATCH (class)-[:CLASS_OWNS_METHOD]->(other_method:Method) " +
            "   WHERE " +
            "   id(a) = $appId AND " +
            "   method <> other_method " +
            "WITH " +
            "   count(DISTINCT [method, other_method]) as pair_count, " +
            "   class " +
            "MATCH " +
            "   (class)-[:CLASS_OWNS_METHOD]->(method:Method) " +
            "MATCH " +
            "   (class)-[:CLASS_OWNS_METHOD]->(other_method:Method) " +
            "MATCH " +
            "   (class)-[:CLASS_OWNS_VARIABLE]->(variable:Variable) " +
            "WHERE " +
            "   method <> other_method and " +
            "   (method)-[:USES]->(variable)<-[:USES]-(other_method) " +
            "WITH " +
            "   class, " +
            "   pair_count, " +
            "   method, " +
            "   other_method, " +
            "   collect(distinct variable.name) as variable_names, " +
            "   count(distinct variable) as variable_count " +
            "WHERE " +
            "   variable_count >= 1 " +
            "WITH " +
            "   class, " +
            "   pair_count, " +
            "   count(distinct [method, other_method]) as connected_method_count " +
            "WITH " +
            "   class, " +
            "   connected_method_count*0.1/pair_count as class_cohesion, " +
            "   connected_method_count, " +
            "   pair_count " +
            "WHERE " +
            "   class_cohesion < $tightClassCohesionFraction and " +
            "   class.class_complexity >= $veryHighWeightedMethodCount " +
            "OPTIONAL MATCH " +
            "   (class)-[:CLASS_OWNS_METHOD]->(m:Method)-[:USES] " +
            "   ->(variable:Variable)<-[:CLASS_OWNS_VARIABLE]-(other_class:Class) " +
            "WHERE " +
            "   class <> other_class " +
            "WITH " +
            "   class, " +
            "   class_cohesion, " +
            "   connected_method_count, " +
            "   pair_count, " +
            "   count(distinct variable) as foreign_variable_count " +
            "WHERE " +
            "   foreign_variable_count >= $fewAccessToForeignData " +
            "RETURN " +
            "   count(distinct class) as nr_of_smells")
    Double countGodClasses(@Param("appId") Long appId,
                           @Param("tightClassCohesionFraction") double tightClassCohesionFraction,
                           @Param("veryHighWeightedMethodCount") double veryHighWeightedMethodCount,
                           @Param("fewAccessToForeignData") double fewAccessToForeignData);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(cl:Class) " +
            "WHERE " +
            "   id(a) = $appId AND " +
            "   cl.is_interface AND " +
            "   cl.number_of_methods > $veryHighNumberOfMethods " +
            "RETURN " +
            "   count(distinct cl) as nr_of_smells")
    Double countSwissArmyKnifeClasses(@Param("appId") Long appId,
                                      @Param("veryHighNumberOfMethods") double veryHighNumberOfMethods);

    @Query("MATCH (a:App)-[:APP_OWNS_CLASS]->(cl:Class) " +
            "WHERE " +
            "   id(a) = $appId AND " +
            "   cl.class_complexity > $veryHighClassComplexity " +
            "RETURN " +
            "   count(distinct cl) as nr_of_smells")
    Double countComplexClasses(@Param("appId") Long appId,
                               @Param("veryHighClassComplexity") double veryHighClassComplexity);
}
