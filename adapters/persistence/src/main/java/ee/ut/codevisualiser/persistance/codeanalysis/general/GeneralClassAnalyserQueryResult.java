package ee.ut.codevisualiser.persistance.codeanalysis.general;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.neo4j.annotation.QueryResult;

@Getter
@Builder
@Accessors(fluent = true)
@NoArgsConstructor
@AllArgsConstructor
@QueryResult
public class GeneralClassAnalyserQueryResult {
    long id;
    String name;
    String path;
    int methods;
}
