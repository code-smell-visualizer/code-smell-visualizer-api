package ee.ut.codevisualiser.persistance.codeevolution;


import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CodeEvolution extends Neo4jRepository<AppNode, Long> {

    List<AppNode> findAllByBranchOrderByTimeDesc(@Param("branch") String branch);

    @Query("match (a:App) return distinct a.branch order by a.branch")
    List<String> getAllBranches();

}
