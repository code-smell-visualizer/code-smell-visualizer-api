package ee.ut.codevisualiser.persistance.codesmell;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.neo4j.ogm.annotation.*;

import java.util.List;

@NodeEntity(label = "CodeSmell")
@Getter
@Builder
@Accessors(fluent = true)
@NoArgsConstructor
@AllArgsConstructor
public class CodeSmellNode {

    @Id
    @GeneratedValue
    private Long id;

    @Property("app_id")
    private Long appId;

    @Property("name")
    private String name;

    @Property("type")
    private String type;

    @Property("description")
    private String description;

    @Relationship(type = "HAS_PARAM")
    private List<ParameterNode> parameters;
}
