package ee.ut.codevisualiser.persistance.codesmell;
import static java.util.stream.Collectors.toList;

import ee.ut.codevisualiser.application.domain.codesmell.CodeSmell;
import ee.ut.codevisualiser.application.domain.codesmell.CodeSmellParameter;
import ee.ut.codevisualiser.application.domain.codesmell.enums.CodeSmellType;
import ee.ut.codevisualiser.application.usecase.codesmell.CodeSmellGateway;
import ee.ut.codevisualiser.common.PersistenceAdapter;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@PersistenceAdapter
@Transactional
public class CodeSmellAdapter implements CodeSmellGateway {

    private final CodeSmells codeSmells;
    private final CodeSmellParameters codeSmellParameters;

    public CodeSmellAdapter(CodeSmells codeSmells, CodeSmellParameters codeSmellParameters) {
        this.codeSmells = codeSmells;
        this.codeSmellParameters = codeSmellParameters;
    }


    @Override
    public List<CodeSmell> getCodeSmells() {
        return getCodeSmellNodes().stream()
                .map(this::toCodeSmell)
                .collect(toList());
    }

    private List<CodeSmellNode> getCodeSmellNodes() {
        return codeSmells.findAllByAppId(1L);
    }

    @Override
    public void save(List<CodeSmell> smells) {
        /*
        List<CodeSmellNode> existingCodeSmells = getCodeSmellNodes();
        if (existingCodeSmells == null || existingCodeSmells.isEmpty()) {
            List<CodeSmellNode> codeSmellNodes = smells.stream().map(this::toCodeSmellNode).collect(toList());
            codeSmells.saveAll(codeSmellNodes);
        } else {
            List<CodeSmellParameter> codeSmellParameters = getParameters(smells);
        }

         */
        List<CodeSmellNode> codeSmellNodes = smells.stream().map(this::toCodeSmellNode).collect(toList());
        codeSmells.saveAll(codeSmellNodes);
    }

    @Override
    public void updateParameters(List<CodeSmellParameter> parameters) {
        Map<Long, Double> parameterIdValueMap = parameters.stream()
                .collect(Collectors.toMap(CodeSmellParameter::id, CodeSmellParameter::value));

        List<ParameterNode> existingNodes = (List<ParameterNode>) codeSmellParameters.findAll();
        existingNodes.forEach(node -> node.value(parameterIdValueMap.get(node.id())));
        codeSmellParameters.saveAll(existingNodes);
    }

    private CodeSmellNode toCodeSmellNode(CodeSmell smell) {
        return CodeSmellNode.builder()
                .appId(1L)
                .id(smell.id())
                .name(smell.name())
                .type(smell.type().name())
                .description(smell.description())
                .parameters(toParametersNodeList(smell.parameters()))
                .build();
    }

    private List<ParameterNode> toParametersNodeList(List<CodeSmellParameter> parameters) {
        if (parameters == null) {
            return null;
        }
        return parameters.stream().map(this::toParameterNode).collect(toList());
    }

    private ParameterNode toParameterNode(CodeSmellParameter parameter) {
        return ParameterNode.builder()
                .id(parameter.id())
                .name(parameter.name())
                .value(parameter.value())
                .build();
    }

    private CodeSmell toCodeSmell(CodeSmellNode node) {
        return CodeSmell.builder()
                .id(node.id())
                .name(node.name())
                .description(node.description())
                .type(CodeSmellType.valueOf(node.type()))
                .selected(true)
                .parameters(toParametersList(node.parameters()))
                .build();
    }

    private List<CodeSmellParameter> toParametersList(List<ParameterNode> parameters) {
        if (parameters == null) {
            return null;
        }
        return parameters.stream().map(this::toParameter).collect(toList());
    }

    private CodeSmellParameter toParameter(ParameterNode node) {
        return CodeSmellParameter.builder()
                .id(node.id())
                .name(node.name())
                .value(node.value())
                .build();
    }


}
