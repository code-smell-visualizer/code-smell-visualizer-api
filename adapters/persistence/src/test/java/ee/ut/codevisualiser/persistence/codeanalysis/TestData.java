package ee.ut.codevisualiser.persistence.codeanalysis;

import ee.ut.codevisualiser.application.domain.codesmell.CodeSmell;
import ee.ut.codevisualiser.application.domain.codesmell.CodeSmellParameter;
import ee.ut.codevisualiser.application.domain.codesmell.enums.CodeSmellType;
import ee.ut.codevisualiser.persistance.codeanalysis.classcodesmell.ClassCodeSmellQueryResult;
import ee.ut.codevisualiser.persistance.codeanalysis.classusage.ClassUsageQueryResult;
import ee.ut.codevisualiser.persistance.codeanalysis.general.GeneralClassAnalyserQueryResult;
import ee.ut.codevisualiser.persistance.codeanalysis.methodcodesmell.MethodCodeSmellQueryResult;

import java.util.List;

public class TestData {

    public static final Long APP_ID = 1L;
    public static final Long CLASS_ID = 2L;
    public static final String CLASS_NAME = "TEST";
    public static final String CLASS_PATH = "TEST/TEST";
    public static final Integer NR_OF_USAGES = 5;

    public static List<GeneralClassAnalyserQueryResult> getGeneralClassAnalyserQueryResult() {
        return List.of(GeneralClassAnalyserQueryResult.builder()
                .id(CLASS_ID)
                .name(CLASS_NAME)
                .path(CLASS_PATH)
                .methods(5)
                .build());
    }

    public static List<ClassUsageQueryResult> getClassUsageQueryResult() {
        return List.of(ClassUsageQueryResult.builder()
                .classId(CLASS_ID)
                .name(CLASS_NAME)
                .count(NR_OF_USAGES)
                .build());
    }

    public static List<MethodCodeSmellQueryResult> getMethodCodeSmellQueryResult() {
        return List.of(MethodCodeSmellQueryResult.builder()
                .classId(CLASS_ID)
                .name(CLASS_NAME)
                .count(5)
                .build());
    }

    public static List<ClassCodeSmellQueryResult> getClassCodeSmellQueryResult() {
        return List.of(ClassCodeSmellQueryResult.builder()
                .classId(CLASS_ID)
                .name(CLASS_NAME)
                .build());
    }

    public static List<CodeSmell> getCodeSmells() {
        return List.of(
                CodeSmell.builder()
                    .name("Long method")
                    .selected(true)
                    .type(CodeSmellType.METHOD_BASED)
                    .description("")
                    .parameters(List.of(
                            CodeSmellParameter.builder().name("nr of instructions").value(30.5).build()))
                    .build(),
                CodeSmell.builder()
                        .name("Data class")
                        .selected(true)
                        .type(CodeSmellType.CLASS_BASED)
                        .description("")
                        .build()
                );
    }
}
