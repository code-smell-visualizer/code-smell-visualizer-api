package ee.ut.codevisualiser.persistence.codeanalysis;

import ee.ut.codevisualiser.application.domain.codeanalysis.CodeAnalysis;
import ee.ut.codevisualiser.persistance.codeanalysis.CodeAnalysisAdapter;
import ee.ut.codevisualiser.persistance.codeanalysis.classcodesmell.ClassCodeSmellAnalyser;
import ee.ut.codevisualiser.persistance.codeanalysis.classusage.ClassUsageAnalyser;
import ee.ut.codevisualiser.persistance.codeanalysis.general.GeneralClassAnalyser;
import ee.ut.codevisualiser.persistance.codeanalysis.methodcodesmell.MethodCodeSmellAnalyser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CodeAnalysisAdapterTest {

    private ClassUsageAnalyser classUsageAnalyser;
    private GeneralClassAnalyser generalClassAnalyser;
    private ClassCodeSmellAnalyser classCodeSmellAnalyser;
    private MethodCodeSmellAnalyser methodCodeSmellAnalyser;
    private CodeAnalysisAdapter codeAnalysisAdapter;
    private Verify verify;
    private Mock mock;


    @BeforeEach
    void setup() {
        classUsageAnalyser = mock(ClassUsageAnalyser.class);
        generalClassAnalyser = mock(GeneralClassAnalyser.class);
        classCodeSmellAnalyser = mock(ClassCodeSmellAnalyser.class);
        methodCodeSmellAnalyser = mock(MethodCodeSmellAnalyser.class);
        codeAnalysisAdapter = new CodeAnalysisAdapter(
                generalClassAnalyser,
                methodCodeSmellAnalyser,
                classUsageAnalyser,
                classCodeSmellAnalyser
        );
        mock = new Mock();
        verify = new Verify();
    }

    @Test
    void WHEN_code_analysis_required_THEN_correct_analysis_returned() {
        mock.initMocks();

        var result = codeAnalysisAdapter.getCodeAnalysis("BRANCH", null, "APP_NAME", TestData.getCodeSmells());

        verify.correctResultPassed(result.classCodeSmellAnalysis().get(TestData.CLASS_ID));
    }


    private class Mock {

        public void initMocks() {
            when(generalClassAnalyser.getLatestAppVersionId(anyString(), anyString())).thenReturn(TestData.APP_ID);
            when(generalClassAnalyser.getGeneralAnalysis(anyLong())).thenReturn(TestData.getGeneralClassAnalyserQueryResult());
            when(classUsageAnalyser.getClassUsageCount(anyLong())).thenReturn(TestData.getClassUsageQueryResult());
            when(methodCodeSmellAnalyser.countLongMethodsByClass(anyLong(), anyInt())).thenReturn(TestData.getMethodCodeSmellQueryResult());
            when(classCodeSmellAnalyser.getDataClasses(anyLong())).thenReturn(TestData.getClassCodeSmellQueryResult());
        }
    }

    private static class Verify {


        public void correctResultPassed(CodeAnalysis analysis) {
            assertThat(analysis.name()).isEqualTo(TestData.CLASS_NAME);
            assertThat(analysis.path()).isEqualTo(TestData.CLASS_PATH);
            assertThat(analysis.nrOfUsages()).isEqualTo(TestData.NR_OF_USAGES);
            assertThat(analysis.codeSmellScore()).isEqualTo(1.0);
        }
    }
}
