package ee.ut.codevisualiser.persistence.codesmellimpact;

import ee.ut.codevisualiser.application.domain.codesmellimpact.CodeSmellImpact;
import ee.ut.codevisualiser.persistance.codeanalysis.general.GeneralClassAnalyser;
import ee.ut.codevisualiser.persistance.codesmellimpact.CodeSmellImpactAdapter;
import ee.ut.codevisualiser.persistance.codesmellimpact.CodeSmellImpacts;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CodeSmellImpactAdapterTest {

    private CodeSmellImpacts codeSmellImpacts;
    private GeneralClassAnalyser generalClassAnalyser;
    private CodeSmellImpactAdapter adapter;
    private Mock mock;
    private Verify verify;

    @BeforeEach
    void setup() {
        codeSmellImpacts = mock(CodeSmellImpacts.class);
        generalClassAnalyser = mock(GeneralClassAnalyser.class);
        adapter = new CodeSmellImpactAdapter(codeSmellImpacts, generalClassAnalyser);
        mock = new Mock();
        verify = new Verify();
    }

    @Test
    void WHEN_code_smell_impact_required_THEN_correct_code_smells_returned() {
        mock.initMocks();

        var response = adapter.getCodeSmellImpacts(TestData.BRANCH, TestData.APP_NAME, TestData.getCodeSmells());

        verify.correctDataPassed(response);
    }


    private class Mock {

        public void initMocks() {
            when(codeSmellImpacts.countLongMethod(anyLong(), anyDouble())).thenReturn(10.3);
            when(codeSmellImpacts.countDataClasses(anyLong())).thenReturn(5.3);
            when(generalClassAnalyser.getLatestAppVersionId(anyString(), anyString())).thenReturn(TestData.APP_ID);
            when(generalClassAnalyser.getGeneralAnalysis(anyLong())).thenReturn(TestData.getGeneralClassAnalyserQueryResult());
        }
    }

    private static class Verify {

        public void correctDataPassed(List<CodeSmellImpact> response) {
            assertThat(response).hasSize(2);
            assertThat(response.get(0).codeSmellName()).isEqualTo("Long method");
            assertThat(response.get(0).impact()).isEqualTo(206.0);
            assertThat(response.get(1).codeSmellName()).isEqualTo("Data class");
            assertThat(response.get(1).impact()).isEqualTo(530.0);
        }
    }
}
