package ee.ut.codevisualiser.persistence.codesmell;

import ee.ut.codevisualiser.application.domain.codesmell.CodeSmell;
import ee.ut.codevisualiser.application.domain.codesmell.CodeSmellParameter;
import ee.ut.codevisualiser.application.usecase.codesmell.DefaultCodeSmells;
import ee.ut.codevisualiser.persistance.codesmell.CodeSmellNode;
import ee.ut.codevisualiser.persistance.codesmell.ParameterNode;

import java.util.List;

public class TestData {

    public static final Long ID = 1L;
    public static final String NAME = "TEST";
    public static final String TYPE = "METHOD_BASED";

    public static final String PARAM_NAME = "test";
    public static final Double PARAM_VALUE = 10.0;
    public static final Double NEW_PARAM_VALUE = 15.0;

    public static List<CodeSmellNode> getCodeSmellNodes() {
        return List.of(CodeSmellNode.builder()
                .id(ID)
                .name(NAME)
                .appId(ID)
                .type(TYPE)
                .parameters((List<ParameterNode>) getModifiedParameters())
                .build());
    }

    public static Iterable<ParameterNode> getModifiedParameters() {
        return List.of(ParameterNode.builder()
                .id(ID)
                .name(PARAM_NAME)
                .value(PARAM_VALUE)
                .build());
    }

    public static List<CodeSmellParameter> getCodeSmellParameters() {
        return List.of(CodeSmellParameter.builder()
                .id(ID)
                .name(PARAM_NAME)
                .value(NEW_PARAM_VALUE)
                .build());
    }

    public static List<CodeSmell> getCodeSmells() {
        return DefaultCodeSmells.getDefaultCodeSmells();
    }
}
