package ee.ut.codevisualiser.persistence.codesmell;

import ee.ut.codevisualiser.application.domain.codesmell.CodeSmell;
import ee.ut.codevisualiser.persistance.codesmell.CodeSmellAdapter;
import ee.ut.codevisualiser.persistance.codesmell.CodeSmellNode;
import ee.ut.codevisualiser.persistance.codesmell.CodeSmellParameters;
import ee.ut.codevisualiser.persistance.codesmell.CodeSmells;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class CodeSmellAdapterTest {

    private CodeSmells codeSmells;
    private CodeSmellParameters codeSmellParameters;
    private CodeSmellAdapter adapter;
    private Verify verify;

    @BeforeEach
    void setup() {
        codeSmells = mock(CodeSmells.class);
        codeSmellParameters = mock(CodeSmellParameters.class);
        adapter = new CodeSmellAdapter(codeSmells, codeSmellParameters);
        verify = new Verify();
    }

    @Test
    void WHEN_code_smells_required_THEN_correct_data_passed() {
        when(codeSmells.findAllByAppId(anyLong())).thenReturn(TestData.getCodeSmellNodes());

        var response = adapter.getCodeSmells();

        verify.correctCodeSmellsPassed(response);
    }

    @Test
    void WHEN_update_parameters_called_THEN_params_updated() {
        when(codeSmellParameters.findAll()).thenReturn(TestData.getModifiedParameters());
        when(codeSmellParameters.saveAll(anyList())).thenReturn(new ArrayList<>());

        adapter.updateParameters(TestData.getCodeSmellParameters());
    }

    @Test
    void WHEN_save_called_THEN_no_error_thrown() {
        when(codeSmells.save(any())).thenReturn(new CodeSmellNode());

        adapter.save(TestData.getCodeSmells());
    }

    private static class Verify {

        public void correctCodeSmellsPassed(List<CodeSmell> response) {
            assertThat(response).hasOnlyOneElementSatisfying(smell -> {
                assertThat(smell.name()).isEqualTo(TestData.NAME);
                assertThat(smell.id()).isEqualTo(TestData.ID);
            });
        }
    }
}
