package ee.ut.codevisualiser.persistence.codeevolution;

import ee.ut.codevisualiser.application.domain.codeevolution.App;
import ee.ut.codevisualiser.persistance.codeevolution.CodeEvolution;
import ee.ut.codevisualiser.persistance.codeevolution.CodeEvolutionAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CodeEvolutionAdapterTest {

    private CodeEvolution codeEvolution;
    private CodeEvolutionAdapter adapter;
    private Verify verify;

    @BeforeEach
    void setup() {
        codeEvolution = mock(CodeEvolution.class);
        adapter = new CodeEvolutionAdapter(codeEvolution);
        verify = new Verify();
    }

    @Test
    void WHEN_commits_required_THEN_correct_data_returned() {
        when(codeEvolution.findAllByBranchOrderByTimeDesc(anyString())).thenReturn(TestData.mockGetCommitsResponse());

        var response = adapter.getCommits(TestData.TEST_APP_BRANCH);

        verify.correctCommitResponsePassed(response);
    }

    @Test
    void WHEN_branches_required_THEN_correct_data_returned() {
        when(codeEvolution.getAllBranches()).thenReturn(TestData.mockgetBranchesResponse());

        var response = adapter.getAllBranches();

        verify.correctBranchResponsePassed(response);
    }

    private static class Verify {

        public void correctCommitResponsePassed(List<App> response) {
            assertThat(response).hasOnlyOneElementSatisfying(app -> {
                assertThat(app.commitMessage()).isEqualTo(TestData.TEST_COMMIT_MESSAGE);
                assertThat(app.name()).isEqualTo(TestData.TEST_APP_NAME);
                assertThat(app.author()).isEqualTo(TestData.TEST_APP_AUTHOR);
                assertThat(app.branch()).isEqualTo(TestData.TEST_APP_BRANCH);
                assertThat(app.versionNumber()).isEqualTo(TestData.VERSION_NUMBER);
            });
        }

        public void correctBranchResponsePassed(List<String> response) {
            assertThat(response).hasOnlyOneElementSatisfying(branch -> assertThat(branch).isEqualTo(TestData.TEST_APP_BRANCH));
        }
    }
}
