package ee.ut.codevisualiser.persistence.codeevolution;

import ee.ut.codevisualiser.persistance.codeanalysis.general.ClassNode;
import ee.ut.codevisualiser.persistance.codeanalysis.general.MethodNode;
import ee.ut.codevisualiser.persistance.codeevolution.AppNode;

import java.time.LocalDateTime;
import java.util.List;

public class TestData {
    public static final String TEST_APP_NAME = "TEST_APP_NAME";
    public static final String TEST_APP_AUTHOR = "TEST_APP_AUTHOR";
    public static final String TEST_APP_BRANCH = "TEST_APP_BRANCH";
    public static final String TEST_COMMIT_HASH = "TEST_COMMIT_HASH";
    public static final String TEST_COMMIT_MESSAGE = "TEST_COMMIT_MESSAGE";

    public static final String TEST_CLASS_CODE = "TEST_CLASS_CODE";
    public static final String TEST_CLASS_NAME = "TEST_CLASS_NAME";
    public static final String TEST_CLASS_PATH = "TEST_CLASS_PATH";
    public static final String TEST_CLASS_USR = "TEST_CLASS_USR";

    public static final String TEST_CLASS_CODE2 = "TEST_CLASS_CODE2";
    public static final String TEST_CLASS_NAME2 = "TEST_CLASS_NAME2";
    public static final String TEST_CLASS_PATH2 = "TEST_CLASS_PATH2";
    public static final String TEST_CLASS_USR2 = "TEST_CLASS_USR2";

    public static final String TEST_METHOD_CODE = "TEST_METHOD_CODE";
    public static final String TEST_METHOD_NAME = "TEST_METHOD_NAME";
    public static final String TEST_METHOD_USR = "TEST_METHOD_USR";
    public static final Integer NUMBER_OF_INSTRUCTIONS = 10;

    public static final Long VERSION_NUMBER = 1L;

    public static List<AppNode> mockGetCommitsResponse() {
        return List.of(AppNode.builder()
                .name(TEST_APP_NAME)
                .author(TEST_APP_AUTHOR)
                .branch(TEST_APP_BRANCH)
                .commitHash(TEST_COMMIT_HASH)
                .commitMessage(TEST_COMMIT_MESSAGE)
                .time(LocalDateTime.now().toString())
                .classNodes(List.of(createClassNode(), createClassNode2()))
                .versionNumber(VERSION_NUMBER)
                .build());
    }

    public static ClassNode createClassNode() {
        return ClassNode.builder()
                .code(TEST_CLASS_CODE)
                .name(TEST_CLASS_NAME)
                .path(TEST_CLASS_PATH)
                .usr(TEST_CLASS_USR)
                .methods(List.of(createMethodNode()))
                .versionNr(VERSION_NUMBER)
                .build();
    }

    public static ClassNode createClassNode2() {
        return ClassNode.builder()
                .code(TEST_CLASS_CODE2)
                .name(TEST_CLASS_NAME2)
                .path(TEST_CLASS_PATH2)
                .usr(TEST_CLASS_USR2)
                .methods(List.of(createMethodNode()))
                .versionNr(VERSION_NUMBER)
                .build();
    }

    public static MethodNode createMethodNode() {
        return MethodNode.builder()
                .code(TEST_METHOD_CODE)
                .name(TEST_METHOD_NAME)
                .numberOfInstructions(NUMBER_OF_INSTRUCTIONS)
                .usr(TEST_METHOD_USR)
                .versionNr(VERSION_NUMBER)
                .build();
    }

    public static List<String> mockgetBranchesResponse() {
        return List.of(TEST_APP_BRANCH);
    }
}
