dependencies {
    implementation(project(":common"))
    implementation(project(":application"))

    implementation("javax.transaction:javax.transaction-api")
    implementation("org.springframework.boot:spring-boot-starter-data-neo4j")
    implementation("org.apache.commons:commons-lang3")
    implementation("org.springframework.boot:spring-boot-starter-validation")
}
