package ee.ut.codevisualiser.graphify.upload;

import ee.ut.codevisualiser.application.usecase.upload.UploadGateway;
import ee.ut.codevisualiser.common.GraphifyAdapter;

import javax.transaction.Transactional;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@GraphifyAdapter
@Transactional
public class UploadAdapter implements UploadGateway {

    private static final String GRAPHIFY_PACKAGE_LOCATION = "GraphifySwiftEvolution";

    @Override
    public String upload(String applicationPath) {
        var runtime = Runtime.getRuntime();
        try {
            String command = "swift run --package-path " + GRAPHIFY_PACKAGE_LOCATION +
                    "/.build/debug GraphifyEvolution analyse " +
                    applicationPath +
                    " --language java";
            var process = runtime.exec(command);
            var input = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String line;

            while((line=input.readLine()) != null) {
                System.out.println(line);
            }

            int exitVal = process.waitFor();
            System.out.println("Exited with error code "+exitVal);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return "TEST";
    }
}
