package upload;

import ee.ut.codevisualiser.graphify.upload.UploadAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class UploadAdapterTest {

    private UploadAdapter adapter;
    private Verify verify;

    @BeforeEach
    void setup() {
        adapter = new UploadAdapter();
        verify = new Verify();
    }

    @Test
    void WHEN_app_upload_called_THEN_app_name_returned() {
        var response = adapter.upload(TestData.mockUploadAppInput());

        verify.uploadAppResponseCorrect(response);
    }

    private static class Verify {
        void uploadAppResponseCorrect(String response) {
            assertThat(response).isEqualTo(TestData.APP_NAME);
        }
    }
}
