FROM swift
RUN apt-get update && apt-get install -y openjdk-11-jdk
COPY configuration/build/libs/configuration.jar /opt/code-visualiser.jar
COPY GraphifySwift /opt/GraphifySwift
ENTRYPOINT ["java", "-jar", "/opt/code-visualiser.jar"]
