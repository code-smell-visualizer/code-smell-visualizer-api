dependencies {
    implementation(project(":common"))

    implementation("org.springframework.boot:spring-boot-starter-validation")
    implementation("javax.transaction:javax.transaction-api")
    implementation(group = "com.fasterxml.jackson.core", name = "jackson-databind", version = "2.11.0")
}
