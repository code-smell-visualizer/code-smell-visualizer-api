package ee.ut.codevisualiser.codesmell;

import ee.ut.codevisualiser.application.usecase.codesmell.CodeSmellGateway;
import ee.ut.codevisualiser.application.usecase.codesmell.UpdateCodeSmells;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

public class UpdateCodeSmellsTest {

    private UpdateCodeSmells updateCodeSmells;
    private CodeSmellGateway gateway;
    private Verify verify;

    @BeforeEach
    void setup() {
        gateway = mock(CodeSmellGateway.class);
        updateCodeSmells = new UpdateCodeSmells(gateway);
        verify = new Verify();
    }

    @Test
    void WHEN_smells_dont_exist_THEN_new_smells_saved() {
        when(gateway.getCodeSmells()).thenReturn(null);
        doNothing().when(gateway).save(anyList());

        updateCodeSmells.execute(TestData.mockUpdateCodeSmellsRequest());

        verify.saveCalled(gateway);
    }

    @Test
    void WHEN_smells_exist_THEN_smells_updated() {
        when(gateway.getCodeSmells()).thenReturn(TestData.getDefaultParams());
        doNothing().when(gateway).updateParameters(anyList());

        updateCodeSmells.execute(TestData.mockUpdateCodeSmellsRequest());

        verify.updateCalled(gateway);
    }


    private static class Verify {

        public void saveCalled(CodeSmellGateway gateway) {
            verify(gateway, times(1)).save(anyList());
            verify(gateway, times(0)).updateParameters(anyList());
        }

        public void updateCalled(CodeSmellGateway gateway) {
            verify(gateway, times(1)).updateParameters(anyList());
            verify(gateway, times(0)).save(anyList());
        }
    }
}
