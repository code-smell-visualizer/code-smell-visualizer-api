package ee.ut.codevisualiser.codesmell;

import ee.ut.codevisualiser.application.domain.codesmell.CodeSmell;
import ee.ut.codevisualiser.application.domain.codesmell.CodeSmellParameter;
import ee.ut.codevisualiser.application.domain.codesmell.enums.CodeSmellType;
import ee.ut.codevisualiser.application.usecase.codesmell.DefaultCodeSmells;
import ee.ut.codevisualiser.application.usecase.codesmell.UpdateCodeSmells;

import java.util.ArrayList;
import java.util.List;

public class TestData {

    public static final String NAME = "Long Method";
    public static final String DESCRIPTION = "TEST";
    public static final Boolean SELECTED = true;


    public static List<CodeSmell> mockResponse() {
        return List.of(CodeSmell.builder()
                .name(NAME)
                .selected(SELECTED)
                .type(CodeSmellType.METHOD_BASED)
                .description(DESCRIPTION)
                .parameters(List.of(
                        CodeSmellParameter.builder().name("nr of instructions").value(30.5).build()))
                .build());
    }

    public static List<CodeSmell> getDefaultParams() {
        return DefaultCodeSmells.getDefaultCodeSmells();
    }

    public static List<CodeSmell> mockModifiedDefaultParametersResponse() {
        List<CodeSmell> defaultCodeSmells = DefaultCodeSmells.getDefaultCodeSmells();
        List<CodeSmell> result = new ArrayList<>();
        defaultCodeSmells.forEach(codeSmell -> {
            if (codeSmell.name().equals("Long method")) {
                result.add(CodeSmell.builder()
                        .name(codeSmell.name())
                        .description(codeSmell.description())
                        .type(codeSmell.type())
                        .selected(codeSmell.selected())
                        .id(codeSmell.id())
                        .parameters(List.of(CodeSmellParameter.builder()
                                .id(codeSmell.parameters().get(0).id())
                                .name(codeSmell.parameters().get(0).name())
                                .value(10)
                                .build()))
                        .build());
            } else {
                result.add(codeSmell);
            }
        });
        return result;
    }

    public static List<CodeSmell> mockEmptyResponse() {
        return List.of();
    }


    public static UpdateCodeSmells.Request mockUpdateCodeSmellsRequest() {
        return UpdateCodeSmells.Request.of(DefaultCodeSmells.getDefaultCodeSmells());
    }
}
