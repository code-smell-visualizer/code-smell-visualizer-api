package ee.ut.codevisualiser.codesmell;

import ee.ut.codevisualiser.application.domain.codesmell.CodeSmell;
import ee.ut.codevisualiser.application.usecase.codesmell.CodeSmellGateway;
import ee.ut.codevisualiser.application.usecase.codesmell.GetCodeSmells;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GetCodeSmellsTest {

    private GetCodeSmells getCodeSmells;
    private CodeSmellGateway gateway;
    private Verify verify;

    @BeforeEach
    void setup() {
        gateway = mock(CodeSmellGateway.class);
        getCodeSmells = new GetCodeSmells(gateway);
        verify = new Verify();
    }

    @Test
    void WHEN_get_code_smells_executed_THEN_correct_code_smells_returned() {
        when(gateway.getCodeSmells()).thenReturn(TestData.mockResponse());

        var response = getCodeSmells.execute();

        verify.correctCodeSmellsPassed(response.getCodeSmells());
    }

    @Test
    void WHEN_get_code_smells_executed_THEN_default_code_smells_returned() {
        when(gateway.getCodeSmells()).thenReturn(TestData.mockEmptyResponse());

        var response = getCodeSmells.execute();

        verify.defaultCodeSmellsPassed(response.getCodeSmells());
    }

    private static class Verify {
        void correctCodeSmellsPassed(List<CodeSmell> response) {
            assertThat(response).hasOnlyOneElementSatisfying(codeSmell -> {
                assertThat(codeSmell.name()).isEqualTo(TestData.NAME);
                assertThat(codeSmell.description()).isEqualTo(TestData.DESCRIPTION);
                assertThat(codeSmell.selected()).isEqualTo(TestData.SELECTED);
                assertThat(codeSmell.parameters()).hasSize(1);
            });
        }

        void defaultCodeSmellsPassed(List<CodeSmell> response) {
            assertThat(response).hasSize(22);
        }

    }
}
