package ee.ut.codevisualiser.codesmell;

import ee.ut.codevisualiser.application.usecase.codesmell.CodeSmellGateway;
import ee.ut.codevisualiser.application.usecase.codesmell.GetNrOfModifiedParameters;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GetNrOfModifiedParametersTest {

    private GetNrOfModifiedParameters getNrOfModifiedParameters;
    private CodeSmellGateway gateway;
    private Verify verify;

    @BeforeEach
    void setup() {
        gateway = mock(CodeSmellGateway.class);
        getNrOfModifiedParameters = new GetNrOfModifiedParameters(gateway);
        verify = new Verify();
    }

    @Test
    void WHEN_no_params_modified_THEN_correct_number_returned() {
        when(gateway.getCodeSmells()).thenReturn(TestData.getDefaultParams());

        var response = getNrOfModifiedParameters.execute();

        verify.noModifiedParams(response.getNrOfParamsModified());
    }

    @Test
    void WHEN_get_nr_of_params_modified_executed_THEN_correct_number_returned() {
        when(gateway.getCodeSmells()).thenReturn(TestData.mockModifiedDefaultParametersResponse());

        var response = getNrOfModifiedParameters.execute();

        verify.oneModifiedParam(response.getNrOfParamsModified());
    }

    private static class Verify {
        void noModifiedParams(Integer response) {
            assertThat(response).isEqualTo(0);
        }

        void oneModifiedParam(Integer response) {
            assertThat(response).isEqualTo(1);
        }

    }
}
