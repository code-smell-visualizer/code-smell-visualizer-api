package ee.ut.codevisualiser.codeanalysis;

import ee.ut.codevisualiser.application.domain.codeanalysis.ClassAnalysis;
import ee.ut.codevisualiser.application.usecase.codeanalysis.CodeAnalysisGateway;
import ee.ut.codevisualiser.application.usecase.codeanalysis.GetCodeAnalysis;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GetCodeAnalysisTest {

    private CodeAnalysisGateway gateway;
    private Verify verify;
    private GetCodeAnalysis getCodeAnalysis;

    @BeforeEach
    void setup() {
        gateway = mock(CodeAnalysisGateway.class);
        getCodeAnalysis = new GetCodeAnalysis(gateway);
        verify = new Verify();
    }

    @Test
    void WHEN_code_analysis_result_received_THEN_result_properly_mapped() {
        when(gateway.getCodeAnalysis(anyString(), anyLong(), anyString(), anyList())).thenReturn(TestData.mockCodeAnalysisResponse());

        var response = getCodeAnalysis.execute(TestData.getRequest());

        verify.classAnalysisCorrect(response.getClassAnalyses());
    }

    private static class Verify {
        void classAnalysisCorrect(List<ClassAnalysis> analysis) {
            assertThat(analysis.size()).isEqualTo(2);
            assertThat(analysis.get(0).name()).isEqualTo(TestData.CLASS1_NAME);
            assertThat(analysis.get(0).score()).isEqualTo(10.4);
            assertThat(analysis.get(0).size()).isEqualTo(100);
            assertThat(analysis.get(1).name()).isEqualTo(TestData.CLASS2_NAME);
            assertThat(analysis.get(1).score()).isEqualTo(5.4);
            assertThat(analysis.get(1).size()).isEqualTo(10);
        }
    }
}
