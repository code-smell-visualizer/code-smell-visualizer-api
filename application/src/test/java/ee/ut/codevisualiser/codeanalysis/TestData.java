package ee.ut.codevisualiser.codeanalysis;

import ee.ut.codevisualiser.application.domain.codeanalysis.CodeAnalysis;
import ee.ut.codevisualiser.application.domain.codeanalysis.CodeAnalysisResult;
import ee.ut.codevisualiser.application.domain.codesmell.CodeSmell;
import ee.ut.codevisualiser.application.domain.codesmell.enums.CodeSmellType;
import ee.ut.codevisualiser.application.usecase.codeanalysis.GetCodeAnalysis;

import java.util.HashMap;
import java.util.List;

public class TestData {

    public static final String BRANCH = "TEST";
    public static final String APP_NAME = "TEST";
    public static final Long COMMIT_ID = 1L;

    public static final String CLASS1_NAME = "CLASS1";
    public static final String CLASS1_PATH = "PATH/CLASS1";
    public static final String CLASS2_NAME = "CLASS2";
    public static final String CLASS2_PATH = "PATH/CLASS2";


    public static GetCodeAnalysis.Request getRequest() {
        return GetCodeAnalysis.Request.of(List.of(getLongMethod()), BRANCH, COMMIT_ID, APP_NAME);
    }

    private static CodeSmell getLongMethod() {
        return CodeSmell.builder()
                .name("Long method")
                .selected(true)
                .type(CodeSmellType.METHOD_BASED)
                .description("")
                .build();
    }

    public static CodeAnalysisResult mockCodeAnalysisResponse() {
        HashMap<Long, CodeAnalysis> result = new HashMap<>();
        CodeAnalysis codeAnalysis = CodeAnalysis.builder()
                .name(CLASS1_NAME)
                .path(CLASS1_PATH)
                .nrOfMethods(1)
                .codeSmellScore(10.4)
                .nrOfUsages(1)
                .build();
        CodeAnalysis codeAnalysis2 = CodeAnalysis.builder()
                .name(CLASS2_NAME)
                .path(CLASS2_PATH)
                .nrOfMethods(2)
                .codeSmellScore(5.4)
                .nrOfUsages(0)
                .build();
        result.put(1L, codeAnalysis);
        result.put(2L, codeAnalysis2);
        return new CodeAnalysisResult(result);
    }
}
