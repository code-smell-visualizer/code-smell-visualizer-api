package ee.ut.codevisualiser.codeevolution;

import ee.ut.codevisualiser.application.usecase.codeevolution.CodeEvolutionGateway;
import ee.ut.codevisualiser.application.usecase.codeevolution.GetBranches;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GetBranchesTest {

    private CodeEvolutionGateway gateway;
    private GetBranches getBranches;
    private Verify verify;

    @BeforeEach
    void setup() {
        gateway = mock(CodeEvolutionGateway.class);
        getBranches = new GetBranches(gateway);
        verify = new Verify();
    }

    @Test
    void WHEN_get_branches_executed_THEN_correct_data_returned() {
        when(gateway.getAllBranches()).thenReturn(List.of("TEST"));

        var response = getBranches.execute();

        verify.branchCorrect(response.getBranches());
    }

    private static class Verify {

        public void branchCorrect(List<String> branches) {
            assertThat(branches).hasOnlyOneElementSatisfying(branch -> assertThat(branch).isEqualTo("TEST"));
        }
    }

}
