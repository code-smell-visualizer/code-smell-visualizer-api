package ee.ut.codevisualiser.codeevolution;

import ee.ut.codevisualiser.application.domain.codeevolution.App;
import ee.ut.codevisualiser.application.usecase.codeevolution.GetCommits;

import java.util.List;

public class TestData {

    public static final String APP_NAME = "TEST";
    public static final String AUTHOR = "TEST";
    public static final String MESSAGE = "TEST";
    public static final String BRANCH = "TEST";


    public static List<App> getCommitsResponse() {
        return List.of(App.builder()
                .name(APP_NAME)
                .author(AUTHOR)
                .commitMessage(MESSAGE)
                .branch(BRANCH)
                .build());
    }

    public static GetCommits.Request getBreanchesRequest() {
        return GetCommits.Request.of(BRANCH);
    }
}
