package ee.ut.codevisualiser.codeevolution;

import ee.ut.codevisualiser.application.domain.codeevolution.App;
import ee.ut.codevisualiser.application.usecase.codeevolution.CodeEvolutionGateway;
import ee.ut.codevisualiser.application.usecase.codeevolution.GetCommits;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GetCommitsTest {

    private CodeEvolutionGateway gateway;
    private GetCommits getCommits;
    private Verify verify;

    @BeforeEach
    void setup() {
        gateway = mock(CodeEvolutionGateway.class);
        getCommits = new GetCommits(gateway);
        verify = new Verify();
    }

    @Test
    void WHEN_get_commits_executed_THEN_correct_data_returned() {
        when(gateway.getCommits(anyString())).thenReturn(TestData.getCommitsResponse());

        var response = getCommits.execute(TestData.getBreanchesRequest());

        verify.branchCorrect(response.getCommits());
    }

    private static class Verify {

        public void branchCorrect(List<App> commits) {
            assertThat(commits).hasOnlyOneElementSatisfying(commit -> {
                assertThat(commit.branch()).isEqualTo(TestData.BRANCH);
                assertThat(commit.author()).isEqualTo(TestData.AUTHOR);
                assertThat(commit.commitMessage()).isEqualTo(TestData.MESSAGE);
            });
        }
    }

}
