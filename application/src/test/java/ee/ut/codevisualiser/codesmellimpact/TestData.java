package ee.ut.codevisualiser.codesmellimpact;

import ee.ut.codevisualiser.application.domain.codesmellimpact.CodeSmellImpact;
import ee.ut.codevisualiser.application.usecase.codesmellimpact.GetCodeSmellImpacts;

import java.util.List;

public class TestData {

    public static final String NAME = "TEST";
    public static final Double IMPACT = 10.0;

    public static List<CodeSmellImpact> mockResponse() {
        return List.of(CodeSmellImpact.builder()
                .codeSmellName(NAME)
                .impact(IMPACT)
                .build());
    }

    public static GetCodeSmellImpacts.Request createEmptyRequest() {
        return GetCodeSmellImpacts.Request.of(List.of(), null, NAME);
    }

    public static GetCodeSmellImpacts.Request createRequest() {
        return GetCodeSmellImpacts.Request.of(List.of(), "TEST", NAME);
    }
}
