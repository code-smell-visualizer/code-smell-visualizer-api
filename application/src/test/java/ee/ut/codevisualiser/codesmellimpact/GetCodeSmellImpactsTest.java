package ee.ut.codevisualiser.codesmellimpact;

import ee.ut.codevisualiser.application.domain.codesmellimpact.CodeSmellImpact;
import ee.ut.codevisualiser.application.usecase.codesmellimpact.CodeSmellImpactGateway;
import ee.ut.codevisualiser.application.usecase.codesmellimpact.GetCodeSmellImpacts;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class GetCodeSmellImpactsTest {

    private CodeSmellImpactGateway gateway;
    private GetCodeSmellImpacts getCodeSmellImpacts;
    private Verify verify;

    @BeforeEach
    void setup() {
        gateway = mock(CodeSmellImpactGateway.class);
        getCodeSmellImpacts = new GetCodeSmellImpacts(gateway);
        verify = new Verify();
    }

    @Test
    void WHEN_no_branch_specified_THEN_gateway_called_with_master_branch() {
        when(gateway.getCodeSmellImpacts(anyString(), anyString(), anyList())).thenReturn(TestData.mockResponse());

        var response = getCodeSmellImpacts.execute(TestData.createEmptyRequest());

        verify.impactCorrect(response.getCodeSmellsImpact());
    }

    @Test
    void WHEN_branch_specified_THEN_gateway_called_with_master_branch() {
        when(gateway.getCodeSmellImpacts(anyString(), anyString(), anyList())).thenReturn(TestData.mockResponse());

        var response = getCodeSmellImpacts.execute(TestData.createRequest());

        verify.impactCorrect(response.getCodeSmellsImpact());
    }

    private static class Verify {
        void impactCorrect(List<CodeSmellImpact> response) {
            assertThat(response).hasOnlyOneElementSatisfying(codeSmellImpact -> {
                assertThat(codeSmellImpact.codeSmellName()).isEqualTo(TestData.NAME);
                assertThat(codeSmellImpact.impact()).isEqualTo(TestData.IMPACT);
            });
        }
    }
}
