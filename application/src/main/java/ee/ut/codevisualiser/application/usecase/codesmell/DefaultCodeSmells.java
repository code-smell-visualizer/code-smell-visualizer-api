package ee.ut.codevisualiser.application.usecase.codesmell;

import ee.ut.codevisualiser.application.domain.codesmell.CodeSmell;
import ee.ut.codevisualiser.application.domain.codesmell.CodeSmellParameter;
import ee.ut.codevisualiser.application.domain.codesmell.enums.CodeSmellType;

import java.util.ArrayList;
import java.util.List;

public class DefaultCodeSmells {

    public static List<CodeSmell> getDefaultCodeSmells() {
        List<CodeSmell> codeSmells = new ArrayList<>();
        codeSmells.add(CodeSmell.builder()
                .name("Long method")
                .selected(true)
                .type(CodeSmellType.METHOD_BASED)
                .description("")
                .parameters(List.of(
                        CodeSmellParameter.builder().name("nr of instructions").value(30.5).build()))
                .build());

        codeSmells.add(CodeSmell.builder()
                .name("Large class / Blob class")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED)
                .description("")
                .parameters(List.of(
                        CodeSmellParameter.builder().name("cohesion in methods").value(17.5).build(),
                        CodeSmellParameter.builder().name("nr of attributes").value(13.5).build(),
                        CodeSmellParameter.builder().name("nr of methods").value(13.5).build()
                ))
                .build());

        codeSmells.add(CodeSmell.builder()
                .name("Shotgun surgery")
                .selected(true)
                .type(CodeSmellType.METHOD_BASED)
                .description("")
                .parameters(List.of(
                        CodeSmellParameter.builder().name("nr of callers").value(2.5).build()))
                .build());

        codeSmells.add(CodeSmell.builder()
                .name("Switch statements")
                .selected(true)
                .type(CodeSmellType.METHOD_BASED)
                .description("")
                .parameters(List.of(
                        CodeSmellParameter.builder().name("nr of statements").value(0).build()))
                .build());

        codeSmells.add(CodeSmell.builder()
                .name("Lazy class")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED)
                .description("")
                .parameters(List.of(
                        CodeSmellParameter.builder().name("complexity-method ratio").value(1).build(),
                        CodeSmellParameter.builder().name("coupling between classes").value(0.1).build(),
                        CodeSmellParameter.builder().name("median nr of instructions").value(20).build(),
                        CodeSmellParameter.builder().name("nr of depth of inheritance").value(1).build()
                ))
                .build());

        codeSmells.add(CodeSmell.builder()
                .name("Message chains")
                .selected(true)
                .type(CodeSmellType.METHOD_BASED)
                .description("")
                .parameters(List.of(
                        CodeSmellParameter.builder().name("nr of chained messages").value(2.5).build()))
                .build());

        codeSmells.add(CodeSmell.builder()
                .name("Data class")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED)
                .description("")
                .build());

        codeSmells.add(CodeSmell.builder()
                .name("Comments")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED)
                .description("")
                .parameters(List.of(
                        CodeSmellParameter.builder().name("nr of comments").value(29.5).build()))
                .build());

        codeSmells.add(CodeSmell.builder()
                .name("Cyclic dependencies (dependencies between classes)")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED)
                .description("")
                .build());

        codeSmells.add(CodeSmell.builder()
                .name("Intensive coupling")
                .selected(true)
                .type(CodeSmellType.METHOD_BASED)
                .description("")
                .parameters(List.of(
                        CodeSmellParameter.builder().name("few coupling intensity").value(2).build(),
                        CodeSmellParameter.builder().name("half coupling dispersion").value(0.5).build(),
                        CodeSmellParameter.builder().name("maximum nesting depth").value(1).build(),
                        CodeSmellParameter.builder().name("quarter coupling dispersion").value(0.25).build(),
                        CodeSmellParameter.builder().name("short-term memory cap").value(7).build()
                ))
                .build());

        codeSmells.add(CodeSmell.builder()
                .name("Distorted hierarchy")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED)
                .description("")
                .parameters(List.of(
                        CodeSmellParameter.builder().name("short term memory cap").value(7).build()))
                .build());

        codeSmells.add(CodeSmell.builder()
                .name("Tradition Breaker")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED)
                .description("")
                .parameters(List.of(
                        CodeSmellParameter.builder().name("low nr of methods/attributes").value(2).build(),
                        CodeSmellParameter.builder().name("high nr of methods/attributes").value(24.5).build()
                ))
                .build());

        codeSmells.add(CodeSmell.builder()
                .name("Divergent Change/Schizophrenic Class")
                .selected(true)
                .type(CodeSmellType.METHOD_BASED)
                .description("")
                .parameters(List.of(
                        CodeSmellParameter.builder().name("nr of called methods").value(2.5).build()))
                .build());

        codeSmells.add(CodeSmell.builder()
                .name("Long parameter list")
                .selected(true)
                .type(CodeSmellType.METHOD_BASED)
                .description("")
                .parameters(List.of(
                        CodeSmellParameter.builder().name("nr of parameters").value(2.5).build()))
                .build());

        codeSmells.add(CodeSmell.builder()
                .name("Feature envy")
                .selected(true)
                .type(CodeSmellType.METHOD_BASED)
                .description("")
                .parameters(List.of(
                        CodeSmellParameter.builder().name("access to foreign classes").value(2).build(),
                        CodeSmellParameter.builder().name("access to foreign variables").value(2).build(),
                        CodeSmellParameter.builder().name("locality fraction").value(0.33).build()
                ))
                .build());

        /*
        codeSmells.add(CodeSmell.builder()
                .name("Data clumps (class variables)")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED)
                .description("")
                .parameters(List.of(
                        CodeSmellParameter.builder().name("nr of repeating variables").value(3).build()))
                .build());
         */

        codeSmells.add(CodeSmell.builder()
                .name("Speculative generality (interfaces)")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED)
                .description("")
                .build());

        codeSmells.add(CodeSmell.builder()
                .name("Speculative generality (methods)")
                .selected(true)
                .type(CodeSmellType.METHOD_BASED)
                .description("")
                .build());

        codeSmells.add(CodeSmell.builder()
                .name("Middle man")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED)
                .description("")
                .parameters(List.of(
                        CodeSmellParameter.builder().name("methods delegation ratio").value(1).build(),
                        CodeSmellParameter.builder().name("nr of instructions method").value(3).build()
                        ))
                .build());

        codeSmells.add(CodeSmell.builder()
                .name("Brain method")
                .selected(true)
                .type(CodeSmellType.METHOD_BASED)
                .description("")
                .parameters(List.of(
                        CodeSmellParameter.builder().name("accessed variables").value(7).build(),
                        CodeSmellParameter.builder().name("cyclomatic complexity").value(3).build(),
                        CodeSmellParameter.builder().name("instructions for class").value(147.5).build(),
                        CodeSmellParameter.builder().name("maximal nesting depth").value(3).build()
                ))
                .build());

        codeSmells.add(CodeSmell.builder()
                .name("God class")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED)
                .description("")
                .parameters(List.of(
                        CodeSmellParameter.builder().name("access to foreign data").value(2).build(),
                        CodeSmellParameter.builder().name("class cohesion fraction").value(0.3).build(),
                        CodeSmellParameter.builder().name("weighted method count").value(33.5).build()
                ))
                .build());

        codeSmells.add(CodeSmell.builder()
                .name("Swiss army knife")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED)
                .description("")
                .parameters(List.of(
                        CodeSmellParameter.builder().name("nr of methods").value(13.5).build()))
                .build());

        codeSmells.add(CodeSmell.builder()
                .name("Complex class")
                .selected(true)
                .type(CodeSmellType.CLASS_BASED)
                .description("")
                .parameters(List.of(
                        CodeSmellParameter.builder().name("class complexity").value(33.5).build()))
                .build());

        return codeSmells;
    }
}
