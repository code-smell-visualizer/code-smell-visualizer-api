package ee.ut.codevisualiser.application.domain.codesmellimpact;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@Accessors(fluent = true)
@AllArgsConstructor
@NoArgsConstructor
public class CodeSmellImpact {
    String codeSmellName;
    Double impact;
}
