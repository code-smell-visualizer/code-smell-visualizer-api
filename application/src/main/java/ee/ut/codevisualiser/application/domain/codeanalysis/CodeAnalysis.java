package ee.ut.codevisualiser.application.domain.codeanalysis;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@Accessors(fluent = true)
public class CodeAnalysis {

    private String name;
    private String path;
    private int nrOfMethods;
    private double codeSmellScore;
    private int nrOfUsages;

    public CodeAnalysis() {
        this.codeSmellScore = 0;
        this.nrOfUsages = 0;
    }

    public CodeAnalysis addMethodCodeSmellScore(int scoreToAdd) {
        this.codeSmellScore += (double) scoreToAdd / nrOfMethods;
        return this;
    }

    public CodeAnalysis addClassCodeSmellScore() {
        this.codeSmellScore += 1.0;
        return this;
    }
}
