package ee.ut.codevisualiser.application.usecase.upload;

import ee.ut.codevisualiser.common.UseCase;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import javax.transaction.Transactional;
import java.util.List;

@UseCase
@RequiredArgsConstructor
@Transactional
public class GetExistingApplications {

    private final AppGateway appGateway;

    public Response execute() {
        return Response.of(appGateway.getExistingApplications());
    }

    @Value(staticConstructor = "of")
    public static class Response {
        public List<String> existingApplications;
    }
}
