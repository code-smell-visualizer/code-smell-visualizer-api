package ee.ut.codevisualiser.application.usecase.upload;

public interface UploadGateway {

    String upload(String applicationPath);

}
