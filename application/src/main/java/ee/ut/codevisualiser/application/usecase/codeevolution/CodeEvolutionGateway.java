package ee.ut.codevisualiser.application.usecase.codeevolution;

import ee.ut.codevisualiser.application.domain.codeevolution.App;

import java.util.List;

public interface CodeEvolutionGateway {

    List<App> getCommits(String branch);

    List<String> getAllBranches();
}
