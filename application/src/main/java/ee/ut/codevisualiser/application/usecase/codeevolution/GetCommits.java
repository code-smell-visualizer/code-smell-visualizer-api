package ee.ut.codevisualiser.application.usecase.codeevolution;

import ee.ut.codevisualiser.application.domain.codeevolution.App;
import ee.ut.codevisualiser.common.SelfValidating;
import ee.ut.codevisualiser.common.UseCase;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.List;

@UseCase
@RequiredArgsConstructor
@Transactional
public class GetCommits {

    private final CodeEvolutionGateway codeEvolutionGateway;

    public Response execute(Request request) {
        return Response.of(codeEvolutionGateway.getCommits(request.branch));
    }

    @Value(staticConstructor = "of")
    @EqualsAndHashCode(callSuper = false)
    public static class Request extends SelfValidating<Request> {
        @NotNull
        public String branch;
    }

    @Value(staticConstructor = "of")
    public static class Response {
        public List<App> commits;
    }
}
