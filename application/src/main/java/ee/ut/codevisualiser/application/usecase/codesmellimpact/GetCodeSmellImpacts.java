package ee.ut.codevisualiser.application.usecase.codesmellimpact;

import ee.ut.codevisualiser.application.domain.codesmell.CodeSmell;
import ee.ut.codevisualiser.application.domain.codesmellimpact.CodeSmellImpact;
import ee.ut.codevisualiser.application.usecase.codeanalysis.GetCodeAnalysis;
import ee.ut.codevisualiser.common.SelfValidating;
import ee.ut.codevisualiser.common.UseCase;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.List;

@UseCase
@RequiredArgsConstructor
@Transactional
public class GetCodeSmellImpacts {

    private final CodeSmellImpactGateway codeSmellImpactGateway;

    public Response execute(Request request) {
        String branch = request.branch == null ? "master" : request.branch;
        return Response.of(codeSmellImpactGateway.getCodeSmellImpacts(branch, request.appName, request.codeSmells));
    }


    @Value(staticConstructor = "of")
    @EqualsAndHashCode(callSuper = false)
    public static class Request extends SelfValidating<GetCodeAnalysis.Request> {

        @NotNull
        public List<CodeSmell> codeSmells;

        public String branch;

        @NotNull
        public String appName;
    }

    @Value(staticConstructor = "of")
    public static class Response {
        public List<CodeSmellImpact> codeSmellsImpact;
    }
}
