package ee.ut.codevisualiser.application.usecase.upload;

import java.util.List;

public interface AppGateway {
    List<String> getExistingApplications();

}
