package ee.ut.codevisualiser.application.usecase.codesmellimpact;

import ee.ut.codevisualiser.application.domain.codesmell.CodeSmell;
import ee.ut.codevisualiser.application.domain.codesmellimpact.CodeSmellImpact;

import java.util.List;

public interface CodeSmellImpactGateway {

    List<CodeSmellImpact> getCodeSmellImpacts(String branch, String appName, List<CodeSmell> codeSmells);
}
