package ee.ut.codevisualiser.application.usecase.codesmell;


import ee.ut.codevisualiser.application.domain.codesmell.CodeSmell;
import ee.ut.codevisualiser.common.UseCase;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import javax.transaction.Transactional;
import java.util.List;

@UseCase
@RequiredArgsConstructor
@Transactional
public class GetCodeSmells {

    private final CodeSmellGateway codeSmellGateway;

    public Response execute() {
        List<CodeSmell> codeSmells = codeSmellGateway.getCodeSmells();
        if (codeSmells == null || codeSmells.isEmpty()) {
            codeSmells = DefaultCodeSmells.getDefaultCodeSmells();
        }
        return Response.of(codeSmells);
    }


    @Value(staticConstructor = "of")
    public static class Response {
        public List<CodeSmell> codeSmells;
    }
}
