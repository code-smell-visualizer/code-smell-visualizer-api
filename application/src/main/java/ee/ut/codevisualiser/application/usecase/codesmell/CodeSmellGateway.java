package ee.ut.codevisualiser.application.usecase.codesmell;

import ee.ut.codevisualiser.application.domain.codesmell.CodeSmell;
import ee.ut.codevisualiser.application.domain.codesmell.CodeSmellParameter;

import java.util.List;

public interface CodeSmellGateway {

    List<CodeSmell> getCodeSmells();

    void save(List<CodeSmell> codeSmells);

    void updateParameters(List<CodeSmellParameter> parameters);
}
