package ee.ut.codevisualiser.application.domain.codesmell;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@Accessors(fluent = true)
@AllArgsConstructor
@NoArgsConstructor
public class CodeSmellParameter {
    private Long id;
    private String name;
    private double value;
}
