package ee.ut.codevisualiser.application.usecase.codeanalysis.calculator;

import ee.ut.codevisualiser.application.domain.codeanalysis.Force;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class ClassForceCalculator {

    private static final int width = 1600;
    private static final int height = 900;
    private static int nrOfHorizontalRectangles;
    private static int nrOfVerticalRectangles;

    public static HashMap<String, Force> getPackageCoordinatesMap(List<String> packageNames) {
        HashMap<String, Force> result = new HashMap<>();
        Collections.sort(packageNames);
        calculateNumberOfLines(packageNames.size());
        List<Force> coordinates = new ArrayList<>();
        for (int i = 1; i <= nrOfVerticalRectangles; i++) {
            for (int j = 1; j <= nrOfHorizontalRectangles; j++) {
                coordinates.add(Force.builder()
                        .x(width / nrOfHorizontalRectangles * j - (width / (nrOfHorizontalRectangles * 2)))
                        .y(height / nrOfVerticalRectangles * i - (height / (nrOfVerticalRectangles * 2)))
                        .build());
            }
        }
        for (int packagesNamesIndex = 0; packagesNamesIndex < packageNames.size(); packagesNamesIndex++) {
            result.put(packageNames.get(packagesNamesIndex), coordinates.get(packagesNamesIndex));
        }

        return result;
    }

    private static void calculateNumberOfLines(int size) {
        nrOfHorizontalRectangles = (int) Math.ceil(Math.sqrt(size));
        nrOfVerticalRectangles = (int) Math.ceil(Math.sqrt(size));
    }
}
