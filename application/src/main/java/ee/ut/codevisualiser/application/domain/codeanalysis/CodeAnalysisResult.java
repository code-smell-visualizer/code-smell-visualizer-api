package ee.ut.codevisualiser.application.domain.codeanalysis;

import lombok.*;
import lombok.experimental.Accessors;

import java.util.HashMap;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(fluent = true)
public class CodeAnalysisResult {

    HashMap<Long, CodeAnalysis> classCodeSmellAnalysis;

    public void setClassUsage(Long key, int classUsage) {
        classCodeSmellAnalysis.put(key, getCodeAnalysis(key).nrOfUsages(classUsage));
    }

    public void addClassCodeSmellScore(Long key) {
        if (classExists(key)) {
            classCodeSmellAnalysis.put(key, getCodeAnalysis(key).addClassCodeSmellScore());
        }
    }

    public void addMethodCodeSmellScore(Long key, int nrOfSmellyMethods) {
        if (classExists(key)) {
            classCodeSmellAnalysis.put(key, getCodeAnalysis(key).addMethodCodeSmellScore(nrOfSmellyMethods));
        }
    }

    public boolean classExists(Long id) {
        return classCodeSmellAnalysis.containsKey(id);
    }

    private CodeAnalysis getCodeAnalysis(Long key) {
        return classCodeSmellAnalysis.getOrDefault(key, new CodeAnalysis());
    }

}
