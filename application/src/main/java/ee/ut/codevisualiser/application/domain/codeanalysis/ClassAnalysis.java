package ee.ut.codevisualiser.application.domain.codeanalysis;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;

@Builder
@Accessors(fluent = true)
@Getter
public class ClassAnalysis {

    private final Long id;
    private final String name;
    private final Force force;
    private final int size;
    private final double score;
}
