package ee.ut.codevisualiser.application.usecase.codeanalysis.calculator;

public class ClassSizeCalculator {
    public static final int MINIMUM_SIZE = 10;
    public static final int MAXIMUM_SIZE = 100;
    public static final int NUMBER_OF_USAGES_IMPACT = 2; //Random constant

    public static int toClassSize(int numberOfUsages, int totalNumberOfClasses) {
        if (numberOfUsages == 0) {
            return MINIMUM_SIZE;
        }
        int size = (int) (MINIMUM_SIZE + ((double) numberOfUsages * NUMBER_OF_USAGES_IMPACT / totalNumberOfClasses * 100));
        return Math.min(size, MAXIMUM_SIZE);
    }
}
