package ee.ut.codevisualiser.application.usecase.codeanalysis;

import ee.ut.codevisualiser.application.domain.codeanalysis.CodeAnalysisResult;
import ee.ut.codevisualiser.application.domain.codesmell.CodeSmell;

import java.util.List;

public interface CodeAnalysisGateway {

    CodeAnalysisResult getCodeAnalysis(String branch, Long commitId, String appName, List<CodeSmell> codeSmells);

}
