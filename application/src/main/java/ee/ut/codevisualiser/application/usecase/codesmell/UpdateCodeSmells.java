package ee.ut.codevisualiser.application.usecase.codesmell;

import ee.ut.codevisualiser.application.domain.codesmell.CodeSmell;
import ee.ut.codevisualiser.application.domain.codesmell.CodeSmellParameter;
import ee.ut.codevisualiser.application.usecase.codeanalysis.GetCodeAnalysis;
import ee.ut.codevisualiser.common.SelfValidating;
import ee.ut.codevisualiser.common.UseCase;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@UseCase
@RequiredArgsConstructor
@Transactional
public class UpdateCodeSmells {

    private final CodeSmellGateway codeSmellGateway;

    public void execute(Request request) {
        List<CodeSmell> existingCodeSmells = codeSmellGateway.getCodeSmells();
        if (existingCodeSmells == null || existingCodeSmells.isEmpty()) {
            codeSmellGateway.save(request.codeSmells);
        } else {
            codeSmellGateway.updateParameters(getParameters(request.codeSmells));
        }
    }

    private List<CodeSmellParameter> getParameters(List<CodeSmell> codeSmells) {
        List<CodeSmellParameter> result = new ArrayList<>();
        codeSmells.forEach(codeSmell -> {
            if (codeSmell.parameters() != null) {
                result.addAll(codeSmell.parameters());
            }
        });
        return result;
    }

    @Value(staticConstructor = "of")
    @EqualsAndHashCode(callSuper = false)
    public static class Request extends SelfValidating<GetCodeAnalysis.Request> {

        @NotNull
        public List<CodeSmell> codeSmells;
    }
}
