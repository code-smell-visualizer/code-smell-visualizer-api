package ee.ut.codevisualiser.application.domain.codeanalysis;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;

@Builder
@Accessors(fluent = true)
@Getter
public class Force {
    private final int x;
    private final int y;
}
