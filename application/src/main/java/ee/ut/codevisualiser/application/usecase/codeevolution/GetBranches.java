package ee.ut.codevisualiser.application.usecase.codeevolution;

import ee.ut.codevisualiser.application.usecase.codeanalysis.CodeAnalysisGateway;
import ee.ut.codevisualiser.common.UseCase;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import javax.transaction.Transactional;
import java.util.List;

@UseCase
@RequiredArgsConstructor
@Transactional
public class GetBranches {

    private final CodeEvolutionGateway codeAnalysisGateway;

    public Response execute() {
        return Response.of(codeAnalysisGateway.getAllBranches());
    }

    @Value(staticConstructor = "of")
    public static class Response {
        public List<String> branches;
    }
}
