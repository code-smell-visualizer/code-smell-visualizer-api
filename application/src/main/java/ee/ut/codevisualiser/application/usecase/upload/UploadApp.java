package ee.ut.codevisualiser.application.usecase.upload;

import ee.ut.codevisualiser.common.SelfValidating;
import ee.ut.codevisualiser.common.UseCase;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

@UseCase
@RequiredArgsConstructor
@Transactional
public class UploadApp {

    private final UploadGateway uploadGateway;

    public Response execute(Request request) {
        return Response.of(uploadGateway.upload(request.getAppPath()));
    }


    @Value(staticConstructor = "of")
    @EqualsAndHashCode(callSuper = false)
    public static class Request extends SelfValidating<Request> {

        @NotNull
        public String appPath;
    }

    @Value(staticConstructor = "of")
    public static class Response {
        public String appName;
    }
}
