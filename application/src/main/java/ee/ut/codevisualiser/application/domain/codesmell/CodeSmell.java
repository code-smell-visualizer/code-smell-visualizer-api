package ee.ut.codevisualiser.application.domain.codesmell;

import ee.ut.codevisualiser.application.domain.codesmell.enums.CodeSmellType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Builder
@Accessors(fluent = true)
@AllArgsConstructor
@NoArgsConstructor
public class CodeSmell {

    private Long id;
    private String name;
    private String description;
    private boolean selected;
    private CodeSmellType type;
    private List<CodeSmellParameter> parameters;
}
