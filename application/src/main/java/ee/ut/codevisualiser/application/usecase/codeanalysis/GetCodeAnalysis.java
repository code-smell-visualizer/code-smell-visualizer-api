package ee.ut.codevisualiser.application.usecase.codeanalysis;

import ee.ut.codevisualiser.application.domain.codeanalysis.ClassAnalysis;
import ee.ut.codevisualiser.application.domain.codeanalysis.CodeAnalysis;
import ee.ut.codevisualiser.application.domain.codeanalysis.CodeAnalysisResult;
import ee.ut.codevisualiser.application.domain.codeanalysis.Force;
import ee.ut.codevisualiser.application.domain.codesmell.CodeSmell;
import ee.ut.codevisualiser.application.usecase.codeanalysis.calculator.ClassForceCalculator;
import ee.ut.codevisualiser.application.usecase.codeanalysis.calculator.ClassSizeCalculator;
import ee.ut.codevisualiser.common.SelfValidating;
import ee.ut.codevisualiser.common.UseCase;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@UseCase
@RequiredArgsConstructor
@Transactional
public class GetCodeAnalysis {

    private final CodeAnalysisGateway codeAnalysisGateway;

    public Response execute(Request request) {
        String branch = request.branch == null ? "master" : request.branch;
        CodeAnalysisResult codeAnalysisResult = codeAnalysisGateway.getCodeAnalysis(
                branch,
                request.commitId,
                request.appName,
                request.getCodeSmells());
        List<ClassAnalysis> classesAnalysis = toClassesAnalysis(codeAnalysisResult.classCodeSmellAnalysis());
        return Response.of(classesAnalysis);
    }

    private List<ClassAnalysis> toClassesAnalysis(HashMap<Long, CodeAnalysis> classCodeSmellAnalysis) {
        HashMap<String, Force> packageCoordinates = getPackageCoordinatesMapFromAnalysis(classCodeSmellAnalysis);
        return classCodeSmellAnalysis.entrySet().stream()
                .map(analysis -> ClassAnalysis.builder()
                        .id(analysis.getKey())
                        .name(analysis.getValue().name() == null ? "" : analysis.getValue().name())
                        .score(analysis.getValue().codeSmellScore())
                        .size(getSize(analysis.getValue(), classCodeSmellAnalysis.size()))
                        .force(packageCoordinates.get(toPackageName(analysis.getValue().path())))
                        .build())
                .collect(Collectors.toList());
    }

    private HashMap<String, Force> getPackageCoordinatesMapFromAnalysis(HashMap<Long, CodeAnalysis> classCodeSmellAnalysis) {
        List<String> packageNames = classCodeSmellAnalysis.values().stream()
                .map(CodeAnalysis::path)
                .map(this::toPackageName)
                .distinct()
                .collect(Collectors.toList());
        return ClassForceCalculator.getPackageCoordinatesMap(packageNames);
    }

    private String toPackageName(String path) {
        int beginningOfClassNameIndex = path.lastIndexOf('/');
        return beginningOfClassNameIndex == -1 ? path : path.substring(0,beginningOfClassNameIndex);
    }

    private int getSize(CodeAnalysis analysis, int numberOfClasses) {
        return ClassSizeCalculator.toClassSize(analysis.nrOfUsages(), numberOfClasses);
    }

    @Value(staticConstructor = "of")
    @EqualsAndHashCode(callSuper = false)
    public static class Request extends SelfValidating<Request> {

        @NotNull
        public List<CodeSmell> codeSmells;

        public String branch;

        public Long commitId;

        @NotNull
        public String appName;
    }

    @Value(staticConstructor = "of")
    public static class Response {
        public List<ClassAnalysis> classAnalyses;
    }

}
