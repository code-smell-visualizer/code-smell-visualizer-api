package ee.ut.codevisualiser.application.usecase.codesmell;

import ee.ut.codevisualiser.application.domain.codesmell.CodeSmell;
import ee.ut.codevisualiser.application.domain.codesmell.CodeSmellParameter;
import ee.ut.codevisualiser.common.UseCase;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import javax.transaction.Transactional;
import java.util.List;

@UseCase
@RequiredArgsConstructor
@Transactional
public class GetNrOfModifiedParameters {

    private final CodeSmellGateway codeSmellGateway;

    public Response execute() {
        List<CodeSmell> codeSmells = codeSmellGateway.getCodeSmells();
       return Response.of(countNrOfDifferentParameterValues(codeSmells));
    }

    private Integer countNrOfDifferentParameterValues(List<CodeSmell> codeSmells) {
        int result = 0;
        for (CodeSmell smell: codeSmells) {
            if (smell.parameters() != null && !smell.parameters().isEmpty()) {
                result += getNrOfMismatchedParameters(smell.parameters(), findDefaultCodeSmellParameters(smell.name()));
            }
        }
        return result;
    }

    private int getNrOfMismatchedParameters(List<CodeSmellParameter> parameters, List<CodeSmellParameter> defaultParameters) {
        int result = 0;
        for (CodeSmellParameter parameter: parameters) {
            if (parameter.value() != findCParameterValue(parameter.name(), defaultParameters)) {
                result++;
            }
        }
        return result;
    }

    private List<CodeSmellParameter> findDefaultCodeSmellParameters(String smellName) {
        return DefaultCodeSmells.getDefaultCodeSmells().stream()
                .filter(smell -> smell.name().equals(smellName))
                .findFirst().get().parameters();
    }

    private double findCParameterValue(String parameterName, List<CodeSmellParameter> parameters) {
        return parameters.stream()
                .filter(parameter -> parameter.name().equals(parameterName))
                .findFirst().get().value();
    }

    @Value(staticConstructor = "of")
    public static class Response {
        public Integer nrOfParamsModified;
    }


}
