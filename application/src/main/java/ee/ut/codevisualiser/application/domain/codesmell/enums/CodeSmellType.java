package ee.ut.codevisualiser.application.domain.codesmell.enums;

public enum CodeSmellType {
    CLASS_BASED("CLASS_BASED"),
    METHOD_BASED("METHOD_BASED"),
    OTHER("OTHER");

    private final String type;

    CodeSmellType(String type) {
        this.type = type;
    }
}
