package ee.ut.codevisualiser.application.domain.codeevolution;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Builder
@Accessors(fluent = true)
@AllArgsConstructor
@NoArgsConstructor
public class App {

    private Long id;
    private String author;
    private String branch;
    private String commitHash;
    private String commitMessage;
    private String name;
    private Date time;
    private long versionNumber;
}
