rootProject.name = "code-visualiser"
include(
        "common",
        "application",
        "adapters:persistence",
        "adapters:graphify",
        "adapters:web",
        "configuration"
)
